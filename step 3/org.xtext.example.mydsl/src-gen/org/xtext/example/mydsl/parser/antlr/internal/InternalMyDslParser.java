package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'", "'('", "')'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=4;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=6;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=7;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_FALSE=5;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Start";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:64:1: entryRuleStart returns [EObject current=null] : iv_ruleStart= ruleStart EOF ;
    public final EObject entryRuleStart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStart = null;


        try {
            // InternalMyDsl.g:64:46: (iv_ruleStart= ruleStart EOF )
            // InternalMyDsl.g:65:2: iv_ruleStart= ruleStart EOF
            {
             newCompositeNode(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStart=ruleStart();

            state._fsp--;

             current =iv_ruleStart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:71:1: ruleStart returns [EObject current=null] : ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )* ;
    public final EObject ruleStart() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_formula_0_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )* )
            // InternalMyDsl.g:78:2: ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )*
            {
            // InternalMyDsl.g:78:2: ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||(LA1_0>=19 && LA1_0<=20)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:79:3: ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';'
            	    {
            	    // InternalMyDsl.g:79:3: ( (lv_formula_0_0= ruleBiimplies ) )
            	    // InternalMyDsl.g:80:4: (lv_formula_0_0= ruleBiimplies )
            	    {
            	    // InternalMyDsl.g:80:4: (lv_formula_0_0= ruleBiimplies )
            	    // InternalMyDsl.g:81:5: lv_formula_0_0= ruleBiimplies
            	    {

            	    					newCompositeNode(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_formula_0_0=ruleBiimplies();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStartRule());
            	    					}
            	    					add(
            	    						current,
            	    						"formula",
            	    						lv_formula_0_0,
            	    						"org.xtext.example.mydsl.MyDsl.Biimplies");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }

            	    otherlv_1=(Token)match(input,13,FOLLOW_4); 

            	    			newLeafNode(otherlv_1, grammarAccess.getStartAccess().getSemicolonKeyword_1());
            	    		

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:106:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;


        try {
            // InternalMyDsl.g:106:50: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalMyDsl.g:107:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:113:1: ruleBiimplies returns [EObject current=null] : (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Implies_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:119:2: ( (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* ) )
            // InternalMyDsl.g:120:2: (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* )
            {
            // InternalMyDsl.g:120:2: (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* )
            // InternalMyDsl.g:121:3: this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )*
            {

            			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
            		
            pushFollow(FOLLOW_5);
            this_Implies_0=ruleImplies();

            state._fsp--;


            			current = this_Implies_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:129:3: ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:130:4: () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) )
            	    {
            	    // InternalMyDsl.g:130:4: ()
            	    // InternalMyDsl.g:131:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:137:4: ( (lv_type_2_0= '<->' ) )
            	    // InternalMyDsl.g:138:5: (lv_type_2_0= '<->' )
            	    {
            	    // InternalMyDsl.g:138:5: (lv_type_2_0= '<->' )
            	    // InternalMyDsl.g:139:6: lv_type_2_0= '<->'
            	    {
            	    lv_type_2_0=(Token)match(input,14,FOLLOW_6); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBiimpliesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "<->");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:151:4: ( (lv_right_3_0= ruleImplies ) )
            	    // InternalMyDsl.g:152:5: (lv_right_3_0= ruleImplies )
            	    {
            	    // InternalMyDsl.g:152:5: (lv_right_3_0= ruleImplies )
            	    // InternalMyDsl.g:153:6: lv_right_3_0= ruleImplies
            	    {

            	    						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_right_3_0=ruleImplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBiimpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Implies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:175:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalMyDsl.g:175:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalMyDsl.g:176:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:182:1: ruleImplies returns [EObject current=null] : (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Excludes_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:188:2: ( (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* ) )
            // InternalMyDsl.g:189:2: (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* )
            {
            // InternalMyDsl.g:189:2: (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* )
            // InternalMyDsl.g:190:3: this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0());
            		
            pushFollow(FOLLOW_7);
            this_Excludes_0=ruleExcludes();

            state._fsp--;


            			current = this_Excludes_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:198:3: ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:199:4: () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) )
            	    {
            	    // InternalMyDsl.g:199:4: ()
            	    // InternalMyDsl.g:200:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:206:4: ( (lv_type_2_0= '->' ) )
            	    // InternalMyDsl.g:207:5: (lv_type_2_0= '->' )
            	    {
            	    // InternalMyDsl.g:207:5: (lv_type_2_0= '->' )
            	    // InternalMyDsl.g:208:6: lv_type_2_0= '->'
            	    {
            	    lv_type_2_0=(Token)match(input,15,FOLLOW_8); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getImpliesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "->");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:220:4: ( (lv_right_3_0= ruleExcludes ) )
            	    // InternalMyDsl.g:221:5: (lv_right_3_0= ruleExcludes )
            	    {
            	    // InternalMyDsl.g:221:5: (lv_right_3_0= ruleExcludes )
            	    // InternalMyDsl.g:222:6: lv_right_3_0= ruleExcludes
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_right_3_0=ruleExcludes();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Excludes");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:244:1: entryRuleExcludes returns [EObject current=null] : iv_ruleExcludes= ruleExcludes EOF ;
    public final EObject entryRuleExcludes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExcludes = null;


        try {
            // InternalMyDsl.g:244:49: (iv_ruleExcludes= ruleExcludes EOF )
            // InternalMyDsl.g:245:2: iv_ruleExcludes= ruleExcludes EOF
            {
             newCompositeNode(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExcludes=ruleExcludes();

            state._fsp--;

             current =iv_ruleExcludes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:251:1: ruleExcludes returns [EObject current=null] : (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* ) ;
    public final EObject ruleExcludes() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Or_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:257:2: ( (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* ) )
            // InternalMyDsl.g:258:2: (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* )
            {
            // InternalMyDsl.g:258:2: (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* )
            // InternalMyDsl.g:259:3: this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )*
            {

            			newCompositeNode(grammarAccess.getExcludesAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_9);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:267:3: ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==16) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:268:4: () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) )
            	    {
            	    // InternalMyDsl.g:268:4: ()
            	    // InternalMyDsl.g:269:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:275:4: ( (lv_type_2_0= 'nand' ) )
            	    // InternalMyDsl.g:276:5: (lv_type_2_0= 'nand' )
            	    {
            	    // InternalMyDsl.g:276:5: (lv_type_2_0= 'nand' )
            	    // InternalMyDsl.g:277:6: lv_type_2_0= 'nand'
            	    {
            	    lv_type_2_0=(Token)match(input,16,FOLLOW_10); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getExcludesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "nand");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:289:4: ( (lv_right_3_0= ruleOr ) )
            	    // InternalMyDsl.g:290:5: (lv_right_3_0= ruleOr )
            	    {
            	    // InternalMyDsl.g:290:5: (lv_right_3_0= ruleOr )
            	    // InternalMyDsl.g:291:6: lv_right_3_0= ruleOr
            	    {

            	    						newCompositeNode(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_right_3_0=ruleOr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExcludesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Or");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:313:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalMyDsl.g:313:43: (iv_ruleOr= ruleOr EOF )
            // InternalMyDsl.g:314:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:320:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:326:2: ( (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalMyDsl.g:327:2: (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalMyDsl.g:327:2: (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalMyDsl.g:328:3: this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:336:3: ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:337:4: () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalMyDsl.g:337:4: ()
            	    // InternalMyDsl.g:338:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:344:4: ( (lv_type_2_0= 'or' ) )
            	    // InternalMyDsl.g:345:5: (lv_type_2_0= 'or' )
            	    {
            	    // InternalMyDsl.g:345:5: (lv_type_2_0= 'or' )
            	    // InternalMyDsl.g:346:6: lv_type_2_0= 'or'
            	    {
            	    lv_type_2_0=(Token)match(input,17,FOLLOW_12); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getOrRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "or");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:358:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalMyDsl.g:359:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalMyDsl.g:359:5: (lv_right_3_0= ruleAnd )
            	    // InternalMyDsl.g:360:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:382:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalMyDsl.g:382:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalMyDsl.g:383:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:389:1: ruleAnd returns [EObject current=null] : (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Not_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:395:2: ( (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* ) )
            // InternalMyDsl.g:396:2: (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* )
            {
            // InternalMyDsl.g:396:2: (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* )
            // InternalMyDsl.g:397:3: this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getNotParserRuleCall_0());
            		
            pushFollow(FOLLOW_13);
            this_Not_0=ruleNot();

            state._fsp--;


            			current = this_Not_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:405:3: ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:406:4: () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) )
            	    {
            	    // InternalMyDsl.g:406:4: ()
            	    // InternalMyDsl.g:407:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:413:4: ( (lv_type_2_0= 'and' ) )
            	    // InternalMyDsl.g:414:5: (lv_type_2_0= 'and' )
            	    {
            	    // InternalMyDsl.g:414:5: (lv_type_2_0= 'and' )
            	    // InternalMyDsl.g:415:6: lv_type_2_0= 'and'
            	    {
            	    lv_type_2_0=(Token)match(input,18,FOLLOW_14); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getAndRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "and");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:427:4: ( (lv_right_3_0= ruleNot ) )
            	    // InternalMyDsl.g:428:5: (lv_right_3_0= ruleNot )
            	    {
            	    // InternalMyDsl.g:428:5: (lv_right_3_0= ruleNot )
            	    // InternalMyDsl.g:429:6: lv_right_3_0= ruleNot
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_right_3_0=ruleNot();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Not");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:451:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalMyDsl.g:451:44: (iv_ruleNot= ruleNot EOF )
            // InternalMyDsl.g:452:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:458:1: ruleNot returns [EObject current=null] : ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        EObject lv_right_1_0 = null;

        EObject lv_right_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:464:2: ( ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) ) )
            // InternalMyDsl.g:465:2: ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) )
            {
            // InternalMyDsl.g:465:2: ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==19) ) {
                alt7=1;
            }
            else if ( ((LA7_0>=RULE_TRUE && LA7_0<=RULE_ID)||LA7_0==20) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalMyDsl.g:466:3: ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) )
                    {
                    // InternalMyDsl.g:466:3: ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) )
                    // InternalMyDsl.g:467:4: ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) )
                    {
                    // InternalMyDsl.g:467:4: ( (lv_type_0_0= 'not' ) )
                    // InternalMyDsl.g:468:5: (lv_type_0_0= 'not' )
                    {
                    // InternalMyDsl.g:468:5: (lv_type_0_0= 'not' )
                    // InternalMyDsl.g:469:6: lv_type_0_0= 'not'
                    {
                    lv_type_0_0=(Token)match(input,19,FOLLOW_15); 

                    						newLeafNode(lv_type_0_0, grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getNotRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_0_0, "not");
                    					

                    }


                    }

                    // InternalMyDsl.g:481:4: ( (lv_right_1_0= rulePrimary ) )
                    // InternalMyDsl.g:482:5: (lv_right_1_0= rulePrimary )
                    {
                    // InternalMyDsl.g:482:5: (lv_right_1_0= rulePrimary )
                    // InternalMyDsl.g:483:6: lv_right_1_0= rulePrimary
                    {

                    						newCompositeNode(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_right_1_0=rulePrimary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNotRule());
                    						}
                    						set(
                    							current,
                    							"right",
                    							lv_right_1_0,
                    							"org.xtext.example.mydsl.MyDsl.Primary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:502:3: ( (lv_right_2_0= rulePrimary ) )
                    {
                    // InternalMyDsl.g:502:3: ( (lv_right_2_0= rulePrimary ) )
                    // InternalMyDsl.g:503:4: (lv_right_2_0= rulePrimary )
                    {
                    // InternalMyDsl.g:503:4: (lv_right_2_0= rulePrimary )
                    // InternalMyDsl.g:504:5: lv_right_2_0= rulePrimary
                    {

                    					newCompositeNode(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_right_2_0=rulePrimary();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getNotRule());
                    					}
                    					set(
                    						current,
                    						"right",
                    						lv_right_2_0,
                    						"org.xtext.example.mydsl.MyDsl.Primary");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:525:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalMyDsl.g:525:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalMyDsl.g:526:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:532:1: rulePrimary returns [EObject current=null] : ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token lv_value_4_0=null;
        Token lv_value_5_0=null;
        EObject this_Biimplies_1 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:538:2: ( ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) ) )
            // InternalMyDsl.g:539:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) )
            {
            // InternalMyDsl.g:539:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt8=1;
                }
                break;
            case RULE_TRUE:
                {
                alt8=2;
                }
                break;
            case RULE_FALSE:
                {
                alt8=3;
                }
                break;
            case RULE_ID:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalMyDsl.g:540:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    {
                    // InternalMyDsl.g:540:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    // InternalMyDsl.g:541:4: otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,20,FOLLOW_16); 

                    				newLeafNode(otherlv_0, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_17);
                    this_Biimplies_1=ruleBiimplies();

                    state._fsp--;


                    				current = this_Biimplies_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,21,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:559:3: ( (lv_value_3_0= RULE_TRUE ) )
                    {
                    // InternalMyDsl.g:559:3: ( (lv_value_3_0= RULE_TRUE ) )
                    // InternalMyDsl.g:560:4: (lv_value_3_0= RULE_TRUE )
                    {
                    // InternalMyDsl.g:560:4: (lv_value_3_0= RULE_TRUE )
                    // InternalMyDsl.g:561:5: lv_value_3_0= RULE_TRUE
                    {
                    lv_value_3_0=(Token)match(input,RULE_TRUE,FOLLOW_2); 

                    					newLeafNode(lv_value_3_0, grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_3_0,
                    						"org.xtext.example.mydsl.MyDsl.TRUE");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:578:3: ( (lv_value_4_0= RULE_FALSE ) )
                    {
                    // InternalMyDsl.g:578:3: ( (lv_value_4_0= RULE_FALSE ) )
                    // InternalMyDsl.g:579:4: (lv_value_4_0= RULE_FALSE )
                    {
                    // InternalMyDsl.g:579:4: (lv_value_4_0= RULE_FALSE )
                    // InternalMyDsl.g:580:5: lv_value_4_0= RULE_FALSE
                    {
                    lv_value_4_0=(Token)match(input,RULE_FALSE,FOLLOW_2); 

                    					newLeafNode(lv_value_4_0, grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_4_0,
                    						"org.xtext.example.mydsl.MyDsl.FALSE");
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:597:3: ( (lv_value_5_0= RULE_ID ) )
                    {
                    // InternalMyDsl.g:597:3: ( (lv_value_5_0= RULE_ID ) )
                    // InternalMyDsl.g:598:4: (lv_value_5_0= RULE_ID )
                    {
                    // InternalMyDsl.g:598:4: (lv_value_5_0= RULE_ID )
                    // InternalMyDsl.g:599:5: lv_value_5_0= RULE_ID
                    {
                    lv_value_5_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_value_5_0, grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_5_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000180072L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000184070L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000188070L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000190070L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000001A0070L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000000001C0070L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000180070L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000380070L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000200000L});

}