package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "'('", "')'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=4;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=6;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=7;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_FALSE=5;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:53:1: entryRuleStart : ruleStart EOF ;
    public final void entryRuleStart() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleStart EOF )
            // InternalMyDsl.g:55:1: ruleStart EOF
            {
             before(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            ruleStart();

            state._fsp--;

             after(grammarAccess.getStartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:62:1: ruleStart : ( ( rule__Start__Group__0 )* ) ;
    public final void ruleStart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Start__Group__0 )* ) )
            // InternalMyDsl.g:67:2: ( ( rule__Start__Group__0 )* )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Start__Group__0 )* )
            // InternalMyDsl.g:68:3: ( rule__Start__Group__0 )*
            {
             before(grammarAccess.getStartAccess().getGroup()); 
            // InternalMyDsl.g:69:3: ( rule__Start__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||LA1_0==14||LA1_0==21) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:69:4: rule__Start__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Start__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStartAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:78:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleBiimplies EOF )
            // InternalMyDsl.g:80:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:87:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalMyDsl.g:93:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalMyDsl.g:94:3: ( rule__Biimplies__Group__0 )
            // InternalMyDsl.g:94:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:103:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleImplies EOF )
            // InternalMyDsl.g:105:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:112:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__Implies__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__Implies__Group__0 )
            // InternalMyDsl.g:119:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:128:1: entryRuleExcludes : ruleExcludes EOF ;
    public final void entryRuleExcludes() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleExcludes EOF )
            // InternalMyDsl.g:130:1: ruleExcludes EOF
            {
             before(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getExcludesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:137:1: ruleExcludes : ( ( rule__Excludes__Group__0 ) ) ;
    public final void ruleExcludes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Excludes__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Excludes__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Excludes__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Excludes__Group__0 )
            {
             before(grammarAccess.getExcludesAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Excludes__Group__0 )
            // InternalMyDsl.g:144:4: rule__Excludes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:153:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleOr EOF )
            // InternalMyDsl.g:155:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:162:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Or__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Or__Group__0 )
            // InternalMyDsl.g:169:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:178:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleAnd EOF )
            // InternalMyDsl.g:180:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:187:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__And__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__And__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__And__Group__0 )
            // InternalMyDsl.g:194:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:203:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleNot EOF )
            // InternalMyDsl.g:205:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:212:1: ruleNot : ( ( rule__Not__Alternatives ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__Not__Alternatives ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__Not__Alternatives ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__Not__Alternatives ) )
            // InternalMyDsl.g:218:3: ( rule__Not__Alternatives )
            {
             before(grammarAccess.getNotAccess().getAlternatives()); 
            // InternalMyDsl.g:219:3: ( rule__Not__Alternatives )
            // InternalMyDsl.g:219:4: rule__Not__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Not__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:228:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( rulePrimary EOF )
            // InternalMyDsl.g:230:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:237:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__Primary__Alternatives ) )
            // InternalMyDsl.g:243:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalMyDsl.g:244:3: ( rule__Primary__Alternatives )
            // InternalMyDsl.g:244:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "rule__Not__Alternatives"
    // InternalMyDsl.g:252:1: rule__Not__Alternatives : ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) );
    public final void rule__Not__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:256:1: ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==21) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=RULE_TRUE && LA2_0<=RULE_ID)||LA2_0==14) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:257:2: ( ( rule__Not__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:257:2: ( ( rule__Not__Group_0__0 ) )
                    // InternalMyDsl.g:258:3: ( rule__Not__Group_0__0 )
                    {
                     before(grammarAccess.getNotAccess().getGroup_0()); 
                    // InternalMyDsl.g:259:3: ( rule__Not__Group_0__0 )
                    // InternalMyDsl.g:259:4: rule__Not__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:263:2: ( ( rule__Not__RightAssignment_1 ) )
                    {
                    // InternalMyDsl.g:263:2: ( ( rule__Not__RightAssignment_1 ) )
                    // InternalMyDsl.g:264:3: ( rule__Not__RightAssignment_1 )
                    {
                     before(grammarAccess.getNotAccess().getRightAssignment_1()); 
                    // InternalMyDsl.g:265:3: ( rule__Not__RightAssignment_1 )
                    // InternalMyDsl.g:265:4: rule__Not__RightAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__RightAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getRightAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalMyDsl.g:273:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:277:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt3=1;
                }
                break;
            case RULE_TRUE:
                {
                alt3=2;
                }
                break;
            case RULE_FALSE:
                {
                alt3=3;
                }
                break;
            case RULE_ID:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:278:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:278:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalMyDsl.g:279:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalMyDsl.g:280:3: ( rule__Primary__Group_0__0 )
                    // InternalMyDsl.g:280:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:284:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:284:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    // InternalMyDsl.g:285:3: ( rule__Primary__ValueAssignment_1 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:286:3: ( rule__Primary__ValueAssignment_1 )
                    // InternalMyDsl.g:286:4: rule__Primary__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:290:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:290:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    // InternalMyDsl.g:291:3: ( rule__Primary__ValueAssignment_2 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:292:3: ( rule__Primary__ValueAssignment_2 )
                    // InternalMyDsl.g:292:4: rule__Primary__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:296:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    {
                    // InternalMyDsl.g:296:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    // InternalMyDsl.g:297:3: ( rule__Primary__ValueAssignment_3 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 
                    // InternalMyDsl.g:298:3: ( rule__Primary__ValueAssignment_3 )
                    // InternalMyDsl.g:298:4: rule__Primary__ValueAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Start__Group__0"
    // InternalMyDsl.g:306:1: rule__Start__Group__0 : rule__Start__Group__0__Impl rule__Start__Group__1 ;
    public final void rule__Start__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:310:1: ( rule__Start__Group__0__Impl rule__Start__Group__1 )
            // InternalMyDsl.g:311:2: rule__Start__Group__0__Impl rule__Start__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Start__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__0"


    // $ANTLR start "rule__Start__Group__0__Impl"
    // InternalMyDsl.g:318:1: rule__Start__Group__0__Impl : ( ( rule__Start__FormulaAssignment_0 ) ) ;
    public final void rule__Start__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:322:1: ( ( ( rule__Start__FormulaAssignment_0 ) ) )
            // InternalMyDsl.g:323:1: ( ( rule__Start__FormulaAssignment_0 ) )
            {
            // InternalMyDsl.g:323:1: ( ( rule__Start__FormulaAssignment_0 ) )
            // InternalMyDsl.g:324:2: ( rule__Start__FormulaAssignment_0 )
            {
             before(grammarAccess.getStartAccess().getFormulaAssignment_0()); 
            // InternalMyDsl.g:325:2: ( rule__Start__FormulaAssignment_0 )
            // InternalMyDsl.g:325:3: rule__Start__FormulaAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Start__FormulaAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFormulaAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__0__Impl"


    // $ANTLR start "rule__Start__Group__1"
    // InternalMyDsl.g:333:1: rule__Start__Group__1 : rule__Start__Group__1__Impl ;
    public final void rule__Start__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:337:1: ( rule__Start__Group__1__Impl )
            // InternalMyDsl.g:338:2: rule__Start__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__1"


    // $ANTLR start "rule__Start__Group__1__Impl"
    // InternalMyDsl.g:344:1: rule__Start__Group__1__Impl : ( ';' ) ;
    public final void rule__Start__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:348:1: ( ( ';' ) )
            // InternalMyDsl.g:349:1: ( ';' )
            {
            // InternalMyDsl.g:349:1: ( ';' )
            // InternalMyDsl.g:350:2: ';'
            {
             before(grammarAccess.getStartAccess().getSemicolonKeyword_1()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalMyDsl.g:360:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:364:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalMyDsl.g:365:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalMyDsl.g:372:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:376:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:377:1: ( ruleImplies )
            {
            // InternalMyDsl.g:377:1: ( ruleImplies )
            // InternalMyDsl.g:378:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalMyDsl.g:387:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:391:1: ( rule__Biimplies__Group__1__Impl )
            // InternalMyDsl.g:392:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalMyDsl.g:398:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:402:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalMyDsl.g:403:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalMyDsl.g:403:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalMyDsl.g:404:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:405:2: ( rule__Biimplies__Group_1__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==16) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:405:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalMyDsl.g:414:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:418:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalMyDsl.g:419:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_5);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalMyDsl.g:426:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:430:1: ( ( () ) )
            // InternalMyDsl.g:431:1: ( () )
            {
            // InternalMyDsl.g:431:1: ( () )
            // InternalMyDsl.g:432:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalMyDsl.g:433:2: ()
            // InternalMyDsl.g:433:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalMyDsl.g:441:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:445:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalMyDsl.g:446:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalMyDsl.g:453:1: rule__Biimplies__Group_1__1__Impl : ( ( rule__Biimplies__TypeAssignment_1_1 ) ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:457:1: ( ( ( rule__Biimplies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:458:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:458:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:459:2: ( rule__Biimplies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:460:2: ( rule__Biimplies__TypeAssignment_1_1 )
            // InternalMyDsl.g:460:3: rule__Biimplies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalMyDsl.g:468:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:472:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalMyDsl.g:473:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalMyDsl.g:479:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:483:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:484:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:484:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:485:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:486:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalMyDsl.g:486:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalMyDsl.g:495:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:499:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalMyDsl.g:500:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalMyDsl.g:507:1: rule__Implies__Group__0__Impl : ( ruleExcludes ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:511:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:512:1: ( ruleExcludes )
            {
            // InternalMyDsl.g:512:1: ( ruleExcludes )
            // InternalMyDsl.g:513:2: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalMyDsl.g:522:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:526:1: ( rule__Implies__Group__1__Impl )
            // InternalMyDsl.g:527:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalMyDsl.g:533:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:537:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalMyDsl.g:538:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalMyDsl.g:538:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalMyDsl.g:539:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:540:2: ( rule__Implies__Group_1__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:540:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalMyDsl.g:549:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:553:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalMyDsl.g:554:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalMyDsl.g:561:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:565:1: ( ( () ) )
            // InternalMyDsl.g:566:1: ( () )
            {
            // InternalMyDsl.g:566:1: ( () )
            // InternalMyDsl.g:567:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalMyDsl.g:568:2: ()
            // InternalMyDsl.g:568:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalMyDsl.g:576:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:580:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalMyDsl.g:581:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalMyDsl.g:588:1: rule__Implies__Group_1__1__Impl : ( ( rule__Implies__TypeAssignment_1_1 ) ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:592:1: ( ( ( rule__Implies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:593:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:593:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:594:2: ( rule__Implies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:595:2: ( rule__Implies__TypeAssignment_1_1 )
            // InternalMyDsl.g:595:3: rule__Implies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Implies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalMyDsl.g:603:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:607:1: ( rule__Implies__Group_1__2__Impl )
            // InternalMyDsl.g:608:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalMyDsl.g:614:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:618:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:619:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:619:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:620:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:621:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalMyDsl.g:621:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__Excludes__Group__0"
    // InternalMyDsl.g:630:1: rule__Excludes__Group__0 : rule__Excludes__Group__0__Impl rule__Excludes__Group__1 ;
    public final void rule__Excludes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:634:1: ( rule__Excludes__Group__0__Impl rule__Excludes__Group__1 )
            // InternalMyDsl.g:635:2: rule__Excludes__Group__0__Impl rule__Excludes__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Excludes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0"


    // $ANTLR start "rule__Excludes__Group__0__Impl"
    // InternalMyDsl.g:642:1: rule__Excludes__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Excludes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:646:1: ( ( ruleOr ) )
            // InternalMyDsl.g:647:1: ( ruleOr )
            {
            // InternalMyDsl.g:647:1: ( ruleOr )
            // InternalMyDsl.g:648:2: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0__Impl"


    // $ANTLR start "rule__Excludes__Group__1"
    // InternalMyDsl.g:657:1: rule__Excludes__Group__1 : rule__Excludes__Group__1__Impl ;
    public final void rule__Excludes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:661:1: ( rule__Excludes__Group__1__Impl )
            // InternalMyDsl.g:662:2: rule__Excludes__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1"


    // $ANTLR start "rule__Excludes__Group__1__Impl"
    // InternalMyDsl.g:668:1: rule__Excludes__Group__1__Impl : ( ( rule__Excludes__Group_1__0 )* ) ;
    public final void rule__Excludes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:672:1: ( ( ( rule__Excludes__Group_1__0 )* ) )
            // InternalMyDsl.g:673:1: ( ( rule__Excludes__Group_1__0 )* )
            {
            // InternalMyDsl.g:673:1: ( ( rule__Excludes__Group_1__0 )* )
            // InternalMyDsl.g:674:2: ( rule__Excludes__Group_1__0 )*
            {
             before(grammarAccess.getExcludesAccess().getGroup_1()); 
            // InternalMyDsl.g:675:2: ( rule__Excludes__Group_1__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:675:3: rule__Excludes__Group_1__0
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Excludes__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getExcludesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__0"
    // InternalMyDsl.g:684:1: rule__Excludes__Group_1__0 : rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 ;
    public final void rule__Excludes__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:688:1: ( rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 )
            // InternalMyDsl.g:689:2: rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Excludes__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0"


    // $ANTLR start "rule__Excludes__Group_1__0__Impl"
    // InternalMyDsl.g:696:1: rule__Excludes__Group_1__0__Impl : ( () ) ;
    public final void rule__Excludes__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:700:1: ( ( () ) )
            // InternalMyDsl.g:701:1: ( () )
            {
            // InternalMyDsl.g:701:1: ( () )
            // InternalMyDsl.g:702:2: ()
            {
             before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 
            // InternalMyDsl.g:703:2: ()
            // InternalMyDsl.g:703:3: 
            {
            }

             after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0__Impl"


    // $ANTLR start "rule__Excludes__Group_1__1"
    // InternalMyDsl.g:711:1: rule__Excludes__Group_1__1 : rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 ;
    public final void rule__Excludes__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:715:1: ( rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 )
            // InternalMyDsl.g:716:2: rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Excludes__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1"


    // $ANTLR start "rule__Excludes__Group_1__1__Impl"
    // InternalMyDsl.g:723:1: rule__Excludes__Group_1__1__Impl : ( ( rule__Excludes__TypeAssignment_1_1 ) ) ;
    public final void rule__Excludes__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:727:1: ( ( ( rule__Excludes__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:728:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:728:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:729:2: ( rule__Excludes__TypeAssignment_1_1 )
            {
             before(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:730:2: ( rule__Excludes__TypeAssignment_1_1 )
            // InternalMyDsl.g:730:3: rule__Excludes__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__2"
    // InternalMyDsl.g:738:1: rule__Excludes__Group_1__2 : rule__Excludes__Group_1__2__Impl ;
    public final void rule__Excludes__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:742:1: ( rule__Excludes__Group_1__2__Impl )
            // InternalMyDsl.g:743:2: rule__Excludes__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2"


    // $ANTLR start "rule__Excludes__Group_1__2__Impl"
    // InternalMyDsl.g:749:1: rule__Excludes__Group_1__2__Impl : ( ( rule__Excludes__RightAssignment_1_2 ) ) ;
    public final void rule__Excludes__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:753:1: ( ( ( rule__Excludes__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:754:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:754:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            // InternalMyDsl.g:755:2: ( rule__Excludes__RightAssignment_1_2 )
            {
             before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:756:2: ( rule__Excludes__RightAssignment_1_2 )
            // InternalMyDsl.g:756:3: rule__Excludes__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalMyDsl.g:765:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:769:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalMyDsl.g:770:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalMyDsl.g:777:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:781:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:782:1: ( ruleAnd )
            {
            // InternalMyDsl.g:782:1: ( ruleAnd )
            // InternalMyDsl.g:783:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalMyDsl.g:792:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:796:1: ( rule__Or__Group__1__Impl )
            // InternalMyDsl.g:797:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalMyDsl.g:803:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:807:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalMyDsl.g:808:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalMyDsl.g:808:1: ( ( rule__Or__Group_1__0 )* )
            // InternalMyDsl.g:809:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalMyDsl.g:810:2: ( rule__Or__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==19) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:810:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalMyDsl.g:819:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:823:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalMyDsl.g:824:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalMyDsl.g:831:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:835:1: ( ( () ) )
            // InternalMyDsl.g:836:1: ( () )
            {
            // InternalMyDsl.g:836:1: ( () )
            // InternalMyDsl.g:837:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalMyDsl.g:838:2: ()
            // InternalMyDsl.g:838:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalMyDsl.g:846:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:850:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalMyDsl.g:851:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalMyDsl.g:858:1: rule__Or__Group_1__1__Impl : ( ( rule__Or__TypeAssignment_1_1 ) ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:862:1: ( ( ( rule__Or__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:863:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:863:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:864:2: ( rule__Or__TypeAssignment_1_1 )
            {
             before(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:865:2: ( rule__Or__TypeAssignment_1_1 )
            // InternalMyDsl.g:865:3: rule__Or__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Or__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalMyDsl.g:873:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:877:1: ( rule__Or__Group_1__2__Impl )
            // InternalMyDsl.g:878:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalMyDsl.g:884:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:888:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:889:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:889:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalMyDsl.g:890:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:891:2: ( rule__Or__RightAssignment_1_2 )
            // InternalMyDsl.g:891:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalMyDsl.g:900:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:904:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalMyDsl.g:905:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalMyDsl.g:912:1: rule__And__Group__0__Impl : ( ruleNot ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:916:1: ( ( ruleNot ) )
            // InternalMyDsl.g:917:1: ( ruleNot )
            {
            // InternalMyDsl.g:917:1: ( ruleNot )
            // InternalMyDsl.g:918:2: ruleNot
            {
             before(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalMyDsl.g:927:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:931:1: ( rule__And__Group__1__Impl )
            // InternalMyDsl.g:932:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalMyDsl.g:938:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:942:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalMyDsl.g:943:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalMyDsl.g:943:1: ( ( rule__And__Group_1__0 )* )
            // InternalMyDsl.g:944:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalMyDsl.g:945:2: ( rule__And__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==20) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:945:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalMyDsl.g:954:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:958:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalMyDsl.g:959:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_14);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalMyDsl.g:966:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:970:1: ( ( () ) )
            // InternalMyDsl.g:971:1: ( () )
            {
            // InternalMyDsl.g:971:1: ( () )
            // InternalMyDsl.g:972:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalMyDsl.g:973:2: ()
            // InternalMyDsl.g:973:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalMyDsl.g:981:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:985:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalMyDsl.g:986:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalMyDsl.g:993:1: rule__And__Group_1__1__Impl : ( ( rule__And__TypeAssignment_1_1 ) ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:997:1: ( ( ( rule__And__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:998:1: ( ( rule__And__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:998:1: ( ( rule__And__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:999:2: ( rule__And__TypeAssignment_1_1 )
            {
             before(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1000:2: ( rule__And__TypeAssignment_1_1 )
            // InternalMyDsl.g:1000:3: rule__And__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__And__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalMyDsl.g:1008:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1012:1: ( rule__And__Group_1__2__Impl )
            // InternalMyDsl.g:1013:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalMyDsl.g:1019:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1023:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1024:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1024:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1025:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1026:2: ( rule__And__RightAssignment_1_2 )
            // InternalMyDsl.g:1026:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group_0__0"
    // InternalMyDsl.g:1035:1: rule__Not__Group_0__0 : rule__Not__Group_0__0__Impl rule__Not__Group_0__1 ;
    public final void rule__Not__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1039:1: ( rule__Not__Group_0__0__Impl rule__Not__Group_0__1 )
            // InternalMyDsl.g:1040:2: rule__Not__Group_0__0__Impl rule__Not__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__Not__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0"


    // $ANTLR start "rule__Not__Group_0__0__Impl"
    // InternalMyDsl.g:1047:1: rule__Not__Group_0__0__Impl : ( ( rule__Not__TypeAssignment_0_0 ) ) ;
    public final void rule__Not__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1051:1: ( ( ( rule__Not__TypeAssignment_0_0 ) ) )
            // InternalMyDsl.g:1052:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            {
            // InternalMyDsl.g:1052:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            // InternalMyDsl.g:1053:2: ( rule__Not__TypeAssignment_0_0 )
            {
             before(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 
            // InternalMyDsl.g:1054:2: ( rule__Not__TypeAssignment_0_0 )
            // InternalMyDsl.g:1054:3: rule__Not__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Not__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0__Impl"


    // $ANTLR start "rule__Not__Group_0__1"
    // InternalMyDsl.g:1062:1: rule__Not__Group_0__1 : rule__Not__Group_0__1__Impl ;
    public final void rule__Not__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1066:1: ( rule__Not__Group_0__1__Impl )
            // InternalMyDsl.g:1067:2: rule__Not__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1"


    // $ANTLR start "rule__Not__Group_0__1__Impl"
    // InternalMyDsl.g:1073:1: rule__Not__Group_0__1__Impl : ( ( rule__Not__RightAssignment_0_1 ) ) ;
    public final void rule__Not__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1077:1: ( ( ( rule__Not__RightAssignment_0_1 ) ) )
            // InternalMyDsl.g:1078:1: ( ( rule__Not__RightAssignment_0_1 ) )
            {
            // InternalMyDsl.g:1078:1: ( ( rule__Not__RightAssignment_0_1 ) )
            // InternalMyDsl.g:1079:2: ( rule__Not__RightAssignment_0_1 )
            {
             before(grammarAccess.getNotAccess().getRightAssignment_0_1()); 
            // InternalMyDsl.g:1080:2: ( rule__Not__RightAssignment_0_1 )
            // InternalMyDsl.g:1080:3: rule__Not__RightAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Not__RightAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getRightAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalMyDsl.g:1089:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1093:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalMyDsl.g:1094:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalMyDsl.g:1101:1: rule__Primary__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1105:1: ( ( '(' ) )
            // InternalMyDsl.g:1106:1: ( '(' )
            {
            // InternalMyDsl.g:1106:1: ( '(' )
            // InternalMyDsl.g:1107:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalMyDsl.g:1116:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1120:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalMyDsl.g:1121:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_16);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalMyDsl.g:1128:1: rule__Primary__Group_0__1__Impl : ( ruleBiimplies ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1132:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1133:1: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1133:1: ( ruleBiimplies )
            // InternalMyDsl.g:1134:2: ruleBiimplies
            {
             before(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalMyDsl.g:1143:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1147:1: ( rule__Primary__Group_0__2__Impl )
            // InternalMyDsl.g:1148:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalMyDsl.g:1154:1: rule__Primary__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1158:1: ( ( ')' ) )
            // InternalMyDsl.g:1159:1: ( ')' )
            {
            // InternalMyDsl.g:1159:1: ( ')' )
            // InternalMyDsl.g:1160:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Start__FormulaAssignment_0"
    // InternalMyDsl.g:1170:1: rule__Start__FormulaAssignment_0 : ( ruleBiimplies ) ;
    public final void rule__Start__FormulaAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1174:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1175:2: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1175:2: ( ruleBiimplies )
            // InternalMyDsl.g:1176:3: ruleBiimplies
            {
             before(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FormulaAssignment_0"


    // $ANTLR start "rule__Biimplies__TypeAssignment_1_1"
    // InternalMyDsl.g:1185:1: rule__Biimplies__TypeAssignment_1_1 : ( ( '<->' ) ) ;
    public final void rule__Biimplies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1189:1: ( ( ( '<->' ) ) )
            // InternalMyDsl.g:1190:2: ( ( '<->' ) )
            {
            // InternalMyDsl.g:1190:2: ( ( '<->' ) )
            // InternalMyDsl.g:1191:3: ( '<->' )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1192:3: ( '<->' )
            // InternalMyDsl.g:1193:4: '<->'
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__TypeAssignment_1_1"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalMyDsl.g:1204:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1208:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:1209:2: ( ruleImplies )
            {
            // InternalMyDsl.g:1209:2: ( ruleImplies )
            // InternalMyDsl.g:1210:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__TypeAssignment_1_1"
    // InternalMyDsl.g:1219:1: rule__Implies__TypeAssignment_1_1 : ( ( '->' ) ) ;
    public final void rule__Implies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1223:1: ( ( ( '->' ) ) )
            // InternalMyDsl.g:1224:2: ( ( '->' ) )
            {
            // InternalMyDsl.g:1224:2: ( ( '->' ) )
            // InternalMyDsl.g:1225:3: ( '->' )
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1226:3: ( '->' )
            // InternalMyDsl.g:1227:4: '->'
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__TypeAssignment_1_1"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalMyDsl.g:1238:1: rule__Implies__RightAssignment_1_2 : ( ruleExcludes ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1242:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:1243:2: ( ruleExcludes )
            {
            // InternalMyDsl.g:1243:2: ( ruleExcludes )
            // InternalMyDsl.g:1244:3: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__Excludes__TypeAssignment_1_1"
    // InternalMyDsl.g:1253:1: rule__Excludes__TypeAssignment_1_1 : ( ( 'nand' ) ) ;
    public final void rule__Excludes__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1257:1: ( ( ( 'nand' ) ) )
            // InternalMyDsl.g:1258:2: ( ( 'nand' ) )
            {
            // InternalMyDsl.g:1258:2: ( ( 'nand' ) )
            // InternalMyDsl.g:1259:3: ( 'nand' )
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            // InternalMyDsl.g:1260:3: ( 'nand' )
            // InternalMyDsl.g:1261:4: 'nand'
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }

             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__TypeAssignment_1_1"


    // $ANTLR start "rule__Excludes__RightAssignment_1_2"
    // InternalMyDsl.g:1272:1: rule__Excludes__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Excludes__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1276:1: ( ( ruleOr ) )
            // InternalMyDsl.g:1277:2: ( ruleOr )
            {
            // InternalMyDsl.g:1277:2: ( ruleOr )
            // InternalMyDsl.g:1278:3: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__RightAssignment_1_2"


    // $ANTLR start "rule__Or__TypeAssignment_1_1"
    // InternalMyDsl.g:1287:1: rule__Or__TypeAssignment_1_1 : ( ( 'or' ) ) ;
    public final void rule__Or__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1291:1: ( ( ( 'or' ) ) )
            // InternalMyDsl.g:1292:2: ( ( 'or' ) )
            {
            // InternalMyDsl.g:1292:2: ( ( 'or' ) )
            // InternalMyDsl.g:1293:3: ( 'or' )
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            // InternalMyDsl.g:1294:3: ( 'or' )
            // InternalMyDsl.g:1295:4: 'or'
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }

             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__TypeAssignment_1_1"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalMyDsl.g:1306:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1310:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1311:2: ( ruleAnd )
            {
            // InternalMyDsl.g:1311:2: ( ruleAnd )
            // InternalMyDsl.g:1312:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__TypeAssignment_1_1"
    // InternalMyDsl.g:1321:1: rule__And__TypeAssignment_1_1 : ( ( 'and' ) ) ;
    public final void rule__And__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1325:1: ( ( ( 'and' ) ) )
            // InternalMyDsl.g:1326:2: ( ( 'and' ) )
            {
            // InternalMyDsl.g:1326:2: ( ( 'and' ) )
            // InternalMyDsl.g:1327:3: ( 'and' )
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            // InternalMyDsl.g:1328:3: ( 'and' )
            // InternalMyDsl.g:1329:4: 'and'
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }

             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__TypeAssignment_1_1"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalMyDsl.g:1340:1: rule__And__RightAssignment_1_2 : ( ruleNot ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1344:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1345:2: ( ruleNot )
            {
            // InternalMyDsl.g:1345:2: ( ruleNot )
            // InternalMyDsl.g:1346:3: ruleNot
            {
             before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Not__TypeAssignment_0_0"
    // InternalMyDsl.g:1355:1: rule__Not__TypeAssignment_0_0 : ( ( 'not' ) ) ;
    public final void rule__Not__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1359:1: ( ( ( 'not' ) ) )
            // InternalMyDsl.g:1360:2: ( ( 'not' ) )
            {
            // InternalMyDsl.g:1360:2: ( ( 'not' ) )
            // InternalMyDsl.g:1361:3: ( 'not' )
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            // InternalMyDsl.g:1362:3: ( 'not' )
            // InternalMyDsl.g:1363:4: 'not'
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }

             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__TypeAssignment_0_0"


    // $ANTLR start "rule__Not__RightAssignment_0_1"
    // InternalMyDsl.g:1374:1: rule__Not__RightAssignment_0_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1378:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1379:2: ( rulePrimary )
            {
            // InternalMyDsl.g:1379:2: ( rulePrimary )
            // InternalMyDsl.g:1380:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_0_1"


    // $ANTLR start "rule__Not__RightAssignment_1"
    // InternalMyDsl.g:1389:1: rule__Not__RightAssignment_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1393:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1394:2: ( rulePrimary )
            {
            // InternalMyDsl.g:1394:2: ( rulePrimary )
            // InternalMyDsl.g:1395:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_1"
    // InternalMyDsl.g:1404:1: rule__Primary__ValueAssignment_1 : ( RULE_TRUE ) ;
    public final void rule__Primary__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1408:1: ( ( RULE_TRUE ) )
            // InternalMyDsl.g:1409:2: ( RULE_TRUE )
            {
            // InternalMyDsl.g:1409:2: ( RULE_TRUE )
            // InternalMyDsl.g:1410:3: RULE_TRUE
            {
             before(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 
            match(input,RULE_TRUE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_2"
    // InternalMyDsl.g:1419:1: rule__Primary__ValueAssignment_2 : ( RULE_FALSE ) ;
    public final void rule__Primary__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1423:1: ( ( RULE_FALSE ) )
            // InternalMyDsl.g:1424:2: ( RULE_FALSE )
            {
            // InternalMyDsl.g:1424:2: ( RULE_FALSE )
            // InternalMyDsl.g:1425:3: RULE_FALSE
            {
             before(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 
            match(input,RULE_FALSE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_2"


    // $ANTLR start "rule__Primary__ValueAssignment_3"
    // InternalMyDsl.g:1434:1: rule__Primary__ValueAssignment_3 : ( RULE_ID ) ;
    public final void rule__Primary__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1438:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1439:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1439:2: ( RULE_ID )
            // InternalMyDsl.g:1440:3: RULE_ID
            {
             before(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000204072L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000204070L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000008000L});

}