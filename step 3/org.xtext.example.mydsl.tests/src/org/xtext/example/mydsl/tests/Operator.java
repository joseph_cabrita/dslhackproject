/**
 * 
 */
package org.xtext.example.mydsl.tests;

/**
 * @author Joseph Cabrita
 *
 */
public interface Operator {
	abstract String type();
}
