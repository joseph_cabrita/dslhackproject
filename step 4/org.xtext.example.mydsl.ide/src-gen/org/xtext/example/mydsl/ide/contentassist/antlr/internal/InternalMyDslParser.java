package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FILE'", "';'", "'('", "')'", "'SAT4J'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=5;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=7;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int RULE_FALSE=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:53:1: entryRuleStart : ruleStart EOF ;
    public final void entryRuleStart() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleStart EOF )
            // InternalMyDsl.g:55:1: ruleStart EOF
            {
             before(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            ruleStart();

            state._fsp--;

             after(grammarAccess.getStartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:62:1: ruleStart : ( ( rule__Start__Alternatives )* ) ;
    public final void ruleStart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Start__Alternatives )* ) )
            // InternalMyDsl.g:67:2: ( ( rule__Start__Alternatives )* )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Start__Alternatives )* )
            // InternalMyDsl.g:68:3: ( rule__Start__Alternatives )*
            {
             before(grammarAccess.getStartAccess().getAlternatives()); 
            // InternalMyDsl.g:69:3: ( rule__Start__Alternatives )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||LA1_0==13||LA1_0==15||LA1_0==23) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:69:4: rule__Start__Alternatives
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Start__Alternatives();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStartAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleSolver"
    // InternalMyDsl.g:78:1: entryRuleSolver : ruleSolver EOF ;
    public final void entryRuleSolver() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleSolver EOF )
            // InternalMyDsl.g:80:1: ruleSolver EOF
            {
             before(grammarAccess.getSolverRule()); 
            pushFollow(FOLLOW_1);
            ruleSolver();

            state._fsp--;

             after(grammarAccess.getSolverRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolver"


    // $ANTLR start "ruleSolver"
    // InternalMyDsl.g:87:1: ruleSolver : ( ( rule__Solver__Alternatives ) ) ;
    public final void ruleSolver() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Solver__Alternatives ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Solver__Alternatives ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Solver__Alternatives ) )
            // InternalMyDsl.g:93:3: ( rule__Solver__Alternatives )
            {
             before(grammarAccess.getSolverAccess().getAlternatives()); 
            // InternalMyDsl.g:94:3: ( rule__Solver__Alternatives )
            // InternalMyDsl.g:94:4: rule__Solver__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolver"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:103:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleBiimplies EOF )
            // InternalMyDsl.g:105:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:112:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__Biimplies__Group__0 )
            // InternalMyDsl.g:119:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:128:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleImplies EOF )
            // InternalMyDsl.g:130:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:137:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Implies__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Implies__Group__0 )
            // InternalMyDsl.g:144:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:153:1: entryRuleExcludes : ruleExcludes EOF ;
    public final void entryRuleExcludes() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleExcludes EOF )
            // InternalMyDsl.g:155:1: ruleExcludes EOF
            {
             before(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getExcludesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:162:1: ruleExcludes : ( ( rule__Excludes__Group__0 ) ) ;
    public final void ruleExcludes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Excludes__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Excludes__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Excludes__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Excludes__Group__0 )
            {
             before(grammarAccess.getExcludesAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Excludes__Group__0 )
            // InternalMyDsl.g:169:4: rule__Excludes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:178:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleOr EOF )
            // InternalMyDsl.g:180:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:187:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__Or__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__Or__Group__0 )
            // InternalMyDsl.g:194:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:203:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleAnd EOF )
            // InternalMyDsl.g:205:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:212:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__And__Group__0 ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__And__Group__0 ) )
            // InternalMyDsl.g:218:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalMyDsl.g:219:3: ( rule__And__Group__0 )
            // InternalMyDsl.g:219:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:228:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( ruleNot EOF )
            // InternalMyDsl.g:230:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:237:1: ruleNot : ( ( rule__Not__Alternatives ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__Not__Alternatives ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__Not__Alternatives ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__Not__Alternatives ) )
            // InternalMyDsl.g:243:3: ( rule__Not__Alternatives )
            {
             before(grammarAccess.getNotAccess().getAlternatives()); 
            // InternalMyDsl.g:244:3: ( rule__Not__Alternatives )
            // InternalMyDsl.g:244:4: rule__Not__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Not__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:253:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( rulePrimary EOF )
            // InternalMyDsl.g:255:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:262:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__Primary__Alternatives ) )
            // InternalMyDsl.g:268:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalMyDsl.g:269:3: ( rule__Primary__Alternatives )
            // InternalMyDsl.g:269:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "rule__Start__Alternatives"
    // InternalMyDsl.g:277:1: rule__Start__Alternatives : ( ( ( rule__Start__Group_0__0 ) ) | ( ( rule__Start__Group_1__0 ) ) );
    public final void rule__Start__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:281:1: ( ( ( rule__Start__Group_0__0 ) ) | ( ( rule__Start__Group_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=RULE_TRUE && LA2_0<=RULE_ID)||LA2_0==15||LA2_0==23) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:282:2: ( ( rule__Start__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:282:2: ( ( rule__Start__Group_0__0 ) )
                    // InternalMyDsl.g:283:3: ( rule__Start__Group_0__0 )
                    {
                     before(grammarAccess.getStartAccess().getGroup_0()); 
                    // InternalMyDsl.g:284:3: ( rule__Start__Group_0__0 )
                    // InternalMyDsl.g:284:4: rule__Start__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Start__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStartAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:288:2: ( ( rule__Start__Group_1__0 ) )
                    {
                    // InternalMyDsl.g:288:2: ( ( rule__Start__Group_1__0 ) )
                    // InternalMyDsl.g:289:3: ( rule__Start__Group_1__0 )
                    {
                     before(grammarAccess.getStartAccess().getGroup_1()); 
                    // InternalMyDsl.g:290:3: ( rule__Start__Group_1__0 )
                    // InternalMyDsl.g:290:4: rule__Start__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Start__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStartAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Alternatives"


    // $ANTLR start "rule__Solver__Alternatives"
    // InternalMyDsl.g:298:1: rule__Solver__Alternatives : ( ( ( rule__Solver__Group_0__0 ) ) | ( ( rule__Solver__ValueAssignment_1 ) ) );
    public final void rule__Solver__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:302:1: ( ( ( rule__Solver__Group_0__0 ) ) | ( ( rule__Solver__ValueAssignment_1 ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==17) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:303:2: ( ( rule__Solver__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:303:2: ( ( rule__Solver__Group_0__0 ) )
                    // InternalMyDsl.g:304:3: ( rule__Solver__Group_0__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_0()); 
                    // InternalMyDsl.g:305:3: ( rule__Solver__Group_0__0 )
                    // InternalMyDsl.g:305:4: rule__Solver__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:309:2: ( ( rule__Solver__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:309:2: ( ( rule__Solver__ValueAssignment_1 ) )
                    // InternalMyDsl.g:310:3: ( rule__Solver__ValueAssignment_1 )
                    {
                     before(grammarAccess.getSolverAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:311:3: ( rule__Solver__ValueAssignment_1 )
                    // InternalMyDsl.g:311:4: rule__Solver__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getValueAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Alternatives"


    // $ANTLR start "rule__Not__Alternatives"
    // InternalMyDsl.g:319:1: rule__Not__Alternatives : ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) );
    public final void rule__Not__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:323:1: ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==23) ) {
                alt4=1;
            }
            else if ( ((LA4_0>=RULE_TRUE && LA4_0<=RULE_ID)||LA4_0==15) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:324:2: ( ( rule__Not__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:324:2: ( ( rule__Not__Group_0__0 ) )
                    // InternalMyDsl.g:325:3: ( rule__Not__Group_0__0 )
                    {
                     before(grammarAccess.getNotAccess().getGroup_0()); 
                    // InternalMyDsl.g:326:3: ( rule__Not__Group_0__0 )
                    // InternalMyDsl.g:326:4: rule__Not__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:330:2: ( ( rule__Not__RightAssignment_1 ) )
                    {
                    // InternalMyDsl.g:330:2: ( ( rule__Not__RightAssignment_1 ) )
                    // InternalMyDsl.g:331:3: ( rule__Not__RightAssignment_1 )
                    {
                     before(grammarAccess.getNotAccess().getRightAssignment_1()); 
                    // InternalMyDsl.g:332:3: ( rule__Not__RightAssignment_1 )
                    // InternalMyDsl.g:332:4: rule__Not__RightAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__RightAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getRightAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalMyDsl.g:340:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:344:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt5=1;
                }
                break;
            case RULE_TRUE:
                {
                alt5=2;
                }
                break;
            case RULE_FALSE:
                {
                alt5=3;
                }
                break;
            case RULE_ID:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:345:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:345:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalMyDsl.g:346:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalMyDsl.g:347:3: ( rule__Primary__Group_0__0 )
                    // InternalMyDsl.g:347:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:351:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:351:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    // InternalMyDsl.g:352:3: ( rule__Primary__ValueAssignment_1 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:353:3: ( rule__Primary__ValueAssignment_1 )
                    // InternalMyDsl.g:353:4: rule__Primary__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:357:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:357:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    // InternalMyDsl.g:358:3: ( rule__Primary__ValueAssignment_2 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:359:3: ( rule__Primary__ValueAssignment_2 )
                    // InternalMyDsl.g:359:4: rule__Primary__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:363:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    {
                    // InternalMyDsl.g:363:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    // InternalMyDsl.g:364:3: ( rule__Primary__ValueAssignment_3 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 
                    // InternalMyDsl.g:365:3: ( rule__Primary__ValueAssignment_3 )
                    // InternalMyDsl.g:365:4: rule__Primary__ValueAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Start__Group_0__0"
    // InternalMyDsl.g:373:1: rule__Start__Group_0__0 : rule__Start__Group_0__0__Impl rule__Start__Group_0__1 ;
    public final void rule__Start__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:377:1: ( rule__Start__Group_0__0__Impl rule__Start__Group_0__1 )
            // InternalMyDsl.g:378:2: rule__Start__Group_0__0__Impl rule__Start__Group_0__1
            {
            pushFollow(FOLLOW_4);
            rule__Start__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__0"


    // $ANTLR start "rule__Start__Group_0__0__Impl"
    // InternalMyDsl.g:385:1: rule__Start__Group_0__0__Impl : ( 'FILE' ) ;
    public final void rule__Start__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:389:1: ( ( 'FILE' ) )
            // InternalMyDsl.g:390:1: ( 'FILE' )
            {
            // InternalMyDsl.g:390:1: ( 'FILE' )
            // InternalMyDsl.g:391:2: 'FILE'
            {
             before(grammarAccess.getStartAccess().getFILEKeyword_0_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getFILEKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__0__Impl"


    // $ANTLR start "rule__Start__Group_0__1"
    // InternalMyDsl.g:400:1: rule__Start__Group_0__1 : rule__Start__Group_0__1__Impl rule__Start__Group_0__2 ;
    public final void rule__Start__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:404:1: ( rule__Start__Group_0__1__Impl rule__Start__Group_0__2 )
            // InternalMyDsl.g:405:2: rule__Start__Group_0__1__Impl rule__Start__Group_0__2
            {
            pushFollow(FOLLOW_5);
            rule__Start__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__1"


    // $ANTLR start "rule__Start__Group_0__1__Impl"
    // InternalMyDsl.g:412:1: rule__Start__Group_0__1__Impl : ( ( rule__Start__FileAssignment_0_1 ) ) ;
    public final void rule__Start__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:416:1: ( ( ( rule__Start__FileAssignment_0_1 ) ) )
            // InternalMyDsl.g:417:1: ( ( rule__Start__FileAssignment_0_1 ) )
            {
            // InternalMyDsl.g:417:1: ( ( rule__Start__FileAssignment_0_1 ) )
            // InternalMyDsl.g:418:2: ( rule__Start__FileAssignment_0_1 )
            {
             before(grammarAccess.getStartAccess().getFileAssignment_0_1()); 
            // InternalMyDsl.g:419:2: ( rule__Start__FileAssignment_0_1 )
            // InternalMyDsl.g:419:3: rule__Start__FileAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Start__FileAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFileAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__1__Impl"


    // $ANTLR start "rule__Start__Group_0__2"
    // InternalMyDsl.g:427:1: rule__Start__Group_0__2 : rule__Start__Group_0__2__Impl ;
    public final void rule__Start__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:431:1: ( rule__Start__Group_0__2__Impl )
            // InternalMyDsl.g:432:2: rule__Start__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__2"


    // $ANTLR start "rule__Start__Group_0__2__Impl"
    // InternalMyDsl.g:438:1: rule__Start__Group_0__2__Impl : ( ';' ) ;
    public final void rule__Start__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:442:1: ( ( ';' ) )
            // InternalMyDsl.g:443:1: ( ';' )
            {
            // InternalMyDsl.g:443:1: ( ';' )
            // InternalMyDsl.g:444:2: ';'
            {
             before(grammarAccess.getStartAccess().getSemicolonKeyword_0_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getSemicolonKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__2__Impl"


    // $ANTLR start "rule__Start__Group_1__0"
    // InternalMyDsl.g:454:1: rule__Start__Group_1__0 : rule__Start__Group_1__0__Impl rule__Start__Group_1__1 ;
    public final void rule__Start__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:458:1: ( rule__Start__Group_1__0__Impl rule__Start__Group_1__1 )
            // InternalMyDsl.g:459:2: rule__Start__Group_1__0__Impl rule__Start__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Start__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__0"


    // $ANTLR start "rule__Start__Group_1__0__Impl"
    // InternalMyDsl.g:466:1: rule__Start__Group_1__0__Impl : ( ( rule__Start__FormulaAssignment_1_0 ) ) ;
    public final void rule__Start__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:470:1: ( ( ( rule__Start__FormulaAssignment_1_0 ) ) )
            // InternalMyDsl.g:471:1: ( ( rule__Start__FormulaAssignment_1_0 ) )
            {
            // InternalMyDsl.g:471:1: ( ( rule__Start__FormulaAssignment_1_0 ) )
            // InternalMyDsl.g:472:2: ( rule__Start__FormulaAssignment_1_0 )
            {
             before(grammarAccess.getStartAccess().getFormulaAssignment_1_0()); 
            // InternalMyDsl.g:473:2: ( rule__Start__FormulaAssignment_1_0 )
            // InternalMyDsl.g:473:3: rule__Start__FormulaAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Start__FormulaAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFormulaAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__0__Impl"


    // $ANTLR start "rule__Start__Group_1__1"
    // InternalMyDsl.g:481:1: rule__Start__Group_1__1 : rule__Start__Group_1__1__Impl ;
    public final void rule__Start__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:485:1: ( rule__Start__Group_1__1__Impl )
            // InternalMyDsl.g:486:2: rule__Start__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__1"


    // $ANTLR start "rule__Start__Group_1__1__Impl"
    // InternalMyDsl.g:492:1: rule__Start__Group_1__1__Impl : ( ( rule__Start__SATAssignment_1_1 ) ) ;
    public final void rule__Start__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:496:1: ( ( ( rule__Start__SATAssignment_1_1 ) ) )
            // InternalMyDsl.g:497:1: ( ( rule__Start__SATAssignment_1_1 ) )
            {
            // InternalMyDsl.g:497:1: ( ( rule__Start__SATAssignment_1_1 ) )
            // InternalMyDsl.g:498:2: ( rule__Start__SATAssignment_1_1 )
            {
             before(grammarAccess.getStartAccess().getSATAssignment_1_1()); 
            // InternalMyDsl.g:499:2: ( rule__Start__SATAssignment_1_1 )
            // InternalMyDsl.g:499:3: rule__Start__SATAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Start__SATAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getSATAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__1__Impl"


    // $ANTLR start "rule__Solver__Group_0__0"
    // InternalMyDsl.g:508:1: rule__Solver__Group_0__0 : rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1 ;
    public final void rule__Solver__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:512:1: ( rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1 )
            // InternalMyDsl.g:513:2: rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1
            {
            pushFollow(FOLLOW_5);
            rule__Solver__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__0"


    // $ANTLR start "rule__Solver__Group_0__0__Impl"
    // InternalMyDsl.g:520:1: rule__Solver__Group_0__0__Impl : ( ( rule__Solver__ValueAssignment_0_0 ) ) ;
    public final void rule__Solver__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:524:1: ( ( ( rule__Solver__ValueAssignment_0_0 ) ) )
            // InternalMyDsl.g:525:1: ( ( rule__Solver__ValueAssignment_0_0 ) )
            {
            // InternalMyDsl.g:525:1: ( ( rule__Solver__ValueAssignment_0_0 ) )
            // InternalMyDsl.g:526:2: ( rule__Solver__ValueAssignment_0_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_0_0()); 
            // InternalMyDsl.g:527:2: ( rule__Solver__ValueAssignment_0_0 )
            // InternalMyDsl.g:527:3: rule__Solver__ValueAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__0__Impl"


    // $ANTLR start "rule__Solver__Group_0__1"
    // InternalMyDsl.g:535:1: rule__Solver__Group_0__1 : rule__Solver__Group_0__1__Impl ;
    public final void rule__Solver__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:539:1: ( rule__Solver__Group_0__1__Impl )
            // InternalMyDsl.g:540:2: rule__Solver__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__1"


    // $ANTLR start "rule__Solver__Group_0__1__Impl"
    // InternalMyDsl.g:546:1: rule__Solver__Group_0__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:550:1: ( ( ';' ) )
            // InternalMyDsl.g:551:1: ( ';' )
            {
            // InternalMyDsl.g:551:1: ( ';' )
            // InternalMyDsl.g:552:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_0_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__1__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalMyDsl.g:562:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:566:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalMyDsl.g:567:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalMyDsl.g:574:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:578:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:579:1: ( ruleImplies )
            {
            // InternalMyDsl.g:579:1: ( ruleImplies )
            // InternalMyDsl.g:580:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalMyDsl.g:589:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:593:1: ( rule__Biimplies__Group__1__Impl )
            // InternalMyDsl.g:594:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalMyDsl.g:600:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:604:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalMyDsl.g:605:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalMyDsl.g:605:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalMyDsl.g:606:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:607:2: ( rule__Biimplies__Group_1__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:607:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalMyDsl.g:616:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:620:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalMyDsl.g:621:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalMyDsl.g:628:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:632:1: ( ( () ) )
            // InternalMyDsl.g:633:1: ( () )
            {
            // InternalMyDsl.g:633:1: ( () )
            // InternalMyDsl.g:634:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalMyDsl.g:635:2: ()
            // InternalMyDsl.g:635:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalMyDsl.g:643:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:647:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalMyDsl.g:648:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalMyDsl.g:655:1: rule__Biimplies__Group_1__1__Impl : ( ( rule__Biimplies__TypeAssignment_1_1 ) ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:659:1: ( ( ( rule__Biimplies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:660:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:660:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:661:2: ( rule__Biimplies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:662:2: ( rule__Biimplies__TypeAssignment_1_1 )
            // InternalMyDsl.g:662:3: rule__Biimplies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalMyDsl.g:670:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:674:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalMyDsl.g:675:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalMyDsl.g:681:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:685:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:686:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:686:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:687:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:688:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalMyDsl.g:688:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalMyDsl.g:697:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:701:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalMyDsl.g:702:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalMyDsl.g:709:1: rule__Implies__Group__0__Impl : ( ruleExcludes ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:713:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:714:1: ( ruleExcludes )
            {
            // InternalMyDsl.g:714:1: ( ruleExcludes )
            // InternalMyDsl.g:715:2: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalMyDsl.g:724:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:728:1: ( rule__Implies__Group__1__Impl )
            // InternalMyDsl.g:729:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalMyDsl.g:735:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:739:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalMyDsl.g:740:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalMyDsl.g:740:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalMyDsl.g:741:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:742:2: ( rule__Implies__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==19) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:742:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalMyDsl.g:751:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:755:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalMyDsl.g:756:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalMyDsl.g:763:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:767:1: ( ( () ) )
            // InternalMyDsl.g:768:1: ( () )
            {
            // InternalMyDsl.g:768:1: ( () )
            // InternalMyDsl.g:769:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalMyDsl.g:770:2: ()
            // InternalMyDsl.g:770:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalMyDsl.g:778:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:782:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalMyDsl.g:783:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalMyDsl.g:790:1: rule__Implies__Group_1__1__Impl : ( ( rule__Implies__TypeAssignment_1_1 ) ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:794:1: ( ( ( rule__Implies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:795:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:795:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:796:2: ( rule__Implies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:797:2: ( rule__Implies__TypeAssignment_1_1 )
            // InternalMyDsl.g:797:3: rule__Implies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Implies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalMyDsl.g:805:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:809:1: ( rule__Implies__Group_1__2__Impl )
            // InternalMyDsl.g:810:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalMyDsl.g:816:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:820:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:821:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:821:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:822:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:823:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalMyDsl.g:823:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__Excludes__Group__0"
    // InternalMyDsl.g:832:1: rule__Excludes__Group__0 : rule__Excludes__Group__0__Impl rule__Excludes__Group__1 ;
    public final void rule__Excludes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:836:1: ( rule__Excludes__Group__0__Impl rule__Excludes__Group__1 )
            // InternalMyDsl.g:837:2: rule__Excludes__Group__0__Impl rule__Excludes__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Excludes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0"


    // $ANTLR start "rule__Excludes__Group__0__Impl"
    // InternalMyDsl.g:844:1: rule__Excludes__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Excludes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:848:1: ( ( ruleOr ) )
            // InternalMyDsl.g:849:1: ( ruleOr )
            {
            // InternalMyDsl.g:849:1: ( ruleOr )
            // InternalMyDsl.g:850:2: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0__Impl"


    // $ANTLR start "rule__Excludes__Group__1"
    // InternalMyDsl.g:859:1: rule__Excludes__Group__1 : rule__Excludes__Group__1__Impl ;
    public final void rule__Excludes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:863:1: ( rule__Excludes__Group__1__Impl )
            // InternalMyDsl.g:864:2: rule__Excludes__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1"


    // $ANTLR start "rule__Excludes__Group__1__Impl"
    // InternalMyDsl.g:870:1: rule__Excludes__Group__1__Impl : ( ( rule__Excludes__Group_1__0 )* ) ;
    public final void rule__Excludes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:874:1: ( ( ( rule__Excludes__Group_1__0 )* ) )
            // InternalMyDsl.g:875:1: ( ( rule__Excludes__Group_1__0 )* )
            {
            // InternalMyDsl.g:875:1: ( ( rule__Excludes__Group_1__0 )* )
            // InternalMyDsl.g:876:2: ( rule__Excludes__Group_1__0 )*
            {
             before(grammarAccess.getExcludesAccess().getGroup_1()); 
            // InternalMyDsl.g:877:2: ( rule__Excludes__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==20) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:877:3: rule__Excludes__Group_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Excludes__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getExcludesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__0"
    // InternalMyDsl.g:886:1: rule__Excludes__Group_1__0 : rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 ;
    public final void rule__Excludes__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:890:1: ( rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 )
            // InternalMyDsl.g:891:2: rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__Excludes__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0"


    // $ANTLR start "rule__Excludes__Group_1__0__Impl"
    // InternalMyDsl.g:898:1: rule__Excludes__Group_1__0__Impl : ( () ) ;
    public final void rule__Excludes__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:902:1: ( ( () ) )
            // InternalMyDsl.g:903:1: ( () )
            {
            // InternalMyDsl.g:903:1: ( () )
            // InternalMyDsl.g:904:2: ()
            {
             before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 
            // InternalMyDsl.g:905:2: ()
            // InternalMyDsl.g:905:3: 
            {
            }

             after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0__Impl"


    // $ANTLR start "rule__Excludes__Group_1__1"
    // InternalMyDsl.g:913:1: rule__Excludes__Group_1__1 : rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 ;
    public final void rule__Excludes__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:917:1: ( rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 )
            // InternalMyDsl.g:918:2: rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Excludes__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1"


    // $ANTLR start "rule__Excludes__Group_1__1__Impl"
    // InternalMyDsl.g:925:1: rule__Excludes__Group_1__1__Impl : ( ( rule__Excludes__TypeAssignment_1_1 ) ) ;
    public final void rule__Excludes__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:929:1: ( ( ( rule__Excludes__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:930:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:930:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:931:2: ( rule__Excludes__TypeAssignment_1_1 )
            {
             before(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:932:2: ( rule__Excludes__TypeAssignment_1_1 )
            // InternalMyDsl.g:932:3: rule__Excludes__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__2"
    // InternalMyDsl.g:940:1: rule__Excludes__Group_1__2 : rule__Excludes__Group_1__2__Impl ;
    public final void rule__Excludes__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:944:1: ( rule__Excludes__Group_1__2__Impl )
            // InternalMyDsl.g:945:2: rule__Excludes__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2"


    // $ANTLR start "rule__Excludes__Group_1__2__Impl"
    // InternalMyDsl.g:951:1: rule__Excludes__Group_1__2__Impl : ( ( rule__Excludes__RightAssignment_1_2 ) ) ;
    public final void rule__Excludes__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:955:1: ( ( ( rule__Excludes__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:956:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:956:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            // InternalMyDsl.g:957:2: ( rule__Excludes__RightAssignment_1_2 )
            {
             before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:958:2: ( rule__Excludes__RightAssignment_1_2 )
            // InternalMyDsl.g:958:3: rule__Excludes__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalMyDsl.g:967:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:971:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalMyDsl.g:972:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalMyDsl.g:979:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:983:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:984:1: ( ruleAnd )
            {
            // InternalMyDsl.g:984:1: ( ruleAnd )
            // InternalMyDsl.g:985:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalMyDsl.g:994:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:998:1: ( rule__Or__Group__1__Impl )
            // InternalMyDsl.g:999:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalMyDsl.g:1005:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1009:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalMyDsl.g:1010:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalMyDsl.g:1010:1: ( ( rule__Or__Group_1__0 )* )
            // InternalMyDsl.g:1011:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalMyDsl.g:1012:2: ( rule__Or__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==21) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:1012:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalMyDsl.g:1021:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1025:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalMyDsl.g:1026:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_14);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalMyDsl.g:1033:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1037:1: ( ( () ) )
            // InternalMyDsl.g:1038:1: ( () )
            {
            // InternalMyDsl.g:1038:1: ( () )
            // InternalMyDsl.g:1039:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalMyDsl.g:1040:2: ()
            // InternalMyDsl.g:1040:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalMyDsl.g:1048:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1052:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalMyDsl.g:1053:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalMyDsl.g:1060:1: rule__Or__Group_1__1__Impl : ( ( rule__Or__TypeAssignment_1_1 ) ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1064:1: ( ( ( rule__Or__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1065:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1065:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1066:2: ( rule__Or__TypeAssignment_1_1 )
            {
             before(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1067:2: ( rule__Or__TypeAssignment_1_1 )
            // InternalMyDsl.g:1067:3: rule__Or__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Or__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalMyDsl.g:1075:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1079:1: ( rule__Or__Group_1__2__Impl )
            // InternalMyDsl.g:1080:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalMyDsl.g:1086:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1090:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1091:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1091:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1092:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1093:2: ( rule__Or__RightAssignment_1_2 )
            // InternalMyDsl.g:1093:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalMyDsl.g:1102:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1106:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalMyDsl.g:1107:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalMyDsl.g:1114:1: rule__And__Group__0__Impl : ( ruleNot ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1118:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1119:1: ( ruleNot )
            {
            // InternalMyDsl.g:1119:1: ( ruleNot )
            // InternalMyDsl.g:1120:2: ruleNot
            {
             before(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalMyDsl.g:1129:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1133:1: ( rule__And__Group__1__Impl )
            // InternalMyDsl.g:1134:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalMyDsl.g:1140:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1144:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalMyDsl.g:1145:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalMyDsl.g:1145:1: ( ( rule__And__Group_1__0 )* )
            // InternalMyDsl.g:1146:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalMyDsl.g:1147:2: ( rule__And__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==22) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:1147:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_17);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalMyDsl.g:1156:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1160:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalMyDsl.g:1161:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_16);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalMyDsl.g:1168:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1172:1: ( ( () ) )
            // InternalMyDsl.g:1173:1: ( () )
            {
            // InternalMyDsl.g:1173:1: ( () )
            // InternalMyDsl.g:1174:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalMyDsl.g:1175:2: ()
            // InternalMyDsl.g:1175:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalMyDsl.g:1183:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1187:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalMyDsl.g:1188:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalMyDsl.g:1195:1: rule__And__Group_1__1__Impl : ( ( rule__And__TypeAssignment_1_1 ) ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1199:1: ( ( ( rule__And__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1200:1: ( ( rule__And__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1200:1: ( ( rule__And__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1201:2: ( rule__And__TypeAssignment_1_1 )
            {
             before(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1202:2: ( rule__And__TypeAssignment_1_1 )
            // InternalMyDsl.g:1202:3: rule__And__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__And__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalMyDsl.g:1210:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1214:1: ( rule__And__Group_1__2__Impl )
            // InternalMyDsl.g:1215:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalMyDsl.g:1221:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1225:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1226:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1226:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1227:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1228:2: ( rule__And__RightAssignment_1_2 )
            // InternalMyDsl.g:1228:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group_0__0"
    // InternalMyDsl.g:1237:1: rule__Not__Group_0__0 : rule__Not__Group_0__0__Impl rule__Not__Group_0__1 ;
    public final void rule__Not__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1241:1: ( rule__Not__Group_0__0__Impl rule__Not__Group_0__1 )
            // InternalMyDsl.g:1242:2: rule__Not__Group_0__0__Impl rule__Not__Group_0__1
            {
            pushFollow(FOLLOW_9);
            rule__Not__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0"


    // $ANTLR start "rule__Not__Group_0__0__Impl"
    // InternalMyDsl.g:1249:1: rule__Not__Group_0__0__Impl : ( ( rule__Not__TypeAssignment_0_0 ) ) ;
    public final void rule__Not__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1253:1: ( ( ( rule__Not__TypeAssignment_0_0 ) ) )
            // InternalMyDsl.g:1254:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            {
            // InternalMyDsl.g:1254:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            // InternalMyDsl.g:1255:2: ( rule__Not__TypeAssignment_0_0 )
            {
             before(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 
            // InternalMyDsl.g:1256:2: ( rule__Not__TypeAssignment_0_0 )
            // InternalMyDsl.g:1256:3: rule__Not__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Not__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0__Impl"


    // $ANTLR start "rule__Not__Group_0__1"
    // InternalMyDsl.g:1264:1: rule__Not__Group_0__1 : rule__Not__Group_0__1__Impl ;
    public final void rule__Not__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1268:1: ( rule__Not__Group_0__1__Impl )
            // InternalMyDsl.g:1269:2: rule__Not__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1"


    // $ANTLR start "rule__Not__Group_0__1__Impl"
    // InternalMyDsl.g:1275:1: rule__Not__Group_0__1__Impl : ( ( rule__Not__RightAssignment_0_1 ) ) ;
    public final void rule__Not__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1279:1: ( ( ( rule__Not__RightAssignment_0_1 ) ) )
            // InternalMyDsl.g:1280:1: ( ( rule__Not__RightAssignment_0_1 ) )
            {
            // InternalMyDsl.g:1280:1: ( ( rule__Not__RightAssignment_0_1 ) )
            // InternalMyDsl.g:1281:2: ( rule__Not__RightAssignment_0_1 )
            {
             before(grammarAccess.getNotAccess().getRightAssignment_0_1()); 
            // InternalMyDsl.g:1282:2: ( rule__Not__RightAssignment_0_1 )
            // InternalMyDsl.g:1282:3: rule__Not__RightAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Not__RightAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getRightAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalMyDsl.g:1291:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1295:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalMyDsl.g:1296:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_9);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalMyDsl.g:1303:1: rule__Primary__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1307:1: ( ( '(' ) )
            // InternalMyDsl.g:1308:1: ( '(' )
            {
            // InternalMyDsl.g:1308:1: ( '(' )
            // InternalMyDsl.g:1309:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalMyDsl.g:1318:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1322:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalMyDsl.g:1323:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_18);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalMyDsl.g:1330:1: rule__Primary__Group_0__1__Impl : ( ruleBiimplies ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1334:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1335:1: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1335:1: ( ruleBiimplies )
            // InternalMyDsl.g:1336:2: ruleBiimplies
            {
             before(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalMyDsl.g:1345:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1349:1: ( rule__Primary__Group_0__2__Impl )
            // InternalMyDsl.g:1350:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalMyDsl.g:1356:1: rule__Primary__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1360:1: ( ( ')' ) )
            // InternalMyDsl.g:1361:1: ( ')' )
            {
            // InternalMyDsl.g:1361:1: ( ')' )
            // InternalMyDsl.g:1362:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Start__FileAssignment_0_1"
    // InternalMyDsl.g:1372:1: rule__Start__FileAssignment_0_1 : ( RULE_STRING ) ;
    public final void rule__Start__FileAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1376:1: ( ( RULE_STRING ) )
            // InternalMyDsl.g:1377:2: ( RULE_STRING )
            {
            // InternalMyDsl.g:1377:2: ( RULE_STRING )
            // InternalMyDsl.g:1378:3: RULE_STRING
            {
             before(grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FileAssignment_0_1"


    // $ANTLR start "rule__Start__FormulaAssignment_1_0"
    // InternalMyDsl.g:1387:1: rule__Start__FormulaAssignment_1_0 : ( ruleBiimplies ) ;
    public final void rule__Start__FormulaAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1391:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1392:2: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1392:2: ( ruleBiimplies )
            // InternalMyDsl.g:1393:3: ruleBiimplies
            {
             before(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FormulaAssignment_1_0"


    // $ANTLR start "rule__Start__SATAssignment_1_1"
    // InternalMyDsl.g:1402:1: rule__Start__SATAssignment_1_1 : ( ruleSolver ) ;
    public final void rule__Start__SATAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1406:1: ( ( ruleSolver ) )
            // InternalMyDsl.g:1407:2: ( ruleSolver )
            {
            // InternalMyDsl.g:1407:2: ( ruleSolver )
            // InternalMyDsl.g:1408:3: ruleSolver
            {
             before(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSolver();

            state._fsp--;

             after(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__SATAssignment_1_1"


    // $ANTLR start "rule__Solver__ValueAssignment_0_0"
    // InternalMyDsl.g:1417:1: rule__Solver__ValueAssignment_0_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__Solver__ValueAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1421:1: ( ( ( 'SAT4J' ) ) )
            // InternalMyDsl.g:1422:2: ( ( 'SAT4J' ) )
            {
            // InternalMyDsl.g:1422:2: ( ( 'SAT4J' ) )
            // InternalMyDsl.g:1423:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 
            // InternalMyDsl.g:1424:3: ( 'SAT4J' )
            // InternalMyDsl.g:1425:4: 'SAT4J'
            {
             before(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_0_0"


    // $ANTLR start "rule__Solver__ValueAssignment_1"
    // InternalMyDsl.g:1436:1: rule__Solver__ValueAssignment_1 : ( ( ';' ) ) ;
    public final void rule__Solver__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1440:1: ( ( ( ';' ) ) )
            // InternalMyDsl.g:1441:2: ( ( ';' ) )
            {
            // InternalMyDsl.g:1441:2: ( ( ';' ) )
            // InternalMyDsl.g:1442:3: ( ';' )
            {
             before(grammarAccess.getSolverAccess().getValueSemicolonKeyword_1_0()); 
            // InternalMyDsl.g:1443:3: ( ';' )
            // InternalMyDsl.g:1444:4: ';'
            {
             before(grammarAccess.getSolverAccess().getValueSemicolonKeyword_1_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueSemicolonKeyword_1_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueSemicolonKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_1"


    // $ANTLR start "rule__Biimplies__TypeAssignment_1_1"
    // InternalMyDsl.g:1455:1: rule__Biimplies__TypeAssignment_1_1 : ( ( '<->' ) ) ;
    public final void rule__Biimplies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1459:1: ( ( ( '<->' ) ) )
            // InternalMyDsl.g:1460:2: ( ( '<->' ) )
            {
            // InternalMyDsl.g:1460:2: ( ( '<->' ) )
            // InternalMyDsl.g:1461:3: ( '<->' )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1462:3: ( '<->' )
            // InternalMyDsl.g:1463:4: '<->'
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__TypeAssignment_1_1"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalMyDsl.g:1474:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1478:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:1479:2: ( ruleImplies )
            {
            // InternalMyDsl.g:1479:2: ( ruleImplies )
            // InternalMyDsl.g:1480:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__TypeAssignment_1_1"
    // InternalMyDsl.g:1489:1: rule__Implies__TypeAssignment_1_1 : ( ( '->' ) ) ;
    public final void rule__Implies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1493:1: ( ( ( '->' ) ) )
            // InternalMyDsl.g:1494:2: ( ( '->' ) )
            {
            // InternalMyDsl.g:1494:2: ( ( '->' ) )
            // InternalMyDsl.g:1495:3: ( '->' )
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1496:3: ( '->' )
            // InternalMyDsl.g:1497:4: '->'
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__TypeAssignment_1_1"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalMyDsl.g:1508:1: rule__Implies__RightAssignment_1_2 : ( ruleExcludes ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1512:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:1513:2: ( ruleExcludes )
            {
            // InternalMyDsl.g:1513:2: ( ruleExcludes )
            // InternalMyDsl.g:1514:3: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__Excludes__TypeAssignment_1_1"
    // InternalMyDsl.g:1523:1: rule__Excludes__TypeAssignment_1_1 : ( ( 'nand' ) ) ;
    public final void rule__Excludes__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1527:1: ( ( ( 'nand' ) ) )
            // InternalMyDsl.g:1528:2: ( ( 'nand' ) )
            {
            // InternalMyDsl.g:1528:2: ( ( 'nand' ) )
            // InternalMyDsl.g:1529:3: ( 'nand' )
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            // InternalMyDsl.g:1530:3: ( 'nand' )
            // InternalMyDsl.g:1531:4: 'nand'
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }

             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__TypeAssignment_1_1"


    // $ANTLR start "rule__Excludes__RightAssignment_1_2"
    // InternalMyDsl.g:1542:1: rule__Excludes__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Excludes__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1546:1: ( ( ruleOr ) )
            // InternalMyDsl.g:1547:2: ( ruleOr )
            {
            // InternalMyDsl.g:1547:2: ( ruleOr )
            // InternalMyDsl.g:1548:3: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__RightAssignment_1_2"


    // $ANTLR start "rule__Or__TypeAssignment_1_1"
    // InternalMyDsl.g:1557:1: rule__Or__TypeAssignment_1_1 : ( ( 'or' ) ) ;
    public final void rule__Or__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1561:1: ( ( ( 'or' ) ) )
            // InternalMyDsl.g:1562:2: ( ( 'or' ) )
            {
            // InternalMyDsl.g:1562:2: ( ( 'or' ) )
            // InternalMyDsl.g:1563:3: ( 'or' )
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            // InternalMyDsl.g:1564:3: ( 'or' )
            // InternalMyDsl.g:1565:4: 'or'
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }

             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__TypeAssignment_1_1"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalMyDsl.g:1576:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1580:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1581:2: ( ruleAnd )
            {
            // InternalMyDsl.g:1581:2: ( ruleAnd )
            // InternalMyDsl.g:1582:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__TypeAssignment_1_1"
    // InternalMyDsl.g:1591:1: rule__And__TypeAssignment_1_1 : ( ( 'and' ) ) ;
    public final void rule__And__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1595:1: ( ( ( 'and' ) ) )
            // InternalMyDsl.g:1596:2: ( ( 'and' ) )
            {
            // InternalMyDsl.g:1596:2: ( ( 'and' ) )
            // InternalMyDsl.g:1597:3: ( 'and' )
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            // InternalMyDsl.g:1598:3: ( 'and' )
            // InternalMyDsl.g:1599:4: 'and'
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }

             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__TypeAssignment_1_1"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalMyDsl.g:1610:1: rule__And__RightAssignment_1_2 : ( ruleNot ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1614:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1615:2: ( ruleNot )
            {
            // InternalMyDsl.g:1615:2: ( ruleNot )
            // InternalMyDsl.g:1616:3: ruleNot
            {
             before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Not__TypeAssignment_0_0"
    // InternalMyDsl.g:1625:1: rule__Not__TypeAssignment_0_0 : ( ( 'not' ) ) ;
    public final void rule__Not__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1629:1: ( ( ( 'not' ) ) )
            // InternalMyDsl.g:1630:2: ( ( 'not' ) )
            {
            // InternalMyDsl.g:1630:2: ( ( 'not' ) )
            // InternalMyDsl.g:1631:3: ( 'not' )
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            // InternalMyDsl.g:1632:3: ( 'not' )
            // InternalMyDsl.g:1633:4: 'not'
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }

             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__TypeAssignment_0_0"


    // $ANTLR start "rule__Not__RightAssignment_0_1"
    // InternalMyDsl.g:1644:1: rule__Not__RightAssignment_0_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1648:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1649:2: ( rulePrimary )
            {
            // InternalMyDsl.g:1649:2: ( rulePrimary )
            // InternalMyDsl.g:1650:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_0_1"


    // $ANTLR start "rule__Not__RightAssignment_1"
    // InternalMyDsl.g:1659:1: rule__Not__RightAssignment_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1663:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1664:2: ( rulePrimary )
            {
            // InternalMyDsl.g:1664:2: ( rulePrimary )
            // InternalMyDsl.g:1665:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_1"
    // InternalMyDsl.g:1674:1: rule__Primary__ValueAssignment_1 : ( RULE_TRUE ) ;
    public final void rule__Primary__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1678:1: ( ( RULE_TRUE ) )
            // InternalMyDsl.g:1679:2: ( RULE_TRUE )
            {
            // InternalMyDsl.g:1679:2: ( RULE_TRUE )
            // InternalMyDsl.g:1680:3: RULE_TRUE
            {
             before(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 
            match(input,RULE_TRUE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_2"
    // InternalMyDsl.g:1689:1: rule__Primary__ValueAssignment_2 : ( RULE_FALSE ) ;
    public final void rule__Primary__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1693:1: ( ( RULE_FALSE ) )
            // InternalMyDsl.g:1694:2: ( RULE_FALSE )
            {
            // InternalMyDsl.g:1694:2: ( RULE_FALSE )
            // InternalMyDsl.g:1695:3: RULE_FALSE
            {
             before(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 
            match(input,RULE_FALSE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_2"


    // $ANTLR start "rule__Primary__ValueAssignment_3"
    // InternalMyDsl.g:1704:1: rule__Primary__ValueAssignment_3 : ( RULE_ID ) ;
    public final void rule__Primary__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1708:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1709:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1709:2: ( RULE_ID )
            // InternalMyDsl.g:1710:3: RULE_ID
            {
             before(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000080A0E2L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000024000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000080A0E0L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000010000L});

}