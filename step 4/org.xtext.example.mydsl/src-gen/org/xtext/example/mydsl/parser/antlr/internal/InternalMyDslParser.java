package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FILE'", "';'", "'SAT4J'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'", "'('", "')'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=5;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=7;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int RULE_FALSE=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Start";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:64:1: entryRuleStart returns [EObject current=null] : iv_ruleStart= ruleStart EOF ;
    public final EObject entryRuleStart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStart = null;


        try {
            // InternalMyDsl.g:64:46: (iv_ruleStart= ruleStart EOF )
            // InternalMyDsl.g:65:2: iv_ruleStart= ruleStart EOF
            {
             newCompositeNode(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStart=ruleStart();

            state._fsp--;

             current =iv_ruleStart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:71:1: ruleStart returns [EObject current=null] : ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';' ) | ( ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) ) ) )* ;
    public final EObject ruleStart() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_File_1_0=null;
        Token otherlv_2=null;
        EObject lv_formula_3_0 = null;

        EObject lv_SAT_4_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';' ) | ( ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) ) ) )* )
            // InternalMyDsl.g:78:2: ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';' ) | ( ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) ) ) )*
            {
            // InternalMyDsl.g:78:2: ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';' ) | ( ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) ) ) )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13) ) {
                    alt1=1;
                }
                else if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||(LA1_0>=21 && LA1_0<=22)) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:79:3: (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';' )
            	    {
            	    // InternalMyDsl.g:79:3: (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';' )
            	    // InternalMyDsl.g:80:4: otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) otherlv_2= ';'
            	    {
            	    otherlv_0=(Token)match(input,13,FOLLOW_3); 

            	    				newLeafNode(otherlv_0, grammarAccess.getStartAccess().getFILEKeyword_0_0());
            	    			
            	    // InternalMyDsl.g:84:4: ( (lv_File_1_0= RULE_STRING ) )
            	    // InternalMyDsl.g:85:5: (lv_File_1_0= RULE_STRING )
            	    {
            	    // InternalMyDsl.g:85:5: (lv_File_1_0= RULE_STRING )
            	    // InternalMyDsl.g:86:6: lv_File_1_0= RULE_STRING
            	    {
            	    lv_File_1_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

            	    						newLeafNode(lv_File_1_0, grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getStartRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"File",
            	    							lv_File_1_0,
            	    							"org.eclipse.xtext.common.Terminals.STRING");
            	    					

            	    }


            	    }

            	    otherlv_2=(Token)match(input,14,FOLLOW_5); 

            	    				newLeafNode(otherlv_2, grammarAccess.getStartAccess().getSemicolonKeyword_0_2());
            	    			

            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:108:3: ( ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) ) )
            	    {
            	    // InternalMyDsl.g:108:3: ( ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) ) )
            	    // InternalMyDsl.g:109:4: ( (lv_formula_3_0= ruleBiimplies ) ) ( (lv_SAT_4_0= ruleSolver ) )
            	    {
            	    // InternalMyDsl.g:109:4: ( (lv_formula_3_0= ruleBiimplies ) )
            	    // InternalMyDsl.g:110:5: (lv_formula_3_0= ruleBiimplies )
            	    {
            	    // InternalMyDsl.g:110:5: (lv_formula_3_0= ruleBiimplies )
            	    // InternalMyDsl.g:111:6: lv_formula_3_0= ruleBiimplies
            	    {

            	    						newCompositeNode(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_formula_3_0=ruleBiimplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStartRule());
            	    						}
            	    						add(
            	    							current,
            	    							"formula",
            	    							lv_formula_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Biimplies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:128:4: ( (lv_SAT_4_0= ruleSolver ) )
            	    // InternalMyDsl.g:129:5: (lv_SAT_4_0= ruleSolver )
            	    {
            	    // InternalMyDsl.g:129:5: (lv_SAT_4_0= ruleSolver )
            	    // InternalMyDsl.g:130:6: lv_SAT_4_0= ruleSolver
            	    {

            	    						newCompositeNode(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_SAT_4_0=ruleSolver();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStartRule());
            	    						}
            	    						add(
            	    							current,
            	    							"SAT",
            	    							lv_SAT_4_0,
            	    							"org.xtext.example.mydsl.MyDsl.Solver");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleSolver"
    // InternalMyDsl.g:152:1: entryRuleSolver returns [EObject current=null] : iv_ruleSolver= ruleSolver EOF ;
    public final EObject entryRuleSolver() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSolver = null;


        try {
            // InternalMyDsl.g:152:47: (iv_ruleSolver= ruleSolver EOF )
            // InternalMyDsl.g:153:2: iv_ruleSolver= ruleSolver EOF
            {
             newCompositeNode(grammarAccess.getSolverRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSolver=ruleSolver();

            state._fsp--;

             current =iv_ruleSolver; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSolver"


    // $ANTLR start "ruleSolver"
    // InternalMyDsl.g:159:1: ruleSolver returns [EObject current=null] : ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( (lv_value_2_0= ';' ) ) ) ;
    public final EObject ruleSolver() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;


        	enterRule();

        try {
            // InternalMyDsl.g:165:2: ( ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( (lv_value_2_0= ';' ) ) ) )
            // InternalMyDsl.g:166:2: ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( (lv_value_2_0= ';' ) ) )
            {
            // InternalMyDsl.g:166:2: ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( (lv_value_2_0= ';' ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==15) ) {
                alt2=1;
            }
            else if ( (LA2_0==14) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:167:3: ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' )
                    {
                    // InternalMyDsl.g:167:3: ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' )
                    // InternalMyDsl.g:168:4: ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';'
                    {
                    // InternalMyDsl.g:168:4: ( (lv_value_0_0= 'SAT4J' ) )
                    // InternalMyDsl.g:169:5: (lv_value_0_0= 'SAT4J' )
                    {
                    // InternalMyDsl.g:169:5: (lv_value_0_0= 'SAT4J' )
                    // InternalMyDsl.g:170:6: lv_value_0_0= 'SAT4J'
                    {
                    lv_value_0_0=(Token)match(input,15,FOLLOW_4); 

                    						newLeafNode(lv_value_0_0, grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSolverRule());
                    						}
                    						setWithLastConsumed(current, "value", lv_value_0_0, "SAT4J");
                    					

                    }


                    }

                    otherlv_1=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_1, grammarAccess.getSolverAccess().getSemicolonKeyword_0_1());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:188:3: ( (lv_value_2_0= ';' ) )
                    {
                    // InternalMyDsl.g:188:3: ( (lv_value_2_0= ';' ) )
                    // InternalMyDsl.g:189:4: (lv_value_2_0= ';' )
                    {
                    // InternalMyDsl.g:189:4: (lv_value_2_0= ';' )
                    // InternalMyDsl.g:190:5: lv_value_2_0= ';'
                    {
                    lv_value_2_0=(Token)match(input,14,FOLLOW_2); 

                    					newLeafNode(lv_value_2_0, grammarAccess.getSolverAccess().getValueSemicolonKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSolverRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_2_0, ";");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSolver"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:206:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;


        try {
            // InternalMyDsl.g:206:50: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalMyDsl.g:207:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:213:1: ruleBiimplies returns [EObject current=null] : (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Implies_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:219:2: ( (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* ) )
            // InternalMyDsl.g:220:2: (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* )
            {
            // InternalMyDsl.g:220:2: (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* )
            // InternalMyDsl.g:221:3: this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )*
            {

            			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
            		
            pushFollow(FOLLOW_8);
            this_Implies_0=ruleImplies();

            state._fsp--;


            			current = this_Implies_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:229:3: ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:230:4: () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) )
            	    {
            	    // InternalMyDsl.g:230:4: ()
            	    // InternalMyDsl.g:231:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:237:4: ( (lv_type_2_0= '<->' ) )
            	    // InternalMyDsl.g:238:5: (lv_type_2_0= '<->' )
            	    {
            	    // InternalMyDsl.g:238:5: (lv_type_2_0= '<->' )
            	    // InternalMyDsl.g:239:6: lv_type_2_0= '<->'
            	    {
            	    lv_type_2_0=(Token)match(input,16,FOLLOW_9); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBiimpliesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "<->");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:251:4: ( (lv_right_3_0= ruleImplies ) )
            	    // InternalMyDsl.g:252:5: (lv_right_3_0= ruleImplies )
            	    {
            	    // InternalMyDsl.g:252:5: (lv_right_3_0= ruleImplies )
            	    // InternalMyDsl.g:253:6: lv_right_3_0= ruleImplies
            	    {

            	    						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_right_3_0=ruleImplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBiimpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Implies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:275:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalMyDsl.g:275:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalMyDsl.g:276:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:282:1: ruleImplies returns [EObject current=null] : (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Excludes_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:288:2: ( (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* ) )
            // InternalMyDsl.g:289:2: (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* )
            {
            // InternalMyDsl.g:289:2: (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* )
            // InternalMyDsl.g:290:3: this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0());
            		
            pushFollow(FOLLOW_10);
            this_Excludes_0=ruleExcludes();

            state._fsp--;


            			current = this_Excludes_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:298:3: ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:299:4: () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) )
            	    {
            	    // InternalMyDsl.g:299:4: ()
            	    // InternalMyDsl.g:300:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:306:4: ( (lv_type_2_0= '->' ) )
            	    // InternalMyDsl.g:307:5: (lv_type_2_0= '->' )
            	    {
            	    // InternalMyDsl.g:307:5: (lv_type_2_0= '->' )
            	    // InternalMyDsl.g:308:6: lv_type_2_0= '->'
            	    {
            	    lv_type_2_0=(Token)match(input,17,FOLLOW_11); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getImpliesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "->");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:320:4: ( (lv_right_3_0= ruleExcludes ) )
            	    // InternalMyDsl.g:321:5: (lv_right_3_0= ruleExcludes )
            	    {
            	    // InternalMyDsl.g:321:5: (lv_right_3_0= ruleExcludes )
            	    // InternalMyDsl.g:322:6: lv_right_3_0= ruleExcludes
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_10);
            	    lv_right_3_0=ruleExcludes();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Excludes");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:344:1: entryRuleExcludes returns [EObject current=null] : iv_ruleExcludes= ruleExcludes EOF ;
    public final EObject entryRuleExcludes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExcludes = null;


        try {
            // InternalMyDsl.g:344:49: (iv_ruleExcludes= ruleExcludes EOF )
            // InternalMyDsl.g:345:2: iv_ruleExcludes= ruleExcludes EOF
            {
             newCompositeNode(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExcludes=ruleExcludes();

            state._fsp--;

             current =iv_ruleExcludes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:351:1: ruleExcludes returns [EObject current=null] : (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* ) ;
    public final EObject ruleExcludes() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Or_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:357:2: ( (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* ) )
            // InternalMyDsl.g:358:2: (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* )
            {
            // InternalMyDsl.g:358:2: (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* )
            // InternalMyDsl.g:359:3: this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )*
            {

            			newCompositeNode(grammarAccess.getExcludesAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_12);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:367:3: ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:368:4: () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) )
            	    {
            	    // InternalMyDsl.g:368:4: ()
            	    // InternalMyDsl.g:369:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:375:4: ( (lv_type_2_0= 'nand' ) )
            	    // InternalMyDsl.g:376:5: (lv_type_2_0= 'nand' )
            	    {
            	    // InternalMyDsl.g:376:5: (lv_type_2_0= 'nand' )
            	    // InternalMyDsl.g:377:6: lv_type_2_0= 'nand'
            	    {
            	    lv_type_2_0=(Token)match(input,18,FOLLOW_13); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getExcludesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "nand");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:389:4: ( (lv_right_3_0= ruleOr ) )
            	    // InternalMyDsl.g:390:5: (lv_right_3_0= ruleOr )
            	    {
            	    // InternalMyDsl.g:390:5: (lv_right_3_0= ruleOr )
            	    // InternalMyDsl.g:391:6: lv_right_3_0= ruleOr
            	    {

            	    						newCompositeNode(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_right_3_0=ruleOr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExcludesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Or");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:413:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalMyDsl.g:413:43: (iv_ruleOr= ruleOr EOF )
            // InternalMyDsl.g:414:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:420:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:426:2: ( (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalMyDsl.g:427:2: (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalMyDsl.g:427:2: (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalMyDsl.g:428:3: this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_14);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:436:3: ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:437:4: () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalMyDsl.g:437:4: ()
            	    // InternalMyDsl.g:438:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:444:4: ( (lv_type_2_0= 'or' ) )
            	    // InternalMyDsl.g:445:5: (lv_type_2_0= 'or' )
            	    {
            	    // InternalMyDsl.g:445:5: (lv_type_2_0= 'or' )
            	    // InternalMyDsl.g:446:6: lv_type_2_0= 'or'
            	    {
            	    lv_type_2_0=(Token)match(input,19,FOLLOW_15); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getOrRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "or");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:458:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalMyDsl.g:459:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalMyDsl.g:459:5: (lv_right_3_0= ruleAnd )
            	    // InternalMyDsl.g:460:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_14);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:482:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalMyDsl.g:482:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalMyDsl.g:483:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:489:1: ruleAnd returns [EObject current=null] : (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Not_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:495:2: ( (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* ) )
            // InternalMyDsl.g:496:2: (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* )
            {
            // InternalMyDsl.g:496:2: (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* )
            // InternalMyDsl.g:497:3: this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getNotParserRuleCall_0());
            		
            pushFollow(FOLLOW_16);
            this_Not_0=ruleNot();

            state._fsp--;


            			current = this_Not_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:505:3: ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==20) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:506:4: () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) )
            	    {
            	    // InternalMyDsl.g:506:4: ()
            	    // InternalMyDsl.g:507:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:513:4: ( (lv_type_2_0= 'and' ) )
            	    // InternalMyDsl.g:514:5: (lv_type_2_0= 'and' )
            	    {
            	    // InternalMyDsl.g:514:5: (lv_type_2_0= 'and' )
            	    // InternalMyDsl.g:515:6: lv_type_2_0= 'and'
            	    {
            	    lv_type_2_0=(Token)match(input,20,FOLLOW_17); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getAndRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "and");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:527:4: ( (lv_right_3_0= ruleNot ) )
            	    // InternalMyDsl.g:528:5: (lv_right_3_0= ruleNot )
            	    {
            	    // InternalMyDsl.g:528:5: (lv_right_3_0= ruleNot )
            	    // InternalMyDsl.g:529:6: lv_right_3_0= ruleNot
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_16);
            	    lv_right_3_0=ruleNot();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Not");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:551:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalMyDsl.g:551:44: (iv_ruleNot= ruleNot EOF )
            // InternalMyDsl.g:552:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:558:1: ruleNot returns [EObject current=null] : ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        EObject lv_right_1_0 = null;

        EObject lv_right_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:564:2: ( ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) ) )
            // InternalMyDsl.g:565:2: ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) )
            {
            // InternalMyDsl.g:565:2: ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==21) ) {
                alt8=1;
            }
            else if ( ((LA8_0>=RULE_TRUE && LA8_0<=RULE_ID)||LA8_0==22) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalMyDsl.g:566:3: ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) )
                    {
                    // InternalMyDsl.g:566:3: ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) )
                    // InternalMyDsl.g:567:4: ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) )
                    {
                    // InternalMyDsl.g:567:4: ( (lv_type_0_0= 'not' ) )
                    // InternalMyDsl.g:568:5: (lv_type_0_0= 'not' )
                    {
                    // InternalMyDsl.g:568:5: (lv_type_0_0= 'not' )
                    // InternalMyDsl.g:569:6: lv_type_0_0= 'not'
                    {
                    lv_type_0_0=(Token)match(input,21,FOLLOW_18); 

                    						newLeafNode(lv_type_0_0, grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getNotRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_0_0, "not");
                    					

                    }


                    }

                    // InternalMyDsl.g:581:4: ( (lv_right_1_0= rulePrimary ) )
                    // InternalMyDsl.g:582:5: (lv_right_1_0= rulePrimary )
                    {
                    // InternalMyDsl.g:582:5: (lv_right_1_0= rulePrimary )
                    // InternalMyDsl.g:583:6: lv_right_1_0= rulePrimary
                    {

                    						newCompositeNode(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_right_1_0=rulePrimary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNotRule());
                    						}
                    						set(
                    							current,
                    							"right",
                    							lv_right_1_0,
                    							"org.xtext.example.mydsl.MyDsl.Primary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:602:3: ( (lv_right_2_0= rulePrimary ) )
                    {
                    // InternalMyDsl.g:602:3: ( (lv_right_2_0= rulePrimary ) )
                    // InternalMyDsl.g:603:4: (lv_right_2_0= rulePrimary )
                    {
                    // InternalMyDsl.g:603:4: (lv_right_2_0= rulePrimary )
                    // InternalMyDsl.g:604:5: lv_right_2_0= rulePrimary
                    {

                    					newCompositeNode(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_right_2_0=rulePrimary();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getNotRule());
                    					}
                    					set(
                    						current,
                    						"right",
                    						lv_right_2_0,
                    						"org.xtext.example.mydsl.MyDsl.Primary");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:625:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalMyDsl.g:625:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalMyDsl.g:626:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:632:1: rulePrimary returns [EObject current=null] : ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token lv_value_4_0=null;
        Token lv_value_5_0=null;
        EObject this_Biimplies_1 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:638:2: ( ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) ) )
            // InternalMyDsl.g:639:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) )
            {
            // InternalMyDsl.g:639:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt9=1;
                }
                break;
            case RULE_TRUE:
                {
                alt9=2;
                }
                break;
            case RULE_FALSE:
                {
                alt9=3;
                }
                break;
            case RULE_ID:
                {
                alt9=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:640:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    {
                    // InternalMyDsl.g:640:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    // InternalMyDsl.g:641:4: otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,22,FOLLOW_19); 

                    				newLeafNode(otherlv_0, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_20);
                    this_Biimplies_1=ruleBiimplies();

                    state._fsp--;


                    				current = this_Biimplies_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,23,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:659:3: ( (lv_value_3_0= RULE_TRUE ) )
                    {
                    // InternalMyDsl.g:659:3: ( (lv_value_3_0= RULE_TRUE ) )
                    // InternalMyDsl.g:660:4: (lv_value_3_0= RULE_TRUE )
                    {
                    // InternalMyDsl.g:660:4: (lv_value_3_0= RULE_TRUE )
                    // InternalMyDsl.g:661:5: lv_value_3_0= RULE_TRUE
                    {
                    lv_value_3_0=(Token)match(input,RULE_TRUE,FOLLOW_2); 

                    					newLeafNode(lv_value_3_0, grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_3_0,
                    						"org.xtext.example.mydsl.MyDsl.TRUE");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:678:3: ( (lv_value_4_0= RULE_FALSE ) )
                    {
                    // InternalMyDsl.g:678:3: ( (lv_value_4_0= RULE_FALSE ) )
                    // InternalMyDsl.g:679:4: (lv_value_4_0= RULE_FALSE )
                    {
                    // InternalMyDsl.g:679:4: (lv_value_4_0= RULE_FALSE )
                    // InternalMyDsl.g:680:5: lv_value_4_0= RULE_FALSE
                    {
                    lv_value_4_0=(Token)match(input,RULE_FALSE,FOLLOW_2); 

                    					newLeafNode(lv_value_4_0, grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_4_0,
                    						"org.xtext.example.mydsl.MyDsl.FALSE");
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:697:3: ( (lv_value_5_0= RULE_ID ) )
                    {
                    // InternalMyDsl.g:697:3: ( (lv_value_5_0= RULE_ID ) )
                    // InternalMyDsl.g:698:4: (lv_value_5_0= RULE_ID )
                    {
                    // InternalMyDsl.g:698:4: (lv_value_5_0= RULE_ID )
                    // InternalMyDsl.g:699:5: lv_value_5_0= RULE_ID
                    {
                    lv_value_5_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_value_5_0, grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_5_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000006020E2L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000060E0E2L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000000006120E0L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000006220E0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x00000000006420E0L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x00000000006820E0L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000007020E0L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00000000006020E0L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000E020E0L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000800000L});

}