package org.xtext.example.mydsl.tests;

public class Value implements Operator 
{
	int value;
	public Value(int _data)
	{
		value = _data;
	}
	
	public int getValue()
	{
		return value;
	}
	
	public void setValue(int _data)
	{
		value = _data;
	}
	
	public String type()
	{
		return "Value";
	}
}
