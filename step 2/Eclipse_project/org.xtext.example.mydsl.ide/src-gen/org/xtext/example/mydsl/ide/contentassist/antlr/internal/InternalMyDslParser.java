package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'True'", "'False'", "';'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'", "'('", "')'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:53:1: entryRuleStart : ruleStart EOF ;
    public final void entryRuleStart() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleStart EOF )
            // InternalMyDsl.g:55:1: ruleStart EOF
            {
             before(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            ruleStart();

            state._fsp--;

             after(grammarAccess.getStartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:62:1: ruleStart : ( ( rule__Start__Group__0 )* ) ;
    public final void ruleStart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Start__Group__0 )* ) )
            // InternalMyDsl.g:67:2: ( ( rule__Start__Group__0 )* )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Start__Group__0 )* )
            // InternalMyDsl.g:68:3: ( rule__Start__Group__0 )*
            {
             before(grammarAccess.getStartAccess().getGroup()); 
            // InternalMyDsl.g:69:3: ( rule__Start__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID||(LA1_0>=11 && LA1_0<=12)||(LA1_0>=19 && LA1_0<=20)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:69:4: rule__Start__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Start__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStartAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:78:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleBiimplies EOF )
            // InternalMyDsl.g:80:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:87:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalMyDsl.g:93:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalMyDsl.g:94:3: ( rule__Biimplies__Group__0 )
            // InternalMyDsl.g:94:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:103:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleImplies EOF )
            // InternalMyDsl.g:105:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:112:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__Implies__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__Implies__Group__0 )
            // InternalMyDsl.g:119:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:128:1: entryRuleExcludes : ruleExcludes EOF ;
    public final void entryRuleExcludes() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleExcludes EOF )
            // InternalMyDsl.g:130:1: ruleExcludes EOF
            {
             before(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getExcludesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:137:1: ruleExcludes : ( ( rule__Excludes__Group__0 ) ) ;
    public final void ruleExcludes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Excludes__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Excludes__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Excludes__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Excludes__Group__0 )
            {
             before(grammarAccess.getExcludesAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Excludes__Group__0 )
            // InternalMyDsl.g:144:4: rule__Excludes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:153:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleOr EOF )
            // InternalMyDsl.g:155:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:162:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Or__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Or__Group__0 )
            // InternalMyDsl.g:169:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:178:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleAnd EOF )
            // InternalMyDsl.g:180:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:187:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__And__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__And__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__And__Group__0 )
            // InternalMyDsl.g:194:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:203:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleNot EOF )
            // InternalMyDsl.g:205:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:212:1: ruleNot : ( ( rule__Not__Alternatives ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__Not__Alternatives ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__Not__Alternatives ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__Not__Alternatives ) )
            // InternalMyDsl.g:218:3: ( rule__Not__Alternatives )
            {
             before(grammarAccess.getNotAccess().getAlternatives()); 
            // InternalMyDsl.g:219:3: ( rule__Not__Alternatives )
            // InternalMyDsl.g:219:4: rule__Not__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Not__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:228:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( rulePrimary EOF )
            // InternalMyDsl.g:230:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:237:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__Primary__Alternatives ) )
            // InternalMyDsl.g:243:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalMyDsl.g:244:3: ( rule__Primary__Alternatives )
            // InternalMyDsl.g:244:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleBoolean"
    // InternalMyDsl.g:253:1: entryRuleBoolean : ruleBoolean EOF ;
    public final void entryRuleBoolean() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( ruleBoolean EOF )
            // InternalMyDsl.g:255:1: ruleBoolean EOF
            {
             before(grammarAccess.getBooleanRule()); 
            pushFollow(FOLLOW_1);
            ruleBoolean();

            state._fsp--;

             after(grammarAccess.getBooleanRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalMyDsl.g:262:1: ruleBoolean : ( ( rule__Boolean__Alternatives ) ) ;
    public final void ruleBoolean() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__Boolean__Alternatives ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__Boolean__Alternatives ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__Boolean__Alternatives ) )
            // InternalMyDsl.g:268:3: ( rule__Boolean__Alternatives )
            {
             before(grammarAccess.getBooleanAccess().getAlternatives()); 
            // InternalMyDsl.g:269:3: ( rule__Boolean__Alternatives )
            // InternalMyDsl.g:269:4: rule__Boolean__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Boolean__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "rule__Not__Alternatives"
    // InternalMyDsl.g:277:1: rule__Not__Alternatives : ( ( ( rule__Not__Group_0__0 ) ) | ( rulePrimary ) );
    public final void rule__Not__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:281:1: ( ( ( rule__Not__Group_0__0 ) ) | ( rulePrimary ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==19) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID||(LA2_0>=11 && LA2_0<=12)||LA2_0==20) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:282:2: ( ( rule__Not__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:282:2: ( ( rule__Not__Group_0__0 ) )
                    // InternalMyDsl.g:283:3: ( rule__Not__Group_0__0 )
                    {
                     before(grammarAccess.getNotAccess().getGroup_0()); 
                    // InternalMyDsl.g:284:3: ( rule__Not__Group_0__0 )
                    // InternalMyDsl.g:284:4: rule__Not__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:288:2: ( rulePrimary )
                    {
                    // InternalMyDsl.g:288:2: ( rulePrimary )
                    // InternalMyDsl.g:289:3: rulePrimary
                    {
                     before(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary();

                    state._fsp--;

                     after(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalMyDsl.g:298:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:302:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt3=1;
                }
                break;
            case 11:
            case 12:
                {
                alt3=2;
                }
                break;
            case RULE_ID:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:303:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:303:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalMyDsl.g:304:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalMyDsl.g:305:3: ( rule__Primary__Group_0__0 )
                    // InternalMyDsl.g:305:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:309:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:309:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    // InternalMyDsl.g:310:3: ( rule__Primary__ValueAssignment_1 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:311:3: ( rule__Primary__ValueAssignment_1 )
                    // InternalMyDsl.g:311:4: rule__Primary__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:315:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:315:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    // InternalMyDsl.g:316:3: ( rule__Primary__ValueAssignment_2 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:317:3: ( rule__Primary__ValueAssignment_2 )
                    // InternalMyDsl.g:317:4: rule__Primary__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Boolean__Alternatives"
    // InternalMyDsl.g:325:1: rule__Boolean__Alternatives : ( ( 'True' ) | ( 'False' ) );
    public final void rule__Boolean__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:329:1: ( ( 'True' ) | ( 'False' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==11) ) {
                alt4=1;
            }
            else if ( (LA4_0==12) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:330:2: ( 'True' )
                    {
                    // InternalMyDsl.g:330:2: ( 'True' )
                    // InternalMyDsl.g:331:3: 'True'
                    {
                     before(grammarAccess.getBooleanAccess().getTrueKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getBooleanAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:336:2: ( 'False' )
                    {
                    // InternalMyDsl.g:336:2: ( 'False' )
                    // InternalMyDsl.g:337:3: 'False'
                    {
                     before(grammarAccess.getBooleanAccess().getFalseKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getBooleanAccess().getFalseKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Boolean__Alternatives"


    // $ANTLR start "rule__Start__Group__0"
    // InternalMyDsl.g:346:1: rule__Start__Group__0 : rule__Start__Group__0__Impl rule__Start__Group__1 ;
    public final void rule__Start__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:350:1: ( rule__Start__Group__0__Impl rule__Start__Group__1 )
            // InternalMyDsl.g:351:2: rule__Start__Group__0__Impl rule__Start__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Start__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__0"


    // $ANTLR start "rule__Start__Group__0__Impl"
    // InternalMyDsl.g:358:1: rule__Start__Group__0__Impl : ( ( rule__Start__FormulaAssignment_0 ) ) ;
    public final void rule__Start__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:362:1: ( ( ( rule__Start__FormulaAssignment_0 ) ) )
            // InternalMyDsl.g:363:1: ( ( rule__Start__FormulaAssignment_0 ) )
            {
            // InternalMyDsl.g:363:1: ( ( rule__Start__FormulaAssignment_0 ) )
            // InternalMyDsl.g:364:2: ( rule__Start__FormulaAssignment_0 )
            {
             before(grammarAccess.getStartAccess().getFormulaAssignment_0()); 
            // InternalMyDsl.g:365:2: ( rule__Start__FormulaAssignment_0 )
            // InternalMyDsl.g:365:3: rule__Start__FormulaAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Start__FormulaAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFormulaAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__0__Impl"


    // $ANTLR start "rule__Start__Group__1"
    // InternalMyDsl.g:373:1: rule__Start__Group__1 : rule__Start__Group__1__Impl ;
    public final void rule__Start__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:377:1: ( rule__Start__Group__1__Impl )
            // InternalMyDsl.g:378:2: rule__Start__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__1"


    // $ANTLR start "rule__Start__Group__1__Impl"
    // InternalMyDsl.g:384:1: rule__Start__Group__1__Impl : ( ';' ) ;
    public final void rule__Start__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:388:1: ( ( ';' ) )
            // InternalMyDsl.g:389:1: ( ';' )
            {
            // InternalMyDsl.g:389:1: ( ';' )
            // InternalMyDsl.g:390:2: ';'
            {
             before(grammarAccess.getStartAccess().getSemicolonKeyword_1()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalMyDsl.g:400:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:404:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalMyDsl.g:405:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalMyDsl.g:412:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:416:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:417:1: ( ruleImplies )
            {
            // InternalMyDsl.g:417:1: ( ruleImplies )
            // InternalMyDsl.g:418:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalMyDsl.g:427:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:431:1: ( rule__Biimplies__Group__1__Impl )
            // InternalMyDsl.g:432:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalMyDsl.g:438:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:442:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalMyDsl.g:443:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalMyDsl.g:443:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalMyDsl.g:444:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:445:2: ( rule__Biimplies__Group_1__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==14) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:445:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalMyDsl.g:454:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:458:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalMyDsl.g:459:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_5);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalMyDsl.g:466:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:470:1: ( ( () ) )
            // InternalMyDsl.g:471:1: ( () )
            {
            // InternalMyDsl.g:471:1: ( () )
            // InternalMyDsl.g:472:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalMyDsl.g:473:2: ()
            // InternalMyDsl.g:473:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalMyDsl.g:481:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:485:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalMyDsl.g:486:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalMyDsl.g:493:1: rule__Biimplies__Group_1__1__Impl : ( '<->' ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:497:1: ( ( '<->' ) )
            // InternalMyDsl.g:498:1: ( '<->' )
            {
            // InternalMyDsl.g:498:1: ( '<->' )
            // InternalMyDsl.g:499:2: '<->'
            {
             before(grammarAccess.getBiimpliesAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalMyDsl.g:508:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:512:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalMyDsl.g:513:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalMyDsl.g:519:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:523:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:524:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:524:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:525:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:526:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalMyDsl.g:526:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalMyDsl.g:535:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:539:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalMyDsl.g:540:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalMyDsl.g:547:1: rule__Implies__Group__0__Impl : ( ruleExcludes ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:551:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:552:1: ( ruleExcludes )
            {
            // InternalMyDsl.g:552:1: ( ruleExcludes )
            // InternalMyDsl.g:553:2: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalMyDsl.g:562:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:566:1: ( rule__Implies__Group__1__Impl )
            // InternalMyDsl.g:567:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalMyDsl.g:573:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:577:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalMyDsl.g:578:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalMyDsl.g:578:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalMyDsl.g:579:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:580:2: ( rule__Implies__Group_1__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==15) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:580:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalMyDsl.g:589:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:593:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalMyDsl.g:594:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalMyDsl.g:601:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:605:1: ( ( () ) )
            // InternalMyDsl.g:606:1: ( () )
            {
            // InternalMyDsl.g:606:1: ( () )
            // InternalMyDsl.g:607:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalMyDsl.g:608:2: ()
            // InternalMyDsl.g:608:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalMyDsl.g:616:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:620:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalMyDsl.g:621:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalMyDsl.g:628:1: rule__Implies__Group_1__1__Impl : ( '->' ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:632:1: ( ( '->' ) )
            // InternalMyDsl.g:633:1: ( '->' )
            {
            // InternalMyDsl.g:633:1: ( '->' )
            // InternalMyDsl.g:634:2: '->'
            {
             before(grammarAccess.getImpliesAccess().getHyphenMinusGreaterThanSignKeyword_1_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getHyphenMinusGreaterThanSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalMyDsl.g:643:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:647:1: ( rule__Implies__Group_1__2__Impl )
            // InternalMyDsl.g:648:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalMyDsl.g:654:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:658:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:659:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:659:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:660:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:661:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalMyDsl.g:661:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__Excludes__Group__0"
    // InternalMyDsl.g:670:1: rule__Excludes__Group__0 : rule__Excludes__Group__0__Impl rule__Excludes__Group__1 ;
    public final void rule__Excludes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:674:1: ( rule__Excludes__Group__0__Impl rule__Excludes__Group__1 )
            // InternalMyDsl.g:675:2: rule__Excludes__Group__0__Impl rule__Excludes__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Excludes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0"


    // $ANTLR start "rule__Excludes__Group__0__Impl"
    // InternalMyDsl.g:682:1: rule__Excludes__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Excludes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:686:1: ( ( ruleOr ) )
            // InternalMyDsl.g:687:1: ( ruleOr )
            {
            // InternalMyDsl.g:687:1: ( ruleOr )
            // InternalMyDsl.g:688:2: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0__Impl"


    // $ANTLR start "rule__Excludes__Group__1"
    // InternalMyDsl.g:697:1: rule__Excludes__Group__1 : rule__Excludes__Group__1__Impl ;
    public final void rule__Excludes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:701:1: ( rule__Excludes__Group__1__Impl )
            // InternalMyDsl.g:702:2: rule__Excludes__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1"


    // $ANTLR start "rule__Excludes__Group__1__Impl"
    // InternalMyDsl.g:708:1: rule__Excludes__Group__1__Impl : ( ( rule__Excludes__Group_1__0 )* ) ;
    public final void rule__Excludes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:712:1: ( ( ( rule__Excludes__Group_1__0 )* ) )
            // InternalMyDsl.g:713:1: ( ( rule__Excludes__Group_1__0 )* )
            {
            // InternalMyDsl.g:713:1: ( ( rule__Excludes__Group_1__0 )* )
            // InternalMyDsl.g:714:2: ( rule__Excludes__Group_1__0 )*
            {
             before(grammarAccess.getExcludesAccess().getGroup_1()); 
            // InternalMyDsl.g:715:2: ( rule__Excludes__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:715:3: rule__Excludes__Group_1__0
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Excludes__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getExcludesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__0"
    // InternalMyDsl.g:724:1: rule__Excludes__Group_1__0 : rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 ;
    public final void rule__Excludes__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:728:1: ( rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 )
            // InternalMyDsl.g:729:2: rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Excludes__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0"


    // $ANTLR start "rule__Excludes__Group_1__0__Impl"
    // InternalMyDsl.g:736:1: rule__Excludes__Group_1__0__Impl : ( () ) ;
    public final void rule__Excludes__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:740:1: ( ( () ) )
            // InternalMyDsl.g:741:1: ( () )
            {
            // InternalMyDsl.g:741:1: ( () )
            // InternalMyDsl.g:742:2: ()
            {
             before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 
            // InternalMyDsl.g:743:2: ()
            // InternalMyDsl.g:743:3: 
            {
            }

             after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0__Impl"


    // $ANTLR start "rule__Excludes__Group_1__1"
    // InternalMyDsl.g:751:1: rule__Excludes__Group_1__1 : rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 ;
    public final void rule__Excludes__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:755:1: ( rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 )
            // InternalMyDsl.g:756:2: rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Excludes__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1"


    // $ANTLR start "rule__Excludes__Group_1__1__Impl"
    // InternalMyDsl.g:763:1: rule__Excludes__Group_1__1__Impl : ( 'nand' ) ;
    public final void rule__Excludes__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:767:1: ( ( 'nand' ) )
            // InternalMyDsl.g:768:1: ( 'nand' )
            {
            // InternalMyDsl.g:768:1: ( 'nand' )
            // InternalMyDsl.g:769:2: 'nand'
            {
             before(grammarAccess.getExcludesAccess().getNandKeyword_1_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getExcludesAccess().getNandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__2"
    // InternalMyDsl.g:778:1: rule__Excludes__Group_1__2 : rule__Excludes__Group_1__2__Impl ;
    public final void rule__Excludes__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:782:1: ( rule__Excludes__Group_1__2__Impl )
            // InternalMyDsl.g:783:2: rule__Excludes__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2"


    // $ANTLR start "rule__Excludes__Group_1__2__Impl"
    // InternalMyDsl.g:789:1: rule__Excludes__Group_1__2__Impl : ( ( rule__Excludes__RightAssignment_1_2 ) ) ;
    public final void rule__Excludes__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:793:1: ( ( ( rule__Excludes__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:794:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:794:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            // InternalMyDsl.g:795:2: ( rule__Excludes__RightAssignment_1_2 )
            {
             before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:796:2: ( rule__Excludes__RightAssignment_1_2 )
            // InternalMyDsl.g:796:3: rule__Excludes__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalMyDsl.g:805:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:809:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalMyDsl.g:810:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalMyDsl.g:817:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:821:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:822:1: ( ruleAnd )
            {
            // InternalMyDsl.g:822:1: ( ruleAnd )
            // InternalMyDsl.g:823:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalMyDsl.g:832:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:836:1: ( rule__Or__Group__1__Impl )
            // InternalMyDsl.g:837:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalMyDsl.g:843:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:847:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalMyDsl.g:848:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalMyDsl.g:848:1: ( ( rule__Or__Group_1__0 )* )
            // InternalMyDsl.g:849:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalMyDsl.g:850:2: ( rule__Or__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==17) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:850:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalMyDsl.g:859:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:863:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalMyDsl.g:864:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalMyDsl.g:871:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:875:1: ( ( () ) )
            // InternalMyDsl.g:876:1: ( () )
            {
            // InternalMyDsl.g:876:1: ( () )
            // InternalMyDsl.g:877:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalMyDsl.g:878:2: ()
            // InternalMyDsl.g:878:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalMyDsl.g:886:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:890:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalMyDsl.g:891:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalMyDsl.g:898:1: rule__Or__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:902:1: ( ( 'or' ) )
            // InternalMyDsl.g:903:1: ( 'or' )
            {
            // InternalMyDsl.g:903:1: ( 'or' )
            // InternalMyDsl.g:904:2: 'or'
            {
             before(grammarAccess.getOrAccess().getOrKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getOrKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalMyDsl.g:913:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:917:1: ( rule__Or__Group_1__2__Impl )
            // InternalMyDsl.g:918:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalMyDsl.g:924:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:928:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:929:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:929:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalMyDsl.g:930:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:931:2: ( rule__Or__RightAssignment_1_2 )
            // InternalMyDsl.g:931:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalMyDsl.g:940:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:944:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalMyDsl.g:945:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalMyDsl.g:952:1: rule__And__Group__0__Impl : ( ruleNot ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:956:1: ( ( ruleNot ) )
            // InternalMyDsl.g:957:1: ( ruleNot )
            {
            // InternalMyDsl.g:957:1: ( ruleNot )
            // InternalMyDsl.g:958:2: ruleNot
            {
             before(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalMyDsl.g:967:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:971:1: ( rule__And__Group__1__Impl )
            // InternalMyDsl.g:972:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalMyDsl.g:978:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:982:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalMyDsl.g:983:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalMyDsl.g:983:1: ( ( rule__And__Group_1__0 )* )
            // InternalMyDsl.g:984:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalMyDsl.g:985:2: ( rule__And__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==18) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:985:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalMyDsl.g:994:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:998:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalMyDsl.g:999:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_14);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalMyDsl.g:1006:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1010:1: ( ( () ) )
            // InternalMyDsl.g:1011:1: ( () )
            {
            // InternalMyDsl.g:1011:1: ( () )
            // InternalMyDsl.g:1012:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalMyDsl.g:1013:2: ()
            // InternalMyDsl.g:1013:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalMyDsl.g:1021:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1025:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalMyDsl.g:1026:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_7);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalMyDsl.g:1033:1: rule__And__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1037:1: ( ( 'and' ) )
            // InternalMyDsl.g:1038:1: ( 'and' )
            {
            // InternalMyDsl.g:1038:1: ( 'and' )
            // InternalMyDsl.g:1039:2: 'and'
            {
             before(grammarAccess.getAndAccess().getAndKeyword_1_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getAndKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalMyDsl.g:1048:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1052:1: ( rule__And__Group_1__2__Impl )
            // InternalMyDsl.g:1053:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalMyDsl.g:1059:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1063:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1064:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1064:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1065:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1066:2: ( rule__And__RightAssignment_1_2 )
            // InternalMyDsl.g:1066:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group_0__0"
    // InternalMyDsl.g:1075:1: rule__Not__Group_0__0 : rule__Not__Group_0__0__Impl rule__Not__Group_0__1 ;
    public final void rule__Not__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1079:1: ( rule__Not__Group_0__0__Impl rule__Not__Group_0__1 )
            // InternalMyDsl.g:1080:2: rule__Not__Group_0__0__Impl rule__Not__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__Not__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0"


    // $ANTLR start "rule__Not__Group_0__0__Impl"
    // InternalMyDsl.g:1087:1: rule__Not__Group_0__0__Impl : ( 'not' ) ;
    public final void rule__Not__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1091:1: ( ( 'not' ) )
            // InternalMyDsl.g:1092:1: ( 'not' )
            {
            // InternalMyDsl.g:1092:1: ( 'not' )
            // InternalMyDsl.g:1093:2: 'not'
            {
             before(grammarAccess.getNotAccess().getNotKeyword_0_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getNotKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0__Impl"


    // $ANTLR start "rule__Not__Group_0__1"
    // InternalMyDsl.g:1102:1: rule__Not__Group_0__1 : rule__Not__Group_0__1__Impl ;
    public final void rule__Not__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1106:1: ( rule__Not__Group_0__1__Impl )
            // InternalMyDsl.g:1107:2: rule__Not__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1"


    // $ANTLR start "rule__Not__Group_0__1__Impl"
    // InternalMyDsl.g:1113:1: rule__Not__Group_0__1__Impl : ( rulePrimary ) ;
    public final void rule__Not__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1117:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1118:1: ( rulePrimary )
            {
            // InternalMyDsl.g:1118:1: ( rulePrimary )
            // InternalMyDsl.g:1119:2: rulePrimary
            {
             before(grammarAccess.getNotAccess().getPrimaryParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getPrimaryParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalMyDsl.g:1129:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1133:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalMyDsl.g:1134:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalMyDsl.g:1141:1: rule__Primary__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1145:1: ( ( '(' ) )
            // InternalMyDsl.g:1146:1: ( '(' )
            {
            // InternalMyDsl.g:1146:1: ( '(' )
            // InternalMyDsl.g:1147:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalMyDsl.g:1156:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1160:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalMyDsl.g:1161:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_16);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalMyDsl.g:1168:1: rule__Primary__Group_0__1__Impl : ( ruleBiimplies ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1172:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1173:1: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1173:1: ( ruleBiimplies )
            // InternalMyDsl.g:1174:2: ruleBiimplies
            {
             before(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalMyDsl.g:1183:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1187:1: ( rule__Primary__Group_0__2__Impl )
            // InternalMyDsl.g:1188:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalMyDsl.g:1194:1: rule__Primary__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1198:1: ( ( ')' ) )
            // InternalMyDsl.g:1199:1: ( ')' )
            {
            // InternalMyDsl.g:1199:1: ( ')' )
            // InternalMyDsl.g:1200:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Start__FormulaAssignment_0"
    // InternalMyDsl.g:1210:1: rule__Start__FormulaAssignment_0 : ( ruleBiimplies ) ;
    public final void rule__Start__FormulaAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1214:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1215:2: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1215:2: ( ruleBiimplies )
            // InternalMyDsl.g:1216:3: ruleBiimplies
            {
             before(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FormulaAssignment_0"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalMyDsl.g:1225:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1229:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:1230:2: ( ruleImplies )
            {
            // InternalMyDsl.g:1230:2: ( ruleImplies )
            // InternalMyDsl.g:1231:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalMyDsl.g:1240:1: rule__Implies__RightAssignment_1_2 : ( ruleExcludes ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1244:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:1245:2: ( ruleExcludes )
            {
            // InternalMyDsl.g:1245:2: ( ruleExcludes )
            // InternalMyDsl.g:1246:3: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__Excludes__RightAssignment_1_2"
    // InternalMyDsl.g:1255:1: rule__Excludes__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Excludes__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1259:1: ( ( ruleOr ) )
            // InternalMyDsl.g:1260:2: ( ruleOr )
            {
            // InternalMyDsl.g:1260:2: ( ruleOr )
            // InternalMyDsl.g:1261:3: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__RightAssignment_1_2"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalMyDsl.g:1270:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1274:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1275:2: ( ruleAnd )
            {
            // InternalMyDsl.g:1275:2: ( ruleAnd )
            // InternalMyDsl.g:1276:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalMyDsl.g:1285:1: rule__And__RightAssignment_1_2 : ( ruleNot ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1289:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1290:2: ( ruleNot )
            {
            // InternalMyDsl.g:1290:2: ( ruleNot )
            // InternalMyDsl.g:1291:3: ruleNot
            {
             before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Primary__ValueAssignment_1"
    // InternalMyDsl.g:1300:1: rule__Primary__ValueAssignment_1 : ( ruleBoolean ) ;
    public final void rule__Primary__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1304:1: ( ( ruleBoolean ) )
            // InternalMyDsl.g:1305:2: ( ruleBoolean )
            {
            // InternalMyDsl.g:1305:2: ( ruleBoolean )
            // InternalMyDsl.g:1306:3: ruleBoolean
            {
             before(grammarAccess.getPrimaryAccess().getValueBooleanParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBoolean();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getValueBooleanParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_2"
    // InternalMyDsl.g:1315:1: rule__Primary__ValueAssignment_2 : ( RULE_ID ) ;
    public final void rule__Primary__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1319:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1320:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1320:2: ( RULE_ID )
            // InternalMyDsl.g:1321:3: RULE_ID
            {
             before(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000181812L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000181810L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000200000L});

}