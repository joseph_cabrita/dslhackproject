package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'", "'('", "')'", "'True'", "'False'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Start";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:64:1: entryRuleStart returns [EObject current=null] : iv_ruleStart= ruleStart EOF ;
    public final EObject entryRuleStart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStart = null;


        try {
            // InternalMyDsl.g:64:46: (iv_ruleStart= ruleStart EOF )
            // InternalMyDsl.g:65:2: iv_ruleStart= ruleStart EOF
            {
             newCompositeNode(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStart=ruleStart();

            state._fsp--;

             current =iv_ruleStart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:71:1: ruleStart returns [EObject current=null] : ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )* ;
    public final EObject ruleStart() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_formula_0_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )* )
            // InternalMyDsl.g:78:2: ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )*
            {
            // InternalMyDsl.g:78:2: ( ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID||(LA1_0>=17 && LA1_0<=18)||(LA1_0>=20 && LA1_0<=21)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:79:3: ( (lv_formula_0_0= ruleBiimplies ) ) otherlv_1= ';'
            	    {
            	    // InternalMyDsl.g:79:3: ( (lv_formula_0_0= ruleBiimplies ) )
            	    // InternalMyDsl.g:80:4: (lv_formula_0_0= ruleBiimplies )
            	    {
            	    // InternalMyDsl.g:80:4: (lv_formula_0_0= ruleBiimplies )
            	    // InternalMyDsl.g:81:5: lv_formula_0_0= ruleBiimplies
            	    {

            	    					newCompositeNode(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_formula_0_0=ruleBiimplies();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStartRule());
            	    					}
            	    					add(
            	    						current,
            	    						"formula",
            	    						lv_formula_0_0,
            	    						"org.xtext.example.mydsl.MyDsl.Biimplies");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }

            	    otherlv_1=(Token)match(input,11,FOLLOW_4); 

            	    			newLeafNode(otherlv_1, grammarAccess.getStartAccess().getSemicolonKeyword_1());
            	    		

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:106:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;


        try {
            // InternalMyDsl.g:106:50: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalMyDsl.g:107:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:113:1: ruleBiimplies returns [EObject current=null] : (this_Implies_0= ruleImplies ( () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) ) )* ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Implies_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:119:2: ( (this_Implies_0= ruleImplies ( () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) ) )* ) )
            // InternalMyDsl.g:120:2: (this_Implies_0= ruleImplies ( () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) ) )* )
            {
            // InternalMyDsl.g:120:2: (this_Implies_0= ruleImplies ( () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) ) )* )
            // InternalMyDsl.g:121:3: this_Implies_0= ruleImplies ( () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) ) )*
            {

            			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
            		
            pushFollow(FOLLOW_5);
            this_Implies_0=ruleImplies();

            state._fsp--;


            			current = this_Implies_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:129:3: ( () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:130:4: () otherlv_2= '<->' ( (lv_right_3_0= ruleImplies ) )
            	    {
            	    // InternalMyDsl.g:130:4: ()
            	    // InternalMyDsl.g:131:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,12,FOLLOW_6); 

            	    				newLeafNode(otherlv_2, grammarAccess.getBiimpliesAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalMyDsl.g:141:4: ( (lv_right_3_0= ruleImplies ) )
            	    // InternalMyDsl.g:142:5: (lv_right_3_0= ruleImplies )
            	    {
            	    // InternalMyDsl.g:142:5: (lv_right_3_0= ruleImplies )
            	    // InternalMyDsl.g:143:6: lv_right_3_0= ruleImplies
            	    {

            	    						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_right_3_0=ruleImplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBiimpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Implies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:165:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalMyDsl.g:165:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalMyDsl.g:166:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:172:1: ruleImplies returns [EObject current=null] : (this_Excludes_0= ruleExcludes ( () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) ) )* ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Excludes_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:178:2: ( (this_Excludes_0= ruleExcludes ( () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) ) )* ) )
            // InternalMyDsl.g:179:2: (this_Excludes_0= ruleExcludes ( () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) ) )* )
            {
            // InternalMyDsl.g:179:2: (this_Excludes_0= ruleExcludes ( () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) ) )* )
            // InternalMyDsl.g:180:3: this_Excludes_0= ruleExcludes ( () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0());
            		
            pushFollow(FOLLOW_7);
            this_Excludes_0=ruleExcludes();

            state._fsp--;


            			current = this_Excludes_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:188:3: ( () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==13) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:189:4: () otherlv_2= '->' ( (lv_right_3_0= ruleExcludes ) )
            	    {
            	    // InternalMyDsl.g:189:4: ()
            	    // InternalMyDsl.g:190:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,13,FOLLOW_8); 

            	    				newLeafNode(otherlv_2, grammarAccess.getImpliesAccess().getHyphenMinusGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalMyDsl.g:200:4: ( (lv_right_3_0= ruleExcludes ) )
            	    // InternalMyDsl.g:201:5: (lv_right_3_0= ruleExcludes )
            	    {
            	    // InternalMyDsl.g:201:5: (lv_right_3_0= ruleExcludes )
            	    // InternalMyDsl.g:202:6: lv_right_3_0= ruleExcludes
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_right_3_0=ruleExcludes();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Excludes");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:224:1: entryRuleExcludes returns [EObject current=null] : iv_ruleExcludes= ruleExcludes EOF ;
    public final EObject entryRuleExcludes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExcludes = null;


        try {
            // InternalMyDsl.g:224:49: (iv_ruleExcludes= ruleExcludes EOF )
            // InternalMyDsl.g:225:2: iv_ruleExcludes= ruleExcludes EOF
            {
             newCompositeNode(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExcludes=ruleExcludes();

            state._fsp--;

             current =iv_ruleExcludes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:231:1: ruleExcludes returns [EObject current=null] : (this_Or_0= ruleOr ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) ) )* ) ;
    public final EObject ruleExcludes() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Or_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:237:2: ( (this_Or_0= ruleOr ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) ) )* ) )
            // InternalMyDsl.g:238:2: (this_Or_0= ruleOr ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) ) )* )
            {
            // InternalMyDsl.g:238:2: (this_Or_0= ruleOr ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) ) )* )
            // InternalMyDsl.g:239:3: this_Or_0= ruleOr ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) ) )*
            {

            			newCompositeNode(grammarAccess.getExcludesAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_9);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:247:3: ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==14) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:248:4: () otherlv_2= 'nand' ( (lv_right_3_0= ruleOr ) )
            	    {
            	    // InternalMyDsl.g:248:4: ()
            	    // InternalMyDsl.g:249:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,14,FOLLOW_10); 

            	    				newLeafNode(otherlv_2, grammarAccess.getExcludesAccess().getNandKeyword_1_1());
            	    			
            	    // InternalMyDsl.g:259:4: ( (lv_right_3_0= ruleOr ) )
            	    // InternalMyDsl.g:260:5: (lv_right_3_0= ruleOr )
            	    {
            	    // InternalMyDsl.g:260:5: (lv_right_3_0= ruleOr )
            	    // InternalMyDsl.g:261:6: lv_right_3_0= ruleOr
            	    {

            	    						newCompositeNode(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_right_3_0=ruleOr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExcludesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Or");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:283:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalMyDsl.g:283:43: (iv_ruleOr= ruleOr EOF )
            // InternalMyDsl.g:284:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:290:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:296:2: ( (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalMyDsl.g:297:2: (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalMyDsl.g:297:2: (this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalMyDsl.g:298:3: this_And_0= ruleAnd ( () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:306:3: ( () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:307:4: () otherlv_2= 'or' ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalMyDsl.g:307:4: ()
            	    // InternalMyDsl.g:308:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,15,FOLLOW_12); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getOrKeyword_1_1());
            	    			
            	    // InternalMyDsl.g:318:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalMyDsl.g:319:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalMyDsl.g:319:5: (lv_right_3_0= ruleAnd )
            	    // InternalMyDsl.g:320:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:342:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalMyDsl.g:342:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalMyDsl.g:343:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:349:1: ruleAnd returns [EObject current=null] : (this_Not_0= ruleNot ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Not_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:355:2: ( (this_Not_0= ruleNot ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) ) )* ) )
            // InternalMyDsl.g:356:2: (this_Not_0= ruleNot ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) ) )* )
            {
            // InternalMyDsl.g:356:2: (this_Not_0= ruleNot ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) ) )* )
            // InternalMyDsl.g:357:3: this_Not_0= ruleNot ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getNotParserRuleCall_0());
            		
            pushFollow(FOLLOW_13);
            this_Not_0=ruleNot();

            state._fsp--;


            			current = this_Not_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:365:3: ( () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==16) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:366:4: () otherlv_2= 'and' ( (lv_right_3_0= ruleNot ) )
            	    {
            	    // InternalMyDsl.g:366:4: ()
            	    // InternalMyDsl.g:367:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,16,FOLLOW_14); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndAccess().getAndKeyword_1_1());
            	    			
            	    // InternalMyDsl.g:377:4: ( (lv_right_3_0= ruleNot ) )
            	    // InternalMyDsl.g:378:5: (lv_right_3_0= ruleNot )
            	    {
            	    // InternalMyDsl.g:378:5: (lv_right_3_0= ruleNot )
            	    // InternalMyDsl.g:379:6: lv_right_3_0= ruleNot
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_right_3_0=ruleNot();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Not");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:401:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalMyDsl.g:401:44: (iv_ruleNot= ruleNot EOF )
            // InternalMyDsl.g:402:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:408:1: ruleNot returns [EObject current=null] : ( (otherlv_0= 'not' this_Primary_1= rulePrimary ) | this_Primary_2= rulePrimary ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_Primary_1 = null;

        EObject this_Primary_2 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:414:2: ( ( (otherlv_0= 'not' this_Primary_1= rulePrimary ) | this_Primary_2= rulePrimary ) )
            // InternalMyDsl.g:415:2: ( (otherlv_0= 'not' this_Primary_1= rulePrimary ) | this_Primary_2= rulePrimary )
            {
            // InternalMyDsl.g:415:2: ( (otherlv_0= 'not' this_Primary_1= rulePrimary ) | this_Primary_2= rulePrimary )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID||LA7_0==18||(LA7_0>=20 && LA7_0<=21)) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalMyDsl.g:416:3: (otherlv_0= 'not' this_Primary_1= rulePrimary )
                    {
                    // InternalMyDsl.g:416:3: (otherlv_0= 'not' this_Primary_1= rulePrimary )
                    // InternalMyDsl.g:417:4: otherlv_0= 'not' this_Primary_1= rulePrimary
                    {
                    otherlv_0=(Token)match(input,17,FOLLOW_15); 

                    				newLeafNode(otherlv_0, grammarAccess.getNotAccess().getNotKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getNotAccess().getPrimaryParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_2);
                    this_Primary_1=rulePrimary();

                    state._fsp--;


                    				current = this_Primary_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:431:3: this_Primary_2= rulePrimary
                    {

                    			newCompositeNode(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Primary_2=rulePrimary();

                    state._fsp--;


                    			current = this_Primary_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:443:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalMyDsl.g:443:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalMyDsl.g:444:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:450:1: rulePrimary returns [EObject current=null] : ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= ruleBoolean ) ) | ( (lv_value_4_0= RULE_ID ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_value_4_0=null;
        EObject this_Biimplies_1 = null;

        AntlrDatatypeRuleToken lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:456:2: ( ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= ruleBoolean ) ) | ( (lv_value_4_0= RULE_ID ) ) ) )
            // InternalMyDsl.g:457:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= ruleBoolean ) ) | ( (lv_value_4_0= RULE_ID ) ) )
            {
            // InternalMyDsl.g:457:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= ruleBoolean ) ) | ( (lv_value_4_0= RULE_ID ) ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt8=1;
                }
                break;
            case 20:
            case 21:
                {
                alt8=2;
                }
                break;
            case RULE_ID:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalMyDsl.g:458:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    {
                    // InternalMyDsl.g:458:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    // InternalMyDsl.g:459:4: otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,18,FOLLOW_16); 

                    				newLeafNode(otherlv_0, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_17);
                    this_Biimplies_1=ruleBiimplies();

                    state._fsp--;


                    				current = this_Biimplies_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,19,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:477:3: ( (lv_value_3_0= ruleBoolean ) )
                    {
                    // InternalMyDsl.g:477:3: ( (lv_value_3_0= ruleBoolean ) )
                    // InternalMyDsl.g:478:4: (lv_value_3_0= ruleBoolean )
                    {
                    // InternalMyDsl.g:478:4: (lv_value_3_0= ruleBoolean )
                    // InternalMyDsl.g:479:5: lv_value_3_0= ruleBoolean
                    {

                    					newCompositeNode(grammarAccess.getPrimaryAccess().getValueBooleanParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_value_3_0=ruleBoolean();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getPrimaryRule());
                    					}
                    					set(
                    						current,
                    						"value",
                    						lv_value_3_0,
                    						"org.xtext.example.mydsl.MyDsl.Boolean");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:497:3: ( (lv_value_4_0= RULE_ID ) )
                    {
                    // InternalMyDsl.g:497:3: ( (lv_value_4_0= RULE_ID ) )
                    // InternalMyDsl.g:498:4: (lv_value_4_0= RULE_ID )
                    {
                    // InternalMyDsl.g:498:4: (lv_value_4_0= RULE_ID )
                    // InternalMyDsl.g:499:5: lv_value_4_0= RULE_ID
                    {
                    lv_value_4_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_value_4_0, grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_4_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleBoolean"
    // InternalMyDsl.g:519:1: entryRuleBoolean returns [String current=null] : iv_ruleBoolean= ruleBoolean EOF ;
    public final String entryRuleBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBoolean = null;


        try {
            // InternalMyDsl.g:519:47: (iv_ruleBoolean= ruleBoolean EOF )
            // InternalMyDsl.g:520:2: iv_ruleBoolean= ruleBoolean EOF
            {
             newCompositeNode(grammarAccess.getBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoolean=ruleBoolean();

            state._fsp--;

             current =iv_ruleBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalMyDsl.g:526:1: ruleBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'True' | kw= 'False' ) ;
    public final AntlrDatatypeRuleToken ruleBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMyDsl.g:532:2: ( (kw= 'True' | kw= 'False' ) )
            // InternalMyDsl.g:533:2: (kw= 'True' | kw= 'False' )
            {
            // InternalMyDsl.g:533:2: (kw= 'True' | kw= 'False' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            else if ( (LA9_0==21) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:534:3: kw= 'True'
                    {
                    kw=(Token)match(input,20,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:540:3: kw= 'False'
                    {
                    kw=(Token)match(input,21,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000360012L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000361010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000362010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000364010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000368010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000370010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000360010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000000003E0010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000080000L});

}