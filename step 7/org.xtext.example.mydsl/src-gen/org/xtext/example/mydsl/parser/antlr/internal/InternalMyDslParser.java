package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FILE'", "';'", "'SAT4J'", "'MiniSat'", "'PicoSat'", "'Comp'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'", "'('", "')'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=5;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=7;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__26=26;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_FALSE=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Start";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:64:1: entryRuleStart returns [EObject current=null] : iv_ruleStart= ruleStart EOF ;
    public final EObject entryRuleStart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStart = null;


        try {
            // InternalMyDsl.g:64:46: (iv_ruleStart= ruleStart EOF )
            // InternalMyDsl.g:65:2: iv_ruleStart= ruleStart EOF
            {
             newCompositeNode(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStart=ruleStart();

            state._fsp--;

             current =iv_ruleStart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:71:1: ruleStart returns [EObject current=null] : ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';' ) | ( ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) ) ) )* ;
    public final EObject ruleStart() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_File_1_0=null;
        Token otherlv_3=null;
        EObject lv_SATF_2_0 = null;

        EObject lv_formula_4_0 = null;

        EObject lv_SAT_5_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';' ) | ( ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) ) ) )* )
            // InternalMyDsl.g:78:2: ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';' ) | ( ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) ) ) )*
            {
            // InternalMyDsl.g:78:2: ( (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';' ) | ( ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) ) ) )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13) ) {
                    alt1=1;
                }
                else if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||(LA1_0>=24 && LA1_0<=25)) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:79:3: (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';' )
            	    {
            	    // InternalMyDsl.g:79:3: (otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';' )
            	    // InternalMyDsl.g:80:4: otherlv_0= 'FILE' ( (lv_File_1_0= RULE_STRING ) ) ( (lv_SATF_2_0= ruleSolverFile ) ) otherlv_3= ';'
            	    {
            	    otherlv_0=(Token)match(input,13,FOLLOW_3); 

            	    				newLeafNode(otherlv_0, grammarAccess.getStartAccess().getFILEKeyword_0_0());
            	    			
            	    // InternalMyDsl.g:84:4: ( (lv_File_1_0= RULE_STRING ) )
            	    // InternalMyDsl.g:85:5: (lv_File_1_0= RULE_STRING )
            	    {
            	    // InternalMyDsl.g:85:5: (lv_File_1_0= RULE_STRING )
            	    // InternalMyDsl.g:86:6: lv_File_1_0= RULE_STRING
            	    {
            	    lv_File_1_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

            	    						newLeafNode(lv_File_1_0, grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getStartRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"File",
            	    							lv_File_1_0,
            	    							"org.eclipse.xtext.common.Terminals.STRING");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:102:4: ( (lv_SATF_2_0= ruleSolverFile ) )
            	    // InternalMyDsl.g:103:5: (lv_SATF_2_0= ruleSolverFile )
            	    {
            	    // InternalMyDsl.g:103:5: (lv_SATF_2_0= ruleSolverFile )
            	    // InternalMyDsl.g:104:6: lv_SATF_2_0= ruleSolverFile
            	    {

            	    						newCompositeNode(grammarAccess.getStartAccess().getSATFSolverFileParserRuleCall_0_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_SATF_2_0=ruleSolverFile();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStartRule());
            	    						}
            	    						add(
            	    							current,
            	    							"SATF",
            	    							lv_SATF_2_0,
            	    							"org.xtext.example.mydsl.MyDsl.SolverFile");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_3=(Token)match(input,14,FOLLOW_6); 

            	    				newLeafNode(otherlv_3, grammarAccess.getStartAccess().getSemicolonKeyword_0_3());
            	    			

            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:127:3: ( ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) ) )
            	    {
            	    // InternalMyDsl.g:127:3: ( ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) ) )
            	    // InternalMyDsl.g:128:4: ( (lv_formula_4_0= ruleBiimplies ) ) ( (lv_SAT_5_0= ruleSolver ) )
            	    {
            	    // InternalMyDsl.g:128:4: ( (lv_formula_4_0= ruleBiimplies ) )
            	    // InternalMyDsl.g:129:5: (lv_formula_4_0= ruleBiimplies )
            	    {
            	    // InternalMyDsl.g:129:5: (lv_formula_4_0= ruleBiimplies )
            	    // InternalMyDsl.g:130:6: lv_formula_4_0= ruleBiimplies
            	    {

            	    						newCompositeNode(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_formula_4_0=ruleBiimplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStartRule());
            	    						}
            	    						add(
            	    							current,
            	    							"formula",
            	    							lv_formula_4_0,
            	    							"org.xtext.example.mydsl.MyDsl.Biimplies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:147:4: ( (lv_SAT_5_0= ruleSolver ) )
            	    // InternalMyDsl.g:148:5: (lv_SAT_5_0= ruleSolver )
            	    {
            	    // InternalMyDsl.g:148:5: (lv_SAT_5_0= ruleSolver )
            	    // InternalMyDsl.g:149:6: lv_SAT_5_0= ruleSolver
            	    {

            	    						newCompositeNode(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_SAT_5_0=ruleSolver();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStartRule());
            	    						}
            	    						add(
            	    							current,
            	    							"SAT",
            	    							lv_SAT_5_0,
            	    							"org.xtext.example.mydsl.MyDsl.Solver");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleSolver"
    // InternalMyDsl.g:171:1: entryRuleSolver returns [EObject current=null] : iv_ruleSolver= ruleSolver EOF ;
    public final EObject entryRuleSolver() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSolver = null;


        try {
            // InternalMyDsl.g:171:47: (iv_ruleSolver= ruleSolver EOF )
            // InternalMyDsl.g:172:2: iv_ruleSolver= ruleSolver EOF
            {
             newCompositeNode(grammarAccess.getSolverRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSolver=ruleSolver();

            state._fsp--;

             current =iv_ruleSolver; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSolver"


    // $ANTLR start "ruleSolver"
    // InternalMyDsl.g:178:1: ruleSolver returns [EObject current=null] : ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';' ) | ( ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';' ) | ( ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';' ) | ( (lv_value_8_0= ';' ) ) ) ;
    public final EObject ruleSolver() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;
        Token otherlv_3=null;
        Token lv_value_4_0=null;
        Token otherlv_5=null;
        Token lv_value_6_0=null;
        Token otherlv_7=null;
        Token lv_value_8_0=null;


        	enterRule();

        try {
            // InternalMyDsl.g:184:2: ( ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';' ) | ( ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';' ) | ( ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';' ) | ( (lv_value_8_0= ';' ) ) ) )
            // InternalMyDsl.g:185:2: ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';' ) | ( ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';' ) | ( ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';' ) | ( (lv_value_8_0= ';' ) ) )
            {
            // InternalMyDsl.g:185:2: ( ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' ) | ( ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';' ) | ( ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';' ) | ( ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';' ) | ( (lv_value_8_0= ';' ) ) )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt2=1;
                }
                break;
            case 16:
                {
                alt2=2;
                }
                break;
            case 17:
                {
                alt2=3;
                }
                break;
            case 18:
                {
                alt2=4;
                }
                break;
            case 14:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:186:3: ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' )
                    {
                    // InternalMyDsl.g:186:3: ( ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';' )
                    // InternalMyDsl.g:187:4: ( (lv_value_0_0= 'SAT4J' ) ) otherlv_1= ';'
                    {
                    // InternalMyDsl.g:187:4: ( (lv_value_0_0= 'SAT4J' ) )
                    // InternalMyDsl.g:188:5: (lv_value_0_0= 'SAT4J' )
                    {
                    // InternalMyDsl.g:188:5: (lv_value_0_0= 'SAT4J' )
                    // InternalMyDsl.g:189:6: lv_value_0_0= 'SAT4J'
                    {
                    lv_value_0_0=(Token)match(input,15,FOLLOW_5); 

                    						newLeafNode(lv_value_0_0, grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSolverRule());
                    						}
                    						setWithLastConsumed(current, "value", lv_value_0_0, "SAT4J");
                    					

                    }


                    }

                    otherlv_1=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_1, grammarAccess.getSolverAccess().getSemicolonKeyword_0_1());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:207:3: ( ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';' )
                    {
                    // InternalMyDsl.g:207:3: ( ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';' )
                    // InternalMyDsl.g:208:4: ( (lv_value_2_0= 'MiniSat' ) ) otherlv_3= ';'
                    {
                    // InternalMyDsl.g:208:4: ( (lv_value_2_0= 'MiniSat' ) )
                    // InternalMyDsl.g:209:5: (lv_value_2_0= 'MiniSat' )
                    {
                    // InternalMyDsl.g:209:5: (lv_value_2_0= 'MiniSat' )
                    // InternalMyDsl.g:210:6: lv_value_2_0= 'MiniSat'
                    {
                    lv_value_2_0=(Token)match(input,16,FOLLOW_5); 

                    						newLeafNode(lv_value_2_0, grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSolverRule());
                    						}
                    						setWithLastConsumed(current, "value", lv_value_2_0, "MiniSat");
                    					

                    }


                    }

                    otherlv_3=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getSolverAccess().getSemicolonKeyword_1_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:228:3: ( ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';' )
                    {
                    // InternalMyDsl.g:228:3: ( ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';' )
                    // InternalMyDsl.g:229:4: ( (lv_value_4_0= 'PicoSat' ) ) otherlv_5= ';'
                    {
                    // InternalMyDsl.g:229:4: ( (lv_value_4_0= 'PicoSat' ) )
                    // InternalMyDsl.g:230:5: (lv_value_4_0= 'PicoSat' )
                    {
                    // InternalMyDsl.g:230:5: (lv_value_4_0= 'PicoSat' )
                    // InternalMyDsl.g:231:6: lv_value_4_0= 'PicoSat'
                    {
                    lv_value_4_0=(Token)match(input,17,FOLLOW_5); 

                    						newLeafNode(lv_value_4_0, grammarAccess.getSolverAccess().getValuePicoSatKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSolverRule());
                    						}
                    						setWithLastConsumed(current, "value", lv_value_4_0, "PicoSat");
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getSolverAccess().getSemicolonKeyword_2_1());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:249:3: ( ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';' )
                    {
                    // InternalMyDsl.g:249:3: ( ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';' )
                    // InternalMyDsl.g:250:4: ( (lv_value_6_0= 'Comp' ) ) otherlv_7= ';'
                    {
                    // InternalMyDsl.g:250:4: ( (lv_value_6_0= 'Comp' ) )
                    // InternalMyDsl.g:251:5: (lv_value_6_0= 'Comp' )
                    {
                    // InternalMyDsl.g:251:5: (lv_value_6_0= 'Comp' )
                    // InternalMyDsl.g:252:6: lv_value_6_0= 'Comp'
                    {
                    lv_value_6_0=(Token)match(input,18,FOLLOW_5); 

                    						newLeafNode(lv_value_6_0, grammarAccess.getSolverAccess().getValueCompKeyword_3_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSolverRule());
                    						}
                    						setWithLastConsumed(current, "value", lv_value_6_0, "Comp");
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_7, grammarAccess.getSolverAccess().getSemicolonKeyword_3_1());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:270:3: ( (lv_value_8_0= ';' ) )
                    {
                    // InternalMyDsl.g:270:3: ( (lv_value_8_0= ';' ) )
                    // InternalMyDsl.g:271:4: (lv_value_8_0= ';' )
                    {
                    // InternalMyDsl.g:271:4: (lv_value_8_0= ';' )
                    // InternalMyDsl.g:272:5: lv_value_8_0= ';'
                    {
                    lv_value_8_0=(Token)match(input,14,FOLLOW_2); 

                    					newLeafNode(lv_value_8_0, grammarAccess.getSolverAccess().getValueSemicolonKeyword_4_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSolverRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_8_0, ";");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSolver"


    // $ANTLR start "entryRuleSolverFile"
    // InternalMyDsl.g:288:1: entryRuleSolverFile returns [EObject current=null] : iv_ruleSolverFile= ruleSolverFile EOF ;
    public final EObject entryRuleSolverFile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSolverFile = null;


        try {
            // InternalMyDsl.g:288:51: (iv_ruleSolverFile= ruleSolverFile EOF )
            // InternalMyDsl.g:289:2: iv_ruleSolverFile= ruleSolverFile EOF
            {
             newCompositeNode(grammarAccess.getSolverFileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSolverFile=ruleSolverFile();

            state._fsp--;

             current =iv_ruleSolverFile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSolverFile"


    // $ANTLR start "ruleSolverFile"
    // InternalMyDsl.g:295:1: ruleSolverFile returns [EObject current=null] : ( ( (lv_value_0_0= 'SAT4J' ) ) | ( (lv_value_1_0= 'MiniSat' ) ) | ( (lv_value_2_0= 'PicoSat' ) ) | ( (lv_value_3_0= 'Comp' ) ) ) ;
    public final EObject ruleSolverFile() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token lv_value_1_0=null;
        Token lv_value_2_0=null;
        Token lv_value_3_0=null;


        	enterRule();

        try {
            // InternalMyDsl.g:301:2: ( ( ( (lv_value_0_0= 'SAT4J' ) ) | ( (lv_value_1_0= 'MiniSat' ) ) | ( (lv_value_2_0= 'PicoSat' ) ) | ( (lv_value_3_0= 'Comp' ) ) ) )
            // InternalMyDsl.g:302:2: ( ( (lv_value_0_0= 'SAT4J' ) ) | ( (lv_value_1_0= 'MiniSat' ) ) | ( (lv_value_2_0= 'PicoSat' ) ) | ( (lv_value_3_0= 'Comp' ) ) )
            {
            // InternalMyDsl.g:302:2: ( ( (lv_value_0_0= 'SAT4J' ) ) | ( (lv_value_1_0= 'MiniSat' ) ) | ( (lv_value_2_0= 'PicoSat' ) ) | ( (lv_value_3_0= 'Comp' ) ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt3=1;
                }
                break;
            case 16:
                {
                alt3=2;
                }
                break;
            case 17:
                {
                alt3=3;
                }
                break;
            case 18:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:303:3: ( (lv_value_0_0= 'SAT4J' ) )
                    {
                    // InternalMyDsl.g:303:3: ( (lv_value_0_0= 'SAT4J' ) )
                    // InternalMyDsl.g:304:4: (lv_value_0_0= 'SAT4J' )
                    {
                    // InternalMyDsl.g:304:4: (lv_value_0_0= 'SAT4J' )
                    // InternalMyDsl.g:305:5: lv_value_0_0= 'SAT4J'
                    {
                    lv_value_0_0=(Token)match(input,15,FOLLOW_2); 

                    					newLeafNode(lv_value_0_0, grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSolverFileRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_0_0, "SAT4J");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:318:3: ( (lv_value_1_0= 'MiniSat' ) )
                    {
                    // InternalMyDsl.g:318:3: ( (lv_value_1_0= 'MiniSat' ) )
                    // InternalMyDsl.g:319:4: (lv_value_1_0= 'MiniSat' )
                    {
                    // InternalMyDsl.g:319:4: (lv_value_1_0= 'MiniSat' )
                    // InternalMyDsl.g:320:5: lv_value_1_0= 'MiniSat'
                    {
                    lv_value_1_0=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(lv_value_1_0, grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSolverFileRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_1_0, "MiniSat");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:333:3: ( (lv_value_2_0= 'PicoSat' ) )
                    {
                    // InternalMyDsl.g:333:3: ( (lv_value_2_0= 'PicoSat' ) )
                    // InternalMyDsl.g:334:4: (lv_value_2_0= 'PicoSat' )
                    {
                    // InternalMyDsl.g:334:4: (lv_value_2_0= 'PicoSat' )
                    // InternalMyDsl.g:335:5: lv_value_2_0= 'PicoSat'
                    {
                    lv_value_2_0=(Token)match(input,17,FOLLOW_2); 

                    					newLeafNode(lv_value_2_0, grammarAccess.getSolverFileAccess().getValuePicoSatKeyword_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSolverFileRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_2_0, "PicoSat");
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:348:3: ( (lv_value_3_0= 'Comp' ) )
                    {
                    // InternalMyDsl.g:348:3: ( (lv_value_3_0= 'Comp' ) )
                    // InternalMyDsl.g:349:4: (lv_value_3_0= 'Comp' )
                    {
                    // InternalMyDsl.g:349:4: (lv_value_3_0= 'Comp' )
                    // InternalMyDsl.g:350:5: lv_value_3_0= 'Comp'
                    {
                    lv_value_3_0=(Token)match(input,18,FOLLOW_2); 

                    					newLeafNode(lv_value_3_0, grammarAccess.getSolverFileAccess().getValueCompKeyword_3_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSolverFileRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_3_0, "Comp");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSolverFile"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:366:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;


        try {
            // InternalMyDsl.g:366:50: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalMyDsl.g:367:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:373:1: ruleBiimplies returns [EObject current=null] : (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Implies_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:379:2: ( (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* ) )
            // InternalMyDsl.g:380:2: (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* )
            {
            // InternalMyDsl.g:380:2: (this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )* )
            // InternalMyDsl.g:381:3: this_Implies_0= ruleImplies ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )*
            {

            			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
            		
            pushFollow(FOLLOW_9);
            this_Implies_0=ruleImplies();

            state._fsp--;


            			current = this_Implies_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:389:3: ( () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==19) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:390:4: () ( (lv_type_2_0= '<->' ) ) ( (lv_right_3_0= ruleImplies ) )
            	    {
            	    // InternalMyDsl.g:390:4: ()
            	    // InternalMyDsl.g:391:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:397:4: ( (lv_type_2_0= '<->' ) )
            	    // InternalMyDsl.g:398:5: (lv_type_2_0= '<->' )
            	    {
            	    // InternalMyDsl.g:398:5: (lv_type_2_0= '<->' )
            	    // InternalMyDsl.g:399:6: lv_type_2_0= '<->'
            	    {
            	    lv_type_2_0=(Token)match(input,19,FOLLOW_10); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBiimpliesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "<->");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:411:4: ( (lv_right_3_0= ruleImplies ) )
            	    // InternalMyDsl.g:412:5: (lv_right_3_0= ruleImplies )
            	    {
            	    // InternalMyDsl.g:412:5: (lv_right_3_0= ruleImplies )
            	    // InternalMyDsl.g:413:6: lv_right_3_0= ruleImplies
            	    {

            	    						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_right_3_0=ruleImplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBiimpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Implies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:435:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalMyDsl.g:435:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalMyDsl.g:436:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:442:1: ruleImplies returns [EObject current=null] : (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Excludes_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:448:2: ( (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* ) )
            // InternalMyDsl.g:449:2: (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* )
            {
            // InternalMyDsl.g:449:2: (this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )* )
            // InternalMyDsl.g:450:3: this_Excludes_0= ruleExcludes ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_Excludes_0=ruleExcludes();

            state._fsp--;


            			current = this_Excludes_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:458:3: ( () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==20) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:459:4: () ( (lv_type_2_0= '->' ) ) ( (lv_right_3_0= ruleExcludes ) )
            	    {
            	    // InternalMyDsl.g:459:4: ()
            	    // InternalMyDsl.g:460:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:466:4: ( (lv_type_2_0= '->' ) )
            	    // InternalMyDsl.g:467:5: (lv_type_2_0= '->' )
            	    {
            	    // InternalMyDsl.g:467:5: (lv_type_2_0= '->' )
            	    // InternalMyDsl.g:468:6: lv_type_2_0= '->'
            	    {
            	    lv_type_2_0=(Token)match(input,20,FOLLOW_12); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getImpliesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "->");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:480:4: ( (lv_right_3_0= ruleExcludes ) )
            	    // InternalMyDsl.g:481:5: (lv_right_3_0= ruleExcludes )
            	    {
            	    // InternalMyDsl.g:481:5: (lv_right_3_0= ruleExcludes )
            	    // InternalMyDsl.g:482:6: lv_right_3_0= ruleExcludes
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_3_0=ruleExcludes();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Excludes");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:504:1: entryRuleExcludes returns [EObject current=null] : iv_ruleExcludes= ruleExcludes EOF ;
    public final EObject entryRuleExcludes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExcludes = null;


        try {
            // InternalMyDsl.g:504:49: (iv_ruleExcludes= ruleExcludes EOF )
            // InternalMyDsl.g:505:2: iv_ruleExcludes= ruleExcludes EOF
            {
             newCompositeNode(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExcludes=ruleExcludes();

            state._fsp--;

             current =iv_ruleExcludes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:511:1: ruleExcludes returns [EObject current=null] : (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* ) ;
    public final EObject ruleExcludes() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Or_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:517:2: ( (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* ) )
            // InternalMyDsl.g:518:2: (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* )
            {
            // InternalMyDsl.g:518:2: (this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )* )
            // InternalMyDsl.g:519:3: this_Or_0= ruleOr ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )*
            {

            			newCompositeNode(grammarAccess.getExcludesAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_13);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:527:3: ( () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==21) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:528:4: () ( (lv_type_2_0= 'nand' ) ) ( (lv_right_3_0= ruleOr ) )
            	    {
            	    // InternalMyDsl.g:528:4: ()
            	    // InternalMyDsl.g:529:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:535:4: ( (lv_type_2_0= 'nand' ) )
            	    // InternalMyDsl.g:536:5: (lv_type_2_0= 'nand' )
            	    {
            	    // InternalMyDsl.g:536:5: (lv_type_2_0= 'nand' )
            	    // InternalMyDsl.g:537:6: lv_type_2_0= 'nand'
            	    {
            	    lv_type_2_0=(Token)match(input,21,FOLLOW_14); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getExcludesRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "nand");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:549:4: ( (lv_right_3_0= ruleOr ) )
            	    // InternalMyDsl.g:550:5: (lv_right_3_0= ruleOr )
            	    {
            	    // InternalMyDsl.g:550:5: (lv_right_3_0= ruleOr )
            	    // InternalMyDsl.g:551:6: lv_right_3_0= ruleOr
            	    {

            	    						newCompositeNode(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_right_3_0=ruleOr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExcludesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Or");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:573:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalMyDsl.g:573:43: (iv_ruleOr= ruleOr EOF )
            // InternalMyDsl.g:574:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:580:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:586:2: ( (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalMyDsl.g:587:2: (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalMyDsl.g:587:2: (this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalMyDsl.g:588:3: this_And_0= ruleAnd ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_15);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:596:3: ( () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==22) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:597:4: () ( (lv_type_2_0= 'or' ) ) ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalMyDsl.g:597:4: ()
            	    // InternalMyDsl.g:598:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:604:4: ( (lv_type_2_0= 'or' ) )
            	    // InternalMyDsl.g:605:5: (lv_type_2_0= 'or' )
            	    {
            	    // InternalMyDsl.g:605:5: (lv_type_2_0= 'or' )
            	    // InternalMyDsl.g:606:6: lv_type_2_0= 'or'
            	    {
            	    lv_type_2_0=(Token)match(input,22,FOLLOW_16); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getOrRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "or");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:618:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalMyDsl.g:619:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalMyDsl.g:619:5: (lv_right_3_0= ruleAnd )
            	    // InternalMyDsl.g:620:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_15);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:642:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalMyDsl.g:642:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalMyDsl.g:643:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:649:1: ruleAnd returns [EObject current=null] : (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token lv_type_2_0=null;
        EObject this_Not_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:655:2: ( (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* ) )
            // InternalMyDsl.g:656:2: (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* )
            {
            // InternalMyDsl.g:656:2: (this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )* )
            // InternalMyDsl.g:657:3: this_Not_0= ruleNot ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getNotParserRuleCall_0());
            		
            pushFollow(FOLLOW_17);
            this_Not_0=ruleNot();

            state._fsp--;


            			current = this_Not_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMyDsl.g:665:3: ( () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==23) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:666:4: () ( (lv_type_2_0= 'and' ) ) ( (lv_right_3_0= ruleNot ) )
            	    {
            	    // InternalMyDsl.g:666:4: ()
            	    // InternalMyDsl.g:667:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMyDsl.g:673:4: ( (lv_type_2_0= 'and' ) )
            	    // InternalMyDsl.g:674:5: (lv_type_2_0= 'and' )
            	    {
            	    // InternalMyDsl.g:674:5: (lv_type_2_0= 'and' )
            	    // InternalMyDsl.g:675:6: lv_type_2_0= 'and'
            	    {
            	    lv_type_2_0=(Token)match(input,23,FOLLOW_18); 

            	    						newLeafNode(lv_type_2_0, grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getAndRule());
            	    						}
            	    						setWithLastConsumed(current, "type", lv_type_2_0, "and");
            	    					

            	    }


            	    }

            	    // InternalMyDsl.g:687:4: ( (lv_right_3_0= ruleNot ) )
            	    // InternalMyDsl.g:688:5: (lv_right_3_0= ruleNot )
            	    {
            	    // InternalMyDsl.g:688:5: (lv_right_3_0= ruleNot )
            	    // InternalMyDsl.g:689:6: lv_right_3_0= ruleNot
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_right_3_0=ruleNot();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.mydsl.MyDsl.Not");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:711:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalMyDsl.g:711:44: (iv_ruleNot= ruleNot EOF )
            // InternalMyDsl.g:712:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:718:1: ruleNot returns [EObject current=null] : ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        EObject lv_right_1_0 = null;

        EObject lv_right_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:724:2: ( ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) ) )
            // InternalMyDsl.g:725:2: ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) )
            {
            // InternalMyDsl.g:725:2: ( ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) ) | ( (lv_right_2_0= rulePrimary ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==24) ) {
                alt9=1;
            }
            else if ( ((LA9_0>=RULE_TRUE && LA9_0<=RULE_ID)||LA9_0==25) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:726:3: ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) )
                    {
                    // InternalMyDsl.g:726:3: ( ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) ) )
                    // InternalMyDsl.g:727:4: ( (lv_type_0_0= 'not' ) ) ( (lv_right_1_0= rulePrimary ) )
                    {
                    // InternalMyDsl.g:727:4: ( (lv_type_0_0= 'not' ) )
                    // InternalMyDsl.g:728:5: (lv_type_0_0= 'not' )
                    {
                    // InternalMyDsl.g:728:5: (lv_type_0_0= 'not' )
                    // InternalMyDsl.g:729:6: lv_type_0_0= 'not'
                    {
                    lv_type_0_0=(Token)match(input,24,FOLLOW_19); 

                    						newLeafNode(lv_type_0_0, grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getNotRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_0_0, "not");
                    					

                    }


                    }

                    // InternalMyDsl.g:741:4: ( (lv_right_1_0= rulePrimary ) )
                    // InternalMyDsl.g:742:5: (lv_right_1_0= rulePrimary )
                    {
                    // InternalMyDsl.g:742:5: (lv_right_1_0= rulePrimary )
                    // InternalMyDsl.g:743:6: lv_right_1_0= rulePrimary
                    {

                    						newCompositeNode(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_right_1_0=rulePrimary();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNotRule());
                    						}
                    						set(
                    							current,
                    							"right",
                    							lv_right_1_0,
                    							"org.xtext.example.mydsl.MyDsl.Primary");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:762:3: ( (lv_right_2_0= rulePrimary ) )
                    {
                    // InternalMyDsl.g:762:3: ( (lv_right_2_0= rulePrimary ) )
                    // InternalMyDsl.g:763:4: (lv_right_2_0= rulePrimary )
                    {
                    // InternalMyDsl.g:763:4: (lv_right_2_0= rulePrimary )
                    // InternalMyDsl.g:764:5: lv_right_2_0= rulePrimary
                    {

                    					newCompositeNode(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_right_2_0=rulePrimary();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getNotRule());
                    					}
                    					set(
                    						current,
                    						"right",
                    						lv_right_2_0,
                    						"org.xtext.example.mydsl.MyDsl.Primary");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:785:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalMyDsl.g:785:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalMyDsl.g:786:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:792:1: rulePrimary returns [EObject current=null] : ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token lv_value_4_0=null;
        Token lv_value_5_0=null;
        EObject this_Biimplies_1 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:798:2: ( ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) ) )
            // InternalMyDsl.g:799:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) )
            {
            // InternalMyDsl.g:799:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' ) | ( (lv_value_3_0= RULE_TRUE ) ) | ( (lv_value_4_0= RULE_FALSE ) ) | ( (lv_value_5_0= RULE_ID ) ) )
            int alt10=4;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt10=1;
                }
                break;
            case RULE_TRUE:
                {
                alt10=2;
                }
                break;
            case RULE_FALSE:
                {
                alt10=3;
                }
                break;
            case RULE_ID:
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalMyDsl.g:800:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    {
                    // InternalMyDsl.g:800:3: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')' )
                    // InternalMyDsl.g:801:4: otherlv_0= '(' this_Biimplies_1= ruleBiimplies otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,25,FOLLOW_20); 

                    				newLeafNode(otherlv_0, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_21);
                    this_Biimplies_1=ruleBiimplies();

                    state._fsp--;


                    				current = this_Biimplies_1;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_2=(Token)match(input,26,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:819:3: ( (lv_value_3_0= RULE_TRUE ) )
                    {
                    // InternalMyDsl.g:819:3: ( (lv_value_3_0= RULE_TRUE ) )
                    // InternalMyDsl.g:820:4: (lv_value_3_0= RULE_TRUE )
                    {
                    // InternalMyDsl.g:820:4: (lv_value_3_0= RULE_TRUE )
                    // InternalMyDsl.g:821:5: lv_value_3_0= RULE_TRUE
                    {
                    lv_value_3_0=(Token)match(input,RULE_TRUE,FOLLOW_2); 

                    					newLeafNode(lv_value_3_0, grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_3_0,
                    						"org.xtext.example.mydsl.MyDsl.TRUE");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:838:3: ( (lv_value_4_0= RULE_FALSE ) )
                    {
                    // InternalMyDsl.g:838:3: ( (lv_value_4_0= RULE_FALSE ) )
                    // InternalMyDsl.g:839:4: (lv_value_4_0= RULE_FALSE )
                    {
                    // InternalMyDsl.g:839:4: (lv_value_4_0= RULE_FALSE )
                    // InternalMyDsl.g:840:5: lv_value_4_0= RULE_FALSE
                    {
                    lv_value_4_0=(Token)match(input,RULE_FALSE,FOLLOW_2); 

                    					newLeafNode(lv_value_4_0, grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_4_0,
                    						"org.xtext.example.mydsl.MyDsl.FALSE");
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:857:3: ( (lv_value_5_0= RULE_ID ) )
                    {
                    // InternalMyDsl.g:857:3: ( (lv_value_5_0= RULE_ID ) )
                    // InternalMyDsl.g:858:4: (lv_value_5_0= RULE_ID )
                    {
                    // InternalMyDsl.g:858:4: (lv_value_5_0= RULE_ID )
                    // InternalMyDsl.g:859:5: lv_value_5_0= RULE_ID
                    {
                    lv_value_5_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_value_5_0, grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPrimaryRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_5_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000078000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000030020E2L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000007C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000307E0E2L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000030820E0L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000031020E0L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000000032020E0L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000000034020E0L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00000000038020E0L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x00000000030020E0L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000000070020E0L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000000L});

}