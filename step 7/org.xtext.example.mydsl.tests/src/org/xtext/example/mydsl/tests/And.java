package org.xtext.example.mydsl.tests;

import java.util.Vector;

public class And implements Operator 
{
	Vector<Operator> left;
	Vector<Operator> right;
	
	public And(Vector<Operator> _data, Vector<Operator> _data2)
	{
		left = _data;
		right = _data2;
	}
	
	public Vector<Operator> getValueL()
	{
		return left;
	}
	
	public Vector<Operator> getValueR()
	{
		return right;
	}
	
	public void setValueL(Vector<Operator> _data)
	{
		left = _data;
	}
	
	public void setValueR(Vector<Operator> _data)
	{
		right = _data;
	}
	
	public void appendL(Operator value)
	{
		left.add(value);
	}
	
	public void appendR(Operator value)
	{
		right.add(value);
	}
	
	public String type()
	{
		return "And";
	}
}
