package org.xtext.example.mydsl.tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class PicoSat 
{
	public PicoSat()
	{
		
	}
		
	public static Boolean File(String Filename) throws IOException, InterruptedException 
	{
		Process process = Runtime.getRuntime().exec("picosat " + Filename);
		process.waitFor();
		InputStream flux = process.getInputStream();
		InputStreamReader lecture=new InputStreamReader(flux);
		BufferedReader buff=new BufferedReader(lecture);
		String ligne;
		Boolean satisfDone = false;
		Boolean satisf = false;
		while ((ligne=buff.readLine())!=null){
			if(!satisfDone)
			{
				satisfDone = true;
				ligne = ligne.intern();
				if(ligne == "s SATISFIABLE")
				{
					satisf = true;
					System.out.println("SAT");
				}
				else
				{
					System.out.println("UNSAT");
				}
			}
			else
			{
				System.out.println(ligne);
			}
		}
		buff.close(); 
		return satisf;
	}
	
	public static Boolean main(String[] args) throws IOException, InterruptedException 
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter("File.cnf"));
        PrintWriter pWriter = new PrintWriter(bw);
        pWriter.print(args[0]);
        pWriter.close();
        return File("File.cnf");
	}

}
