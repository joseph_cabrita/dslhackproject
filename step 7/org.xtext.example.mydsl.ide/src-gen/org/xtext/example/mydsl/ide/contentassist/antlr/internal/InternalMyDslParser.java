package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FILE'", "';'", "'('", "')'", "'SAT4J'", "'MiniSat'", "'PicoSat'", "'Comp'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=5;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=7;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__26=26;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_FALSE=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:53:1: entryRuleStart : ruleStart EOF ;
    public final void entryRuleStart() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleStart EOF )
            // InternalMyDsl.g:55:1: ruleStart EOF
            {
             before(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            ruleStart();

            state._fsp--;

             after(grammarAccess.getStartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:62:1: ruleStart : ( ( rule__Start__Alternatives )* ) ;
    public final void ruleStart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Start__Alternatives )* ) )
            // InternalMyDsl.g:67:2: ( ( rule__Start__Alternatives )* )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Start__Alternatives )* )
            // InternalMyDsl.g:68:3: ( rule__Start__Alternatives )*
            {
             before(grammarAccess.getStartAccess().getAlternatives()); 
            // InternalMyDsl.g:69:3: ( rule__Start__Alternatives )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||LA1_0==13||LA1_0==15||LA1_0==26) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:69:4: rule__Start__Alternatives
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Start__Alternatives();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStartAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleSolver"
    // InternalMyDsl.g:78:1: entryRuleSolver : ruleSolver EOF ;
    public final void entryRuleSolver() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleSolver EOF )
            // InternalMyDsl.g:80:1: ruleSolver EOF
            {
             before(grammarAccess.getSolverRule()); 
            pushFollow(FOLLOW_1);
            ruleSolver();

            state._fsp--;

             after(grammarAccess.getSolverRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolver"


    // $ANTLR start "ruleSolver"
    // InternalMyDsl.g:87:1: ruleSolver : ( ( rule__Solver__Alternatives ) ) ;
    public final void ruleSolver() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Solver__Alternatives ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Solver__Alternatives ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Solver__Alternatives ) )
            // InternalMyDsl.g:93:3: ( rule__Solver__Alternatives )
            {
             before(grammarAccess.getSolverAccess().getAlternatives()); 
            // InternalMyDsl.g:94:3: ( rule__Solver__Alternatives )
            // InternalMyDsl.g:94:4: rule__Solver__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolver"


    // $ANTLR start "entryRuleSolverFile"
    // InternalMyDsl.g:103:1: entryRuleSolverFile : ruleSolverFile EOF ;
    public final void entryRuleSolverFile() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleSolverFile EOF )
            // InternalMyDsl.g:105:1: ruleSolverFile EOF
            {
             before(grammarAccess.getSolverFileRule()); 
            pushFollow(FOLLOW_1);
            ruleSolverFile();

            state._fsp--;

             after(grammarAccess.getSolverFileRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolverFile"


    // $ANTLR start "ruleSolverFile"
    // InternalMyDsl.g:112:1: ruleSolverFile : ( ( rule__SolverFile__Alternatives ) ) ;
    public final void ruleSolverFile() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__SolverFile__Alternatives ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__SolverFile__Alternatives ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__SolverFile__Alternatives ) )
            // InternalMyDsl.g:118:3: ( rule__SolverFile__Alternatives )
            {
             before(grammarAccess.getSolverFileAccess().getAlternatives()); 
            // InternalMyDsl.g:119:3: ( rule__SolverFile__Alternatives )
            // InternalMyDsl.g:119:4: rule__SolverFile__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SolverFile__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSolverFileAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolverFile"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:128:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleBiimplies EOF )
            // InternalMyDsl.g:130:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:137:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Biimplies__Group__0 )
            // InternalMyDsl.g:144:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:153:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleImplies EOF )
            // InternalMyDsl.g:155:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:162:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Implies__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Implies__Group__0 )
            // InternalMyDsl.g:169:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:178:1: entryRuleExcludes : ruleExcludes EOF ;
    public final void entryRuleExcludes() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleExcludes EOF )
            // InternalMyDsl.g:180:1: ruleExcludes EOF
            {
             before(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getExcludesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:187:1: ruleExcludes : ( ( rule__Excludes__Group__0 ) ) ;
    public final void ruleExcludes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__Excludes__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__Excludes__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__Excludes__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__Excludes__Group__0 )
            {
             before(grammarAccess.getExcludesAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__Excludes__Group__0 )
            // InternalMyDsl.g:194:4: rule__Excludes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:203:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleOr EOF )
            // InternalMyDsl.g:205:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:212:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__Or__Group__0 ) )
            // InternalMyDsl.g:218:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalMyDsl.g:219:3: ( rule__Or__Group__0 )
            // InternalMyDsl.g:219:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:228:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( ruleAnd EOF )
            // InternalMyDsl.g:230:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:237:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__And__Group__0 ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__And__Group__0 ) )
            // InternalMyDsl.g:243:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalMyDsl.g:244:3: ( rule__And__Group__0 )
            // InternalMyDsl.g:244:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:253:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( ruleNot EOF )
            // InternalMyDsl.g:255:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:262:1: ruleNot : ( ( rule__Not__Alternatives ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__Not__Alternatives ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__Not__Alternatives ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__Not__Alternatives ) )
            // InternalMyDsl.g:268:3: ( rule__Not__Alternatives )
            {
             before(grammarAccess.getNotAccess().getAlternatives()); 
            // InternalMyDsl.g:269:3: ( rule__Not__Alternatives )
            // InternalMyDsl.g:269:4: rule__Not__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Not__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:278:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalMyDsl.g:279:1: ( rulePrimary EOF )
            // InternalMyDsl.g:280:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:287:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:291:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalMyDsl.g:292:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalMyDsl.g:292:2: ( ( rule__Primary__Alternatives ) )
            // InternalMyDsl.g:293:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalMyDsl.g:294:3: ( rule__Primary__Alternatives )
            // InternalMyDsl.g:294:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "rule__Start__Alternatives"
    // InternalMyDsl.g:302:1: rule__Start__Alternatives : ( ( ( rule__Start__Group_0__0 ) ) | ( ( rule__Start__Group_1__0 ) ) );
    public final void rule__Start__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:306:1: ( ( ( rule__Start__Group_0__0 ) ) | ( ( rule__Start__Group_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=RULE_TRUE && LA2_0<=RULE_ID)||LA2_0==15||LA2_0==26) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:307:2: ( ( rule__Start__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:307:2: ( ( rule__Start__Group_0__0 ) )
                    // InternalMyDsl.g:308:3: ( rule__Start__Group_0__0 )
                    {
                     before(grammarAccess.getStartAccess().getGroup_0()); 
                    // InternalMyDsl.g:309:3: ( rule__Start__Group_0__0 )
                    // InternalMyDsl.g:309:4: rule__Start__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Start__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStartAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:313:2: ( ( rule__Start__Group_1__0 ) )
                    {
                    // InternalMyDsl.g:313:2: ( ( rule__Start__Group_1__0 ) )
                    // InternalMyDsl.g:314:3: ( rule__Start__Group_1__0 )
                    {
                     before(grammarAccess.getStartAccess().getGroup_1()); 
                    // InternalMyDsl.g:315:3: ( rule__Start__Group_1__0 )
                    // InternalMyDsl.g:315:4: rule__Start__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Start__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStartAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Alternatives"


    // $ANTLR start "rule__Solver__Alternatives"
    // InternalMyDsl.g:323:1: rule__Solver__Alternatives : ( ( ( rule__Solver__Group_0__0 ) ) | ( ( rule__Solver__Group_1__0 ) ) | ( ( rule__Solver__Group_2__0 ) ) | ( ( rule__Solver__Group_3__0 ) ) | ( ( rule__Solver__ValueAssignment_4 ) ) );
    public final void rule__Solver__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:327:1: ( ( ( rule__Solver__Group_0__0 ) ) | ( ( rule__Solver__Group_1__0 ) ) | ( ( rule__Solver__Group_2__0 ) ) | ( ( rule__Solver__Group_3__0 ) ) | ( ( rule__Solver__ValueAssignment_4 ) ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt3=1;
                }
                break;
            case 18:
                {
                alt3=2;
                }
                break;
            case 19:
                {
                alt3=3;
                }
                break;
            case 20:
                {
                alt3=4;
                }
                break;
            case 14:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:328:2: ( ( rule__Solver__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:328:2: ( ( rule__Solver__Group_0__0 ) )
                    // InternalMyDsl.g:329:3: ( rule__Solver__Group_0__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_0()); 
                    // InternalMyDsl.g:330:3: ( rule__Solver__Group_0__0 )
                    // InternalMyDsl.g:330:4: rule__Solver__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:334:2: ( ( rule__Solver__Group_1__0 ) )
                    {
                    // InternalMyDsl.g:334:2: ( ( rule__Solver__Group_1__0 ) )
                    // InternalMyDsl.g:335:3: ( rule__Solver__Group_1__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_1()); 
                    // InternalMyDsl.g:336:3: ( rule__Solver__Group_1__0 )
                    // InternalMyDsl.g:336:4: rule__Solver__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:340:2: ( ( rule__Solver__Group_2__0 ) )
                    {
                    // InternalMyDsl.g:340:2: ( ( rule__Solver__Group_2__0 ) )
                    // InternalMyDsl.g:341:3: ( rule__Solver__Group_2__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_2()); 
                    // InternalMyDsl.g:342:3: ( rule__Solver__Group_2__0 )
                    // InternalMyDsl.g:342:4: rule__Solver__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:346:2: ( ( rule__Solver__Group_3__0 ) )
                    {
                    // InternalMyDsl.g:346:2: ( ( rule__Solver__Group_3__0 ) )
                    // InternalMyDsl.g:347:3: ( rule__Solver__Group_3__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_3()); 
                    // InternalMyDsl.g:348:3: ( rule__Solver__Group_3__0 )
                    // InternalMyDsl.g:348:4: rule__Solver__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:352:2: ( ( rule__Solver__ValueAssignment_4 ) )
                    {
                    // InternalMyDsl.g:352:2: ( ( rule__Solver__ValueAssignment_4 ) )
                    // InternalMyDsl.g:353:3: ( rule__Solver__ValueAssignment_4 )
                    {
                     before(grammarAccess.getSolverAccess().getValueAssignment_4()); 
                    // InternalMyDsl.g:354:3: ( rule__Solver__ValueAssignment_4 )
                    // InternalMyDsl.g:354:4: rule__Solver__ValueAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__ValueAssignment_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getValueAssignment_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Alternatives"


    // $ANTLR start "rule__SolverFile__Alternatives"
    // InternalMyDsl.g:362:1: rule__SolverFile__Alternatives : ( ( ( rule__SolverFile__ValueAssignment_0 ) ) | ( ( rule__SolverFile__ValueAssignment_1 ) ) | ( ( rule__SolverFile__ValueAssignment_2 ) ) | ( ( rule__SolverFile__ValueAssignment_3 ) ) );
    public final void rule__SolverFile__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:366:1: ( ( ( rule__SolverFile__ValueAssignment_0 ) ) | ( ( rule__SolverFile__ValueAssignment_1 ) ) | ( ( rule__SolverFile__ValueAssignment_2 ) ) | ( ( rule__SolverFile__ValueAssignment_3 ) ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt4=1;
                }
                break;
            case 18:
                {
                alt4=2;
                }
                break;
            case 19:
                {
                alt4=3;
                }
                break;
            case 20:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:367:2: ( ( rule__SolverFile__ValueAssignment_0 ) )
                    {
                    // InternalMyDsl.g:367:2: ( ( rule__SolverFile__ValueAssignment_0 ) )
                    // InternalMyDsl.g:368:3: ( rule__SolverFile__ValueAssignment_0 )
                    {
                     before(grammarAccess.getSolverFileAccess().getValueAssignment_0()); 
                    // InternalMyDsl.g:369:3: ( rule__SolverFile__ValueAssignment_0 )
                    // InternalMyDsl.g:369:4: rule__SolverFile__ValueAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SolverFile__ValueAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverFileAccess().getValueAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:373:2: ( ( rule__SolverFile__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:373:2: ( ( rule__SolverFile__ValueAssignment_1 ) )
                    // InternalMyDsl.g:374:3: ( rule__SolverFile__ValueAssignment_1 )
                    {
                     before(grammarAccess.getSolverFileAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:375:3: ( rule__SolverFile__ValueAssignment_1 )
                    // InternalMyDsl.g:375:4: rule__SolverFile__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SolverFile__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverFileAccess().getValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:379:2: ( ( rule__SolverFile__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:379:2: ( ( rule__SolverFile__ValueAssignment_2 ) )
                    // InternalMyDsl.g:380:3: ( rule__SolverFile__ValueAssignment_2 )
                    {
                     before(grammarAccess.getSolverFileAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:381:3: ( rule__SolverFile__ValueAssignment_2 )
                    // InternalMyDsl.g:381:4: rule__SolverFile__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SolverFile__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverFileAccess().getValueAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:385:2: ( ( rule__SolverFile__ValueAssignment_3 ) )
                    {
                    // InternalMyDsl.g:385:2: ( ( rule__SolverFile__ValueAssignment_3 ) )
                    // InternalMyDsl.g:386:3: ( rule__SolverFile__ValueAssignment_3 )
                    {
                     before(grammarAccess.getSolverFileAccess().getValueAssignment_3()); 
                    // InternalMyDsl.g:387:3: ( rule__SolverFile__ValueAssignment_3 )
                    // InternalMyDsl.g:387:4: rule__SolverFile__ValueAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__SolverFile__ValueAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverFileAccess().getValueAssignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__Alternatives"


    // $ANTLR start "rule__Not__Alternatives"
    // InternalMyDsl.g:395:1: rule__Not__Alternatives : ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) );
    public final void rule__Not__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:399:1: ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==26) ) {
                alt5=1;
            }
            else if ( ((LA5_0>=RULE_TRUE && LA5_0<=RULE_ID)||LA5_0==15) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:400:2: ( ( rule__Not__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:400:2: ( ( rule__Not__Group_0__0 ) )
                    // InternalMyDsl.g:401:3: ( rule__Not__Group_0__0 )
                    {
                     before(grammarAccess.getNotAccess().getGroup_0()); 
                    // InternalMyDsl.g:402:3: ( rule__Not__Group_0__0 )
                    // InternalMyDsl.g:402:4: rule__Not__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:406:2: ( ( rule__Not__RightAssignment_1 ) )
                    {
                    // InternalMyDsl.g:406:2: ( ( rule__Not__RightAssignment_1 ) )
                    // InternalMyDsl.g:407:3: ( rule__Not__RightAssignment_1 )
                    {
                     before(grammarAccess.getNotAccess().getRightAssignment_1()); 
                    // InternalMyDsl.g:408:3: ( rule__Not__RightAssignment_1 )
                    // InternalMyDsl.g:408:4: rule__Not__RightAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__RightAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getRightAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalMyDsl.g:416:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:420:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt6=1;
                }
                break;
            case RULE_TRUE:
                {
                alt6=2;
                }
                break;
            case RULE_FALSE:
                {
                alt6=3;
                }
                break;
            case RULE_ID:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:421:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:421:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalMyDsl.g:422:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalMyDsl.g:423:3: ( rule__Primary__Group_0__0 )
                    // InternalMyDsl.g:423:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:427:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:427:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    // InternalMyDsl.g:428:3: ( rule__Primary__ValueAssignment_1 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:429:3: ( rule__Primary__ValueAssignment_1 )
                    // InternalMyDsl.g:429:4: rule__Primary__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:433:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:433:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    // InternalMyDsl.g:434:3: ( rule__Primary__ValueAssignment_2 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:435:3: ( rule__Primary__ValueAssignment_2 )
                    // InternalMyDsl.g:435:4: rule__Primary__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:439:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    {
                    // InternalMyDsl.g:439:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    // InternalMyDsl.g:440:3: ( rule__Primary__ValueAssignment_3 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 
                    // InternalMyDsl.g:441:3: ( rule__Primary__ValueAssignment_3 )
                    // InternalMyDsl.g:441:4: rule__Primary__ValueAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Start__Group_0__0"
    // InternalMyDsl.g:449:1: rule__Start__Group_0__0 : rule__Start__Group_0__0__Impl rule__Start__Group_0__1 ;
    public final void rule__Start__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:453:1: ( rule__Start__Group_0__0__Impl rule__Start__Group_0__1 )
            // InternalMyDsl.g:454:2: rule__Start__Group_0__0__Impl rule__Start__Group_0__1
            {
            pushFollow(FOLLOW_4);
            rule__Start__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__0"


    // $ANTLR start "rule__Start__Group_0__0__Impl"
    // InternalMyDsl.g:461:1: rule__Start__Group_0__0__Impl : ( 'FILE' ) ;
    public final void rule__Start__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:465:1: ( ( 'FILE' ) )
            // InternalMyDsl.g:466:1: ( 'FILE' )
            {
            // InternalMyDsl.g:466:1: ( 'FILE' )
            // InternalMyDsl.g:467:2: 'FILE'
            {
             before(grammarAccess.getStartAccess().getFILEKeyword_0_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getFILEKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__0__Impl"


    // $ANTLR start "rule__Start__Group_0__1"
    // InternalMyDsl.g:476:1: rule__Start__Group_0__1 : rule__Start__Group_0__1__Impl rule__Start__Group_0__2 ;
    public final void rule__Start__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:480:1: ( rule__Start__Group_0__1__Impl rule__Start__Group_0__2 )
            // InternalMyDsl.g:481:2: rule__Start__Group_0__1__Impl rule__Start__Group_0__2
            {
            pushFollow(FOLLOW_5);
            rule__Start__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__1"


    // $ANTLR start "rule__Start__Group_0__1__Impl"
    // InternalMyDsl.g:488:1: rule__Start__Group_0__1__Impl : ( ( rule__Start__FileAssignment_0_1 ) ) ;
    public final void rule__Start__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:492:1: ( ( ( rule__Start__FileAssignment_0_1 ) ) )
            // InternalMyDsl.g:493:1: ( ( rule__Start__FileAssignment_0_1 ) )
            {
            // InternalMyDsl.g:493:1: ( ( rule__Start__FileAssignment_0_1 ) )
            // InternalMyDsl.g:494:2: ( rule__Start__FileAssignment_0_1 )
            {
             before(grammarAccess.getStartAccess().getFileAssignment_0_1()); 
            // InternalMyDsl.g:495:2: ( rule__Start__FileAssignment_0_1 )
            // InternalMyDsl.g:495:3: rule__Start__FileAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Start__FileAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFileAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__1__Impl"


    // $ANTLR start "rule__Start__Group_0__2"
    // InternalMyDsl.g:503:1: rule__Start__Group_0__2 : rule__Start__Group_0__2__Impl rule__Start__Group_0__3 ;
    public final void rule__Start__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:507:1: ( rule__Start__Group_0__2__Impl rule__Start__Group_0__3 )
            // InternalMyDsl.g:508:2: rule__Start__Group_0__2__Impl rule__Start__Group_0__3
            {
            pushFollow(FOLLOW_6);
            rule__Start__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__2"


    // $ANTLR start "rule__Start__Group_0__2__Impl"
    // InternalMyDsl.g:515:1: rule__Start__Group_0__2__Impl : ( ( rule__Start__SATFAssignment_0_2 ) ) ;
    public final void rule__Start__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:519:1: ( ( ( rule__Start__SATFAssignment_0_2 ) ) )
            // InternalMyDsl.g:520:1: ( ( rule__Start__SATFAssignment_0_2 ) )
            {
            // InternalMyDsl.g:520:1: ( ( rule__Start__SATFAssignment_0_2 ) )
            // InternalMyDsl.g:521:2: ( rule__Start__SATFAssignment_0_2 )
            {
             before(grammarAccess.getStartAccess().getSATFAssignment_0_2()); 
            // InternalMyDsl.g:522:2: ( rule__Start__SATFAssignment_0_2 )
            // InternalMyDsl.g:522:3: rule__Start__SATFAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Start__SATFAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getSATFAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__2__Impl"


    // $ANTLR start "rule__Start__Group_0__3"
    // InternalMyDsl.g:530:1: rule__Start__Group_0__3 : rule__Start__Group_0__3__Impl ;
    public final void rule__Start__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:534:1: ( rule__Start__Group_0__3__Impl )
            // InternalMyDsl.g:535:2: rule__Start__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__3"


    // $ANTLR start "rule__Start__Group_0__3__Impl"
    // InternalMyDsl.g:541:1: rule__Start__Group_0__3__Impl : ( ';' ) ;
    public final void rule__Start__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:545:1: ( ( ';' ) )
            // InternalMyDsl.g:546:1: ( ';' )
            {
            // InternalMyDsl.g:546:1: ( ';' )
            // InternalMyDsl.g:547:2: ';'
            {
             before(grammarAccess.getStartAccess().getSemicolonKeyword_0_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getSemicolonKeyword_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__3__Impl"


    // $ANTLR start "rule__Start__Group_1__0"
    // InternalMyDsl.g:557:1: rule__Start__Group_1__0 : rule__Start__Group_1__0__Impl rule__Start__Group_1__1 ;
    public final void rule__Start__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:561:1: ( rule__Start__Group_1__0__Impl rule__Start__Group_1__1 )
            // InternalMyDsl.g:562:2: rule__Start__Group_1__0__Impl rule__Start__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Start__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__0"


    // $ANTLR start "rule__Start__Group_1__0__Impl"
    // InternalMyDsl.g:569:1: rule__Start__Group_1__0__Impl : ( ( rule__Start__FormulaAssignment_1_0 ) ) ;
    public final void rule__Start__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:573:1: ( ( ( rule__Start__FormulaAssignment_1_0 ) ) )
            // InternalMyDsl.g:574:1: ( ( rule__Start__FormulaAssignment_1_0 ) )
            {
            // InternalMyDsl.g:574:1: ( ( rule__Start__FormulaAssignment_1_0 ) )
            // InternalMyDsl.g:575:2: ( rule__Start__FormulaAssignment_1_0 )
            {
             before(grammarAccess.getStartAccess().getFormulaAssignment_1_0()); 
            // InternalMyDsl.g:576:2: ( rule__Start__FormulaAssignment_1_0 )
            // InternalMyDsl.g:576:3: rule__Start__FormulaAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Start__FormulaAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFormulaAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__0__Impl"


    // $ANTLR start "rule__Start__Group_1__1"
    // InternalMyDsl.g:584:1: rule__Start__Group_1__1 : rule__Start__Group_1__1__Impl ;
    public final void rule__Start__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:588:1: ( rule__Start__Group_1__1__Impl )
            // InternalMyDsl.g:589:2: rule__Start__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__1"


    // $ANTLR start "rule__Start__Group_1__1__Impl"
    // InternalMyDsl.g:595:1: rule__Start__Group_1__1__Impl : ( ( rule__Start__SATAssignment_1_1 ) ) ;
    public final void rule__Start__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:599:1: ( ( ( rule__Start__SATAssignment_1_1 ) ) )
            // InternalMyDsl.g:600:1: ( ( rule__Start__SATAssignment_1_1 ) )
            {
            // InternalMyDsl.g:600:1: ( ( rule__Start__SATAssignment_1_1 ) )
            // InternalMyDsl.g:601:2: ( rule__Start__SATAssignment_1_1 )
            {
             before(grammarAccess.getStartAccess().getSATAssignment_1_1()); 
            // InternalMyDsl.g:602:2: ( rule__Start__SATAssignment_1_1 )
            // InternalMyDsl.g:602:3: rule__Start__SATAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Start__SATAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getSATAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__1__Impl"


    // $ANTLR start "rule__Solver__Group_0__0"
    // InternalMyDsl.g:611:1: rule__Solver__Group_0__0 : rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1 ;
    public final void rule__Solver__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:615:1: ( rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1 )
            // InternalMyDsl.g:616:2: rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Solver__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__0"


    // $ANTLR start "rule__Solver__Group_0__0__Impl"
    // InternalMyDsl.g:623:1: rule__Solver__Group_0__0__Impl : ( ( rule__Solver__ValueAssignment_0_0 ) ) ;
    public final void rule__Solver__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:627:1: ( ( ( rule__Solver__ValueAssignment_0_0 ) ) )
            // InternalMyDsl.g:628:1: ( ( rule__Solver__ValueAssignment_0_0 ) )
            {
            // InternalMyDsl.g:628:1: ( ( rule__Solver__ValueAssignment_0_0 ) )
            // InternalMyDsl.g:629:2: ( rule__Solver__ValueAssignment_0_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_0_0()); 
            // InternalMyDsl.g:630:2: ( rule__Solver__ValueAssignment_0_0 )
            // InternalMyDsl.g:630:3: rule__Solver__ValueAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__0__Impl"


    // $ANTLR start "rule__Solver__Group_0__1"
    // InternalMyDsl.g:638:1: rule__Solver__Group_0__1 : rule__Solver__Group_0__1__Impl ;
    public final void rule__Solver__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:642:1: ( rule__Solver__Group_0__1__Impl )
            // InternalMyDsl.g:643:2: rule__Solver__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__1"


    // $ANTLR start "rule__Solver__Group_0__1__Impl"
    // InternalMyDsl.g:649:1: rule__Solver__Group_0__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:653:1: ( ( ';' ) )
            // InternalMyDsl.g:654:1: ( ';' )
            {
            // InternalMyDsl.g:654:1: ( ';' )
            // InternalMyDsl.g:655:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_0_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__1__Impl"


    // $ANTLR start "rule__Solver__Group_1__0"
    // InternalMyDsl.g:665:1: rule__Solver__Group_1__0 : rule__Solver__Group_1__0__Impl rule__Solver__Group_1__1 ;
    public final void rule__Solver__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:669:1: ( rule__Solver__Group_1__0__Impl rule__Solver__Group_1__1 )
            // InternalMyDsl.g:670:2: rule__Solver__Group_1__0__Impl rule__Solver__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Solver__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__0"


    // $ANTLR start "rule__Solver__Group_1__0__Impl"
    // InternalMyDsl.g:677:1: rule__Solver__Group_1__0__Impl : ( ( rule__Solver__ValueAssignment_1_0 ) ) ;
    public final void rule__Solver__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:681:1: ( ( ( rule__Solver__ValueAssignment_1_0 ) ) )
            // InternalMyDsl.g:682:1: ( ( rule__Solver__ValueAssignment_1_0 ) )
            {
            // InternalMyDsl.g:682:1: ( ( rule__Solver__ValueAssignment_1_0 ) )
            // InternalMyDsl.g:683:2: ( rule__Solver__ValueAssignment_1_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_1_0()); 
            // InternalMyDsl.g:684:2: ( rule__Solver__ValueAssignment_1_0 )
            // InternalMyDsl.g:684:3: rule__Solver__ValueAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__0__Impl"


    // $ANTLR start "rule__Solver__Group_1__1"
    // InternalMyDsl.g:692:1: rule__Solver__Group_1__1 : rule__Solver__Group_1__1__Impl ;
    public final void rule__Solver__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:696:1: ( rule__Solver__Group_1__1__Impl )
            // InternalMyDsl.g:697:2: rule__Solver__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__1"


    // $ANTLR start "rule__Solver__Group_1__1__Impl"
    // InternalMyDsl.g:703:1: rule__Solver__Group_1__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:707:1: ( ( ';' ) )
            // InternalMyDsl.g:708:1: ( ';' )
            {
            // InternalMyDsl.g:708:1: ( ';' )
            // InternalMyDsl.g:709:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_1_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__1__Impl"


    // $ANTLR start "rule__Solver__Group_2__0"
    // InternalMyDsl.g:719:1: rule__Solver__Group_2__0 : rule__Solver__Group_2__0__Impl rule__Solver__Group_2__1 ;
    public final void rule__Solver__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:723:1: ( rule__Solver__Group_2__0__Impl rule__Solver__Group_2__1 )
            // InternalMyDsl.g:724:2: rule__Solver__Group_2__0__Impl rule__Solver__Group_2__1
            {
            pushFollow(FOLLOW_6);
            rule__Solver__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_2__0"


    // $ANTLR start "rule__Solver__Group_2__0__Impl"
    // InternalMyDsl.g:731:1: rule__Solver__Group_2__0__Impl : ( ( rule__Solver__ValueAssignment_2_0 ) ) ;
    public final void rule__Solver__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:735:1: ( ( ( rule__Solver__ValueAssignment_2_0 ) ) )
            // InternalMyDsl.g:736:1: ( ( rule__Solver__ValueAssignment_2_0 ) )
            {
            // InternalMyDsl.g:736:1: ( ( rule__Solver__ValueAssignment_2_0 ) )
            // InternalMyDsl.g:737:2: ( rule__Solver__ValueAssignment_2_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_2_0()); 
            // InternalMyDsl.g:738:2: ( rule__Solver__ValueAssignment_2_0 )
            // InternalMyDsl.g:738:3: rule__Solver__ValueAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_2__0__Impl"


    // $ANTLR start "rule__Solver__Group_2__1"
    // InternalMyDsl.g:746:1: rule__Solver__Group_2__1 : rule__Solver__Group_2__1__Impl ;
    public final void rule__Solver__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:750:1: ( rule__Solver__Group_2__1__Impl )
            // InternalMyDsl.g:751:2: rule__Solver__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_2__1"


    // $ANTLR start "rule__Solver__Group_2__1__Impl"
    // InternalMyDsl.g:757:1: rule__Solver__Group_2__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:761:1: ( ( ';' ) )
            // InternalMyDsl.g:762:1: ( ';' )
            {
            // InternalMyDsl.g:762:1: ( ';' )
            // InternalMyDsl.g:763:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_2_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_2__1__Impl"


    // $ANTLR start "rule__Solver__Group_3__0"
    // InternalMyDsl.g:773:1: rule__Solver__Group_3__0 : rule__Solver__Group_3__0__Impl rule__Solver__Group_3__1 ;
    public final void rule__Solver__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:777:1: ( rule__Solver__Group_3__0__Impl rule__Solver__Group_3__1 )
            // InternalMyDsl.g:778:2: rule__Solver__Group_3__0__Impl rule__Solver__Group_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Solver__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_3__0"


    // $ANTLR start "rule__Solver__Group_3__0__Impl"
    // InternalMyDsl.g:785:1: rule__Solver__Group_3__0__Impl : ( ( rule__Solver__ValueAssignment_3_0 ) ) ;
    public final void rule__Solver__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:789:1: ( ( ( rule__Solver__ValueAssignment_3_0 ) ) )
            // InternalMyDsl.g:790:1: ( ( rule__Solver__ValueAssignment_3_0 ) )
            {
            // InternalMyDsl.g:790:1: ( ( rule__Solver__ValueAssignment_3_0 ) )
            // InternalMyDsl.g:791:2: ( rule__Solver__ValueAssignment_3_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_3_0()); 
            // InternalMyDsl.g:792:2: ( rule__Solver__ValueAssignment_3_0 )
            // InternalMyDsl.g:792:3: rule__Solver__ValueAssignment_3_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_3_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_3__0__Impl"


    // $ANTLR start "rule__Solver__Group_3__1"
    // InternalMyDsl.g:800:1: rule__Solver__Group_3__1 : rule__Solver__Group_3__1__Impl ;
    public final void rule__Solver__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:804:1: ( rule__Solver__Group_3__1__Impl )
            // InternalMyDsl.g:805:2: rule__Solver__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_3__1"


    // $ANTLR start "rule__Solver__Group_3__1__Impl"
    // InternalMyDsl.g:811:1: rule__Solver__Group_3__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:815:1: ( ( ';' ) )
            // InternalMyDsl.g:816:1: ( ';' )
            {
            // InternalMyDsl.g:816:1: ( ';' )
            // InternalMyDsl.g:817:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_3_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_3__1__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalMyDsl.g:827:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:831:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalMyDsl.g:832:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalMyDsl.g:839:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:843:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:844:1: ( ruleImplies )
            {
            // InternalMyDsl.g:844:1: ( ruleImplies )
            // InternalMyDsl.g:845:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalMyDsl.g:854:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:858:1: ( rule__Biimplies__Group__1__Impl )
            // InternalMyDsl.g:859:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalMyDsl.g:865:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:869:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalMyDsl.g:870:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalMyDsl.g:870:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalMyDsl.g:871:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:872:2: ( rule__Biimplies__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==21) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:872:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalMyDsl.g:881:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:885:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalMyDsl.g:886:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalMyDsl.g:893:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:897:1: ( ( () ) )
            // InternalMyDsl.g:898:1: ( () )
            {
            // InternalMyDsl.g:898:1: ( () )
            // InternalMyDsl.g:899:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalMyDsl.g:900:2: ()
            // InternalMyDsl.g:900:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalMyDsl.g:908:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:912:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalMyDsl.g:913:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalMyDsl.g:920:1: rule__Biimplies__Group_1__1__Impl : ( ( rule__Biimplies__TypeAssignment_1_1 ) ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:924:1: ( ( ( rule__Biimplies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:925:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:925:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:926:2: ( rule__Biimplies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:927:2: ( rule__Biimplies__TypeAssignment_1_1 )
            // InternalMyDsl.g:927:3: rule__Biimplies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalMyDsl.g:935:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:939:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalMyDsl.g:940:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalMyDsl.g:946:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:950:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:951:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:951:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:952:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:953:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalMyDsl.g:953:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalMyDsl.g:962:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:966:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalMyDsl.g:967:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalMyDsl.g:974:1: rule__Implies__Group__0__Impl : ( ruleExcludes ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:978:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:979:1: ( ruleExcludes )
            {
            // InternalMyDsl.g:979:1: ( ruleExcludes )
            // InternalMyDsl.g:980:2: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalMyDsl.g:989:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:993:1: ( rule__Implies__Group__1__Impl )
            // InternalMyDsl.g:994:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalMyDsl.g:1000:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1004:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalMyDsl.g:1005:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalMyDsl.g:1005:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalMyDsl.g:1006:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:1007:2: ( rule__Implies__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==22) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:1007:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalMyDsl.g:1016:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1020:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalMyDsl.g:1021:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalMyDsl.g:1028:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1032:1: ( ( () ) )
            // InternalMyDsl.g:1033:1: ( () )
            {
            // InternalMyDsl.g:1033:1: ( () )
            // InternalMyDsl.g:1034:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalMyDsl.g:1035:2: ()
            // InternalMyDsl.g:1035:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalMyDsl.g:1043:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1047:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalMyDsl.g:1048:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalMyDsl.g:1055:1: rule__Implies__Group_1__1__Impl : ( ( rule__Implies__TypeAssignment_1_1 ) ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1059:1: ( ( ( rule__Implies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1060:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1060:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1061:2: ( rule__Implies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1062:2: ( rule__Implies__TypeAssignment_1_1 )
            // InternalMyDsl.g:1062:3: rule__Implies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Implies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalMyDsl.g:1070:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1074:1: ( rule__Implies__Group_1__2__Impl )
            // InternalMyDsl.g:1075:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalMyDsl.g:1081:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1085:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1086:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1086:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1087:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1088:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalMyDsl.g:1088:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__Excludes__Group__0"
    // InternalMyDsl.g:1097:1: rule__Excludes__Group__0 : rule__Excludes__Group__0__Impl rule__Excludes__Group__1 ;
    public final void rule__Excludes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1101:1: ( rule__Excludes__Group__0__Impl rule__Excludes__Group__1 )
            // InternalMyDsl.g:1102:2: rule__Excludes__Group__0__Impl rule__Excludes__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Excludes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0"


    // $ANTLR start "rule__Excludes__Group__0__Impl"
    // InternalMyDsl.g:1109:1: rule__Excludes__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Excludes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1113:1: ( ( ruleOr ) )
            // InternalMyDsl.g:1114:1: ( ruleOr )
            {
            // InternalMyDsl.g:1114:1: ( ruleOr )
            // InternalMyDsl.g:1115:2: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0__Impl"


    // $ANTLR start "rule__Excludes__Group__1"
    // InternalMyDsl.g:1124:1: rule__Excludes__Group__1 : rule__Excludes__Group__1__Impl ;
    public final void rule__Excludes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1128:1: ( rule__Excludes__Group__1__Impl )
            // InternalMyDsl.g:1129:2: rule__Excludes__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1"


    // $ANTLR start "rule__Excludes__Group__1__Impl"
    // InternalMyDsl.g:1135:1: rule__Excludes__Group__1__Impl : ( ( rule__Excludes__Group_1__0 )* ) ;
    public final void rule__Excludes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1139:1: ( ( ( rule__Excludes__Group_1__0 )* ) )
            // InternalMyDsl.g:1140:1: ( ( rule__Excludes__Group_1__0 )* )
            {
            // InternalMyDsl.g:1140:1: ( ( rule__Excludes__Group_1__0 )* )
            // InternalMyDsl.g:1141:2: ( rule__Excludes__Group_1__0 )*
            {
             before(grammarAccess.getExcludesAccess().getGroup_1()); 
            // InternalMyDsl.g:1142:2: ( rule__Excludes__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==23) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:1142:3: rule__Excludes__Group_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Excludes__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getExcludesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__0"
    // InternalMyDsl.g:1151:1: rule__Excludes__Group_1__0 : rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 ;
    public final void rule__Excludes__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1155:1: ( rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 )
            // InternalMyDsl.g:1156:2: rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1
            {
            pushFollow(FOLLOW_13);
            rule__Excludes__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0"


    // $ANTLR start "rule__Excludes__Group_1__0__Impl"
    // InternalMyDsl.g:1163:1: rule__Excludes__Group_1__0__Impl : ( () ) ;
    public final void rule__Excludes__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1167:1: ( ( () ) )
            // InternalMyDsl.g:1168:1: ( () )
            {
            // InternalMyDsl.g:1168:1: ( () )
            // InternalMyDsl.g:1169:2: ()
            {
             before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 
            // InternalMyDsl.g:1170:2: ()
            // InternalMyDsl.g:1170:3: 
            {
            }

             after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0__Impl"


    // $ANTLR start "rule__Excludes__Group_1__1"
    // InternalMyDsl.g:1178:1: rule__Excludes__Group_1__1 : rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 ;
    public final void rule__Excludes__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1182:1: ( rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 )
            // InternalMyDsl.g:1183:2: rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Excludes__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1"


    // $ANTLR start "rule__Excludes__Group_1__1__Impl"
    // InternalMyDsl.g:1190:1: rule__Excludes__Group_1__1__Impl : ( ( rule__Excludes__TypeAssignment_1_1 ) ) ;
    public final void rule__Excludes__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1194:1: ( ( ( rule__Excludes__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1195:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1195:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1196:2: ( rule__Excludes__TypeAssignment_1_1 )
            {
             before(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1197:2: ( rule__Excludes__TypeAssignment_1_1 )
            // InternalMyDsl.g:1197:3: rule__Excludes__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__2"
    // InternalMyDsl.g:1205:1: rule__Excludes__Group_1__2 : rule__Excludes__Group_1__2__Impl ;
    public final void rule__Excludes__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1209:1: ( rule__Excludes__Group_1__2__Impl )
            // InternalMyDsl.g:1210:2: rule__Excludes__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2"


    // $ANTLR start "rule__Excludes__Group_1__2__Impl"
    // InternalMyDsl.g:1216:1: rule__Excludes__Group_1__2__Impl : ( ( rule__Excludes__RightAssignment_1_2 ) ) ;
    public final void rule__Excludes__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1220:1: ( ( ( rule__Excludes__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1221:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1221:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1222:2: ( rule__Excludes__RightAssignment_1_2 )
            {
             before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1223:2: ( rule__Excludes__RightAssignment_1_2 )
            // InternalMyDsl.g:1223:3: rule__Excludes__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalMyDsl.g:1232:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1236:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalMyDsl.g:1237:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalMyDsl.g:1244:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1248:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1249:1: ( ruleAnd )
            {
            // InternalMyDsl.g:1249:1: ( ruleAnd )
            // InternalMyDsl.g:1250:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalMyDsl.g:1259:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1263:1: ( rule__Or__Group__1__Impl )
            // InternalMyDsl.g:1264:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalMyDsl.g:1270:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1274:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalMyDsl.g:1275:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalMyDsl.g:1275:1: ( ( rule__Or__Group_1__0 )* )
            // InternalMyDsl.g:1276:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalMyDsl.g:1277:2: ( rule__Or__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==24) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:1277:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalMyDsl.g:1286:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1290:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalMyDsl.g:1291:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalMyDsl.g:1298:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1302:1: ( ( () ) )
            // InternalMyDsl.g:1303:1: ( () )
            {
            // InternalMyDsl.g:1303:1: ( () )
            // InternalMyDsl.g:1304:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalMyDsl.g:1305:2: ()
            // InternalMyDsl.g:1305:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalMyDsl.g:1313:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1317:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalMyDsl.g:1318:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalMyDsl.g:1325:1: rule__Or__Group_1__1__Impl : ( ( rule__Or__TypeAssignment_1_1 ) ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1329:1: ( ( ( rule__Or__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1330:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1330:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1331:2: ( rule__Or__TypeAssignment_1_1 )
            {
             before(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1332:2: ( rule__Or__TypeAssignment_1_1 )
            // InternalMyDsl.g:1332:3: rule__Or__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Or__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalMyDsl.g:1340:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1344:1: ( rule__Or__Group_1__2__Impl )
            // InternalMyDsl.g:1345:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalMyDsl.g:1351:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1355:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1356:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1356:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1357:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1358:2: ( rule__Or__RightAssignment_1_2 )
            // InternalMyDsl.g:1358:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalMyDsl.g:1367:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1371:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalMyDsl.g:1372:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalMyDsl.g:1379:1: rule__And__Group__0__Impl : ( ruleNot ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1383:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1384:1: ( ruleNot )
            {
            // InternalMyDsl.g:1384:1: ( ruleNot )
            // InternalMyDsl.g:1385:2: ruleNot
            {
             before(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalMyDsl.g:1394:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1398:1: ( rule__And__Group__1__Impl )
            // InternalMyDsl.g:1399:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalMyDsl.g:1405:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1409:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalMyDsl.g:1410:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalMyDsl.g:1410:1: ( ( rule__And__Group_1__0 )* )
            // InternalMyDsl.g:1411:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalMyDsl.g:1412:2: ( rule__And__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==25) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMyDsl.g:1412:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalMyDsl.g:1421:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1425:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalMyDsl.g:1426:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalMyDsl.g:1433:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1437:1: ( ( () ) )
            // InternalMyDsl.g:1438:1: ( () )
            {
            // InternalMyDsl.g:1438:1: ( () )
            // InternalMyDsl.g:1439:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalMyDsl.g:1440:2: ()
            // InternalMyDsl.g:1440:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalMyDsl.g:1448:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1452:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalMyDsl.g:1453:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalMyDsl.g:1460:1: rule__And__Group_1__1__Impl : ( ( rule__And__TypeAssignment_1_1 ) ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1464:1: ( ( ( rule__And__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1465:1: ( ( rule__And__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1465:1: ( ( rule__And__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1466:2: ( rule__And__TypeAssignment_1_1 )
            {
             before(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1467:2: ( rule__And__TypeAssignment_1_1 )
            // InternalMyDsl.g:1467:3: rule__And__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__And__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalMyDsl.g:1475:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1479:1: ( rule__And__Group_1__2__Impl )
            // InternalMyDsl.g:1480:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalMyDsl.g:1486:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1490:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1491:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1491:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1492:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1493:2: ( rule__And__RightAssignment_1_2 )
            // InternalMyDsl.g:1493:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group_0__0"
    // InternalMyDsl.g:1502:1: rule__Not__Group_0__0 : rule__Not__Group_0__0__Impl rule__Not__Group_0__1 ;
    public final void rule__Not__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1506:1: ( rule__Not__Group_0__0__Impl rule__Not__Group_0__1 )
            // InternalMyDsl.g:1507:2: rule__Not__Group_0__0__Impl rule__Not__Group_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Not__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0"


    // $ANTLR start "rule__Not__Group_0__0__Impl"
    // InternalMyDsl.g:1514:1: rule__Not__Group_0__0__Impl : ( ( rule__Not__TypeAssignment_0_0 ) ) ;
    public final void rule__Not__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1518:1: ( ( ( rule__Not__TypeAssignment_0_0 ) ) )
            // InternalMyDsl.g:1519:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            {
            // InternalMyDsl.g:1519:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            // InternalMyDsl.g:1520:2: ( rule__Not__TypeAssignment_0_0 )
            {
             before(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 
            // InternalMyDsl.g:1521:2: ( rule__Not__TypeAssignment_0_0 )
            // InternalMyDsl.g:1521:3: rule__Not__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Not__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0__Impl"


    // $ANTLR start "rule__Not__Group_0__1"
    // InternalMyDsl.g:1529:1: rule__Not__Group_0__1 : rule__Not__Group_0__1__Impl ;
    public final void rule__Not__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1533:1: ( rule__Not__Group_0__1__Impl )
            // InternalMyDsl.g:1534:2: rule__Not__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1"


    // $ANTLR start "rule__Not__Group_0__1__Impl"
    // InternalMyDsl.g:1540:1: rule__Not__Group_0__1__Impl : ( ( rule__Not__RightAssignment_0_1 ) ) ;
    public final void rule__Not__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1544:1: ( ( ( rule__Not__RightAssignment_0_1 ) ) )
            // InternalMyDsl.g:1545:1: ( ( rule__Not__RightAssignment_0_1 ) )
            {
            // InternalMyDsl.g:1545:1: ( ( rule__Not__RightAssignment_0_1 ) )
            // InternalMyDsl.g:1546:2: ( rule__Not__RightAssignment_0_1 )
            {
             before(grammarAccess.getNotAccess().getRightAssignment_0_1()); 
            // InternalMyDsl.g:1547:2: ( rule__Not__RightAssignment_0_1 )
            // InternalMyDsl.g:1547:3: rule__Not__RightAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Not__RightAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getRightAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalMyDsl.g:1556:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1560:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalMyDsl.g:1561:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalMyDsl.g:1568:1: rule__Primary__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1572:1: ( ( '(' ) )
            // InternalMyDsl.g:1573:1: ( '(' )
            {
            // InternalMyDsl.g:1573:1: ( '(' )
            // InternalMyDsl.g:1574:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalMyDsl.g:1583:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1587:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalMyDsl.g:1588:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_19);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalMyDsl.g:1595:1: rule__Primary__Group_0__1__Impl : ( ruleBiimplies ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1599:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1600:1: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1600:1: ( ruleBiimplies )
            // InternalMyDsl.g:1601:2: ruleBiimplies
            {
             before(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalMyDsl.g:1610:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1614:1: ( rule__Primary__Group_0__2__Impl )
            // InternalMyDsl.g:1615:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalMyDsl.g:1621:1: rule__Primary__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1625:1: ( ( ')' ) )
            // InternalMyDsl.g:1626:1: ( ')' )
            {
            // InternalMyDsl.g:1626:1: ( ')' )
            // InternalMyDsl.g:1627:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Start__FileAssignment_0_1"
    // InternalMyDsl.g:1637:1: rule__Start__FileAssignment_0_1 : ( RULE_STRING ) ;
    public final void rule__Start__FileAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1641:1: ( ( RULE_STRING ) )
            // InternalMyDsl.g:1642:2: ( RULE_STRING )
            {
            // InternalMyDsl.g:1642:2: ( RULE_STRING )
            // InternalMyDsl.g:1643:3: RULE_STRING
            {
             before(grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FileAssignment_0_1"


    // $ANTLR start "rule__Start__SATFAssignment_0_2"
    // InternalMyDsl.g:1652:1: rule__Start__SATFAssignment_0_2 : ( ruleSolverFile ) ;
    public final void rule__Start__SATFAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1656:1: ( ( ruleSolverFile ) )
            // InternalMyDsl.g:1657:2: ( ruleSolverFile )
            {
            // InternalMyDsl.g:1657:2: ( ruleSolverFile )
            // InternalMyDsl.g:1658:3: ruleSolverFile
            {
             before(grammarAccess.getStartAccess().getSATFSolverFileParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleSolverFile();

            state._fsp--;

             after(grammarAccess.getStartAccess().getSATFSolverFileParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__SATFAssignment_0_2"


    // $ANTLR start "rule__Start__FormulaAssignment_1_0"
    // InternalMyDsl.g:1667:1: rule__Start__FormulaAssignment_1_0 : ( ruleBiimplies ) ;
    public final void rule__Start__FormulaAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1671:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1672:2: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1672:2: ( ruleBiimplies )
            // InternalMyDsl.g:1673:3: ruleBiimplies
            {
             before(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FormulaAssignment_1_0"


    // $ANTLR start "rule__Start__SATAssignment_1_1"
    // InternalMyDsl.g:1682:1: rule__Start__SATAssignment_1_1 : ( ruleSolver ) ;
    public final void rule__Start__SATAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1686:1: ( ( ruleSolver ) )
            // InternalMyDsl.g:1687:2: ( ruleSolver )
            {
            // InternalMyDsl.g:1687:2: ( ruleSolver )
            // InternalMyDsl.g:1688:3: ruleSolver
            {
             before(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSolver();

            state._fsp--;

             after(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__SATAssignment_1_1"


    // $ANTLR start "rule__Solver__ValueAssignment_0_0"
    // InternalMyDsl.g:1697:1: rule__Solver__ValueAssignment_0_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__Solver__ValueAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1701:1: ( ( ( 'SAT4J' ) ) )
            // InternalMyDsl.g:1702:2: ( ( 'SAT4J' ) )
            {
            // InternalMyDsl.g:1702:2: ( ( 'SAT4J' ) )
            // InternalMyDsl.g:1703:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 
            // InternalMyDsl.g:1704:3: ( 'SAT4J' )
            // InternalMyDsl.g:1705:4: 'SAT4J'
            {
             before(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_0_0"


    // $ANTLR start "rule__Solver__ValueAssignment_1_0"
    // InternalMyDsl.g:1716:1: rule__Solver__ValueAssignment_1_0 : ( ( 'MiniSat' ) ) ;
    public final void rule__Solver__ValueAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1720:1: ( ( ( 'MiniSat' ) ) )
            // InternalMyDsl.g:1721:2: ( ( 'MiniSat' ) )
            {
            // InternalMyDsl.g:1721:2: ( ( 'MiniSat' ) )
            // InternalMyDsl.g:1722:3: ( 'MiniSat' )
            {
             before(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 
            // InternalMyDsl.g:1723:3: ( 'MiniSat' )
            // InternalMyDsl.g:1724:4: 'MiniSat'
            {
             before(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_1_0"


    // $ANTLR start "rule__Solver__ValueAssignment_2_0"
    // InternalMyDsl.g:1735:1: rule__Solver__ValueAssignment_2_0 : ( ( 'PicoSat' ) ) ;
    public final void rule__Solver__ValueAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1739:1: ( ( ( 'PicoSat' ) ) )
            // InternalMyDsl.g:1740:2: ( ( 'PicoSat' ) )
            {
            // InternalMyDsl.g:1740:2: ( ( 'PicoSat' ) )
            // InternalMyDsl.g:1741:3: ( 'PicoSat' )
            {
             before(grammarAccess.getSolverAccess().getValuePicoSatKeyword_2_0_0()); 
            // InternalMyDsl.g:1742:3: ( 'PicoSat' )
            // InternalMyDsl.g:1743:4: 'PicoSat'
            {
             before(grammarAccess.getSolverAccess().getValuePicoSatKeyword_2_0_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValuePicoSatKeyword_2_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValuePicoSatKeyword_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_2_0"


    // $ANTLR start "rule__Solver__ValueAssignment_3_0"
    // InternalMyDsl.g:1754:1: rule__Solver__ValueAssignment_3_0 : ( ( 'Comp' ) ) ;
    public final void rule__Solver__ValueAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1758:1: ( ( ( 'Comp' ) ) )
            // InternalMyDsl.g:1759:2: ( ( 'Comp' ) )
            {
            // InternalMyDsl.g:1759:2: ( ( 'Comp' ) )
            // InternalMyDsl.g:1760:3: ( 'Comp' )
            {
             before(grammarAccess.getSolverAccess().getValueCompKeyword_3_0_0()); 
            // InternalMyDsl.g:1761:3: ( 'Comp' )
            // InternalMyDsl.g:1762:4: 'Comp'
            {
             before(grammarAccess.getSolverAccess().getValueCompKeyword_3_0_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueCompKeyword_3_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueCompKeyword_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_3_0"


    // $ANTLR start "rule__Solver__ValueAssignment_4"
    // InternalMyDsl.g:1773:1: rule__Solver__ValueAssignment_4 : ( ( ';' ) ) ;
    public final void rule__Solver__ValueAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1777:1: ( ( ( ';' ) ) )
            // InternalMyDsl.g:1778:2: ( ( ';' ) )
            {
            // InternalMyDsl.g:1778:2: ( ( ';' ) )
            // InternalMyDsl.g:1779:3: ( ';' )
            {
             before(grammarAccess.getSolverAccess().getValueSemicolonKeyword_4_0()); 
            // InternalMyDsl.g:1780:3: ( ';' )
            // InternalMyDsl.g:1781:4: ';'
            {
             before(grammarAccess.getSolverAccess().getValueSemicolonKeyword_4_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueSemicolonKeyword_4_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueSemicolonKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_4"


    // $ANTLR start "rule__SolverFile__ValueAssignment_0"
    // InternalMyDsl.g:1792:1: rule__SolverFile__ValueAssignment_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__SolverFile__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1796:1: ( ( ( 'SAT4J' ) ) )
            // InternalMyDsl.g:1797:2: ( ( 'SAT4J' ) )
            {
            // InternalMyDsl.g:1797:2: ( ( 'SAT4J' ) )
            // InternalMyDsl.g:1798:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 
            // InternalMyDsl.g:1799:3: ( 'SAT4J' )
            // InternalMyDsl.g:1800:4: 'SAT4J'
            {
             before(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 

            }

             after(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__ValueAssignment_0"


    // $ANTLR start "rule__SolverFile__ValueAssignment_1"
    // InternalMyDsl.g:1811:1: rule__SolverFile__ValueAssignment_1 : ( ( 'MiniSat' ) ) ;
    public final void rule__SolverFile__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1815:1: ( ( ( 'MiniSat' ) ) )
            // InternalMyDsl.g:1816:2: ( ( 'MiniSat' ) )
            {
            // InternalMyDsl.g:1816:2: ( ( 'MiniSat' ) )
            // InternalMyDsl.g:1817:3: ( 'MiniSat' )
            {
             before(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 
            // InternalMyDsl.g:1818:3: ( 'MiniSat' )
            // InternalMyDsl.g:1819:4: 'MiniSat'
            {
             before(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 

            }

             after(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__ValueAssignment_1"


    // $ANTLR start "rule__SolverFile__ValueAssignment_2"
    // InternalMyDsl.g:1830:1: rule__SolverFile__ValueAssignment_2 : ( ( 'PicoSat' ) ) ;
    public final void rule__SolverFile__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1834:1: ( ( ( 'PicoSat' ) ) )
            // InternalMyDsl.g:1835:2: ( ( 'PicoSat' ) )
            {
            // InternalMyDsl.g:1835:2: ( ( 'PicoSat' ) )
            // InternalMyDsl.g:1836:3: ( 'PicoSat' )
            {
             before(grammarAccess.getSolverFileAccess().getValuePicoSatKeyword_2_0()); 
            // InternalMyDsl.g:1837:3: ( 'PicoSat' )
            // InternalMyDsl.g:1838:4: 'PicoSat'
            {
             before(grammarAccess.getSolverFileAccess().getValuePicoSatKeyword_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSolverFileAccess().getValuePicoSatKeyword_2_0()); 

            }

             after(grammarAccess.getSolverFileAccess().getValuePicoSatKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__ValueAssignment_2"


    // $ANTLR start "rule__SolverFile__ValueAssignment_3"
    // InternalMyDsl.g:1849:1: rule__SolverFile__ValueAssignment_3 : ( ( 'Comp' ) ) ;
    public final void rule__SolverFile__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1853:1: ( ( ( 'Comp' ) ) )
            // InternalMyDsl.g:1854:2: ( ( 'Comp' ) )
            {
            // InternalMyDsl.g:1854:2: ( ( 'Comp' ) )
            // InternalMyDsl.g:1855:3: ( 'Comp' )
            {
             before(grammarAccess.getSolverFileAccess().getValueCompKeyword_3_0()); 
            // InternalMyDsl.g:1856:3: ( 'Comp' )
            // InternalMyDsl.g:1857:4: 'Comp'
            {
             before(grammarAccess.getSolverFileAccess().getValueCompKeyword_3_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getSolverFileAccess().getValueCompKeyword_3_0()); 

            }

             after(grammarAccess.getSolverFileAccess().getValueCompKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__ValueAssignment_3"


    // $ANTLR start "rule__Biimplies__TypeAssignment_1_1"
    // InternalMyDsl.g:1868:1: rule__Biimplies__TypeAssignment_1_1 : ( ( '<->' ) ) ;
    public final void rule__Biimplies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1872:1: ( ( ( '<->' ) ) )
            // InternalMyDsl.g:1873:2: ( ( '<->' ) )
            {
            // InternalMyDsl.g:1873:2: ( ( '<->' ) )
            // InternalMyDsl.g:1874:3: ( '<->' )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1875:3: ( '<->' )
            // InternalMyDsl.g:1876:4: '<->'
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__TypeAssignment_1_1"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalMyDsl.g:1887:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1891:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:1892:2: ( ruleImplies )
            {
            // InternalMyDsl.g:1892:2: ( ruleImplies )
            // InternalMyDsl.g:1893:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__TypeAssignment_1_1"
    // InternalMyDsl.g:1902:1: rule__Implies__TypeAssignment_1_1 : ( ( '->' ) ) ;
    public final void rule__Implies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1906:1: ( ( ( '->' ) ) )
            // InternalMyDsl.g:1907:2: ( ( '->' ) )
            {
            // InternalMyDsl.g:1907:2: ( ( '->' ) )
            // InternalMyDsl.g:1908:3: ( '->' )
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1909:3: ( '->' )
            // InternalMyDsl.g:1910:4: '->'
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__TypeAssignment_1_1"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalMyDsl.g:1921:1: rule__Implies__RightAssignment_1_2 : ( ruleExcludes ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1925:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:1926:2: ( ruleExcludes )
            {
            // InternalMyDsl.g:1926:2: ( ruleExcludes )
            // InternalMyDsl.g:1927:3: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__Excludes__TypeAssignment_1_1"
    // InternalMyDsl.g:1936:1: rule__Excludes__TypeAssignment_1_1 : ( ( 'nand' ) ) ;
    public final void rule__Excludes__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1940:1: ( ( ( 'nand' ) ) )
            // InternalMyDsl.g:1941:2: ( ( 'nand' ) )
            {
            // InternalMyDsl.g:1941:2: ( ( 'nand' ) )
            // InternalMyDsl.g:1942:3: ( 'nand' )
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            // InternalMyDsl.g:1943:3: ( 'nand' )
            // InternalMyDsl.g:1944:4: 'nand'
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }

             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__TypeAssignment_1_1"


    // $ANTLR start "rule__Excludes__RightAssignment_1_2"
    // InternalMyDsl.g:1955:1: rule__Excludes__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Excludes__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1959:1: ( ( ruleOr ) )
            // InternalMyDsl.g:1960:2: ( ruleOr )
            {
            // InternalMyDsl.g:1960:2: ( ruleOr )
            // InternalMyDsl.g:1961:3: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__RightAssignment_1_2"


    // $ANTLR start "rule__Or__TypeAssignment_1_1"
    // InternalMyDsl.g:1970:1: rule__Or__TypeAssignment_1_1 : ( ( 'or' ) ) ;
    public final void rule__Or__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1974:1: ( ( ( 'or' ) ) )
            // InternalMyDsl.g:1975:2: ( ( 'or' ) )
            {
            // InternalMyDsl.g:1975:2: ( ( 'or' ) )
            // InternalMyDsl.g:1976:3: ( 'or' )
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            // InternalMyDsl.g:1977:3: ( 'or' )
            // InternalMyDsl.g:1978:4: 'or'
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }

             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__TypeAssignment_1_1"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalMyDsl.g:1989:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1993:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1994:2: ( ruleAnd )
            {
            // InternalMyDsl.g:1994:2: ( ruleAnd )
            // InternalMyDsl.g:1995:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__TypeAssignment_1_1"
    // InternalMyDsl.g:2004:1: rule__And__TypeAssignment_1_1 : ( ( 'and' ) ) ;
    public final void rule__And__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2008:1: ( ( ( 'and' ) ) )
            // InternalMyDsl.g:2009:2: ( ( 'and' ) )
            {
            // InternalMyDsl.g:2009:2: ( ( 'and' ) )
            // InternalMyDsl.g:2010:3: ( 'and' )
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            // InternalMyDsl.g:2011:3: ( 'and' )
            // InternalMyDsl.g:2012:4: 'and'
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }

             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__TypeAssignment_1_1"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalMyDsl.g:2023:1: rule__And__RightAssignment_1_2 : ( ruleNot ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2027:1: ( ( ruleNot ) )
            // InternalMyDsl.g:2028:2: ( ruleNot )
            {
            // InternalMyDsl.g:2028:2: ( ruleNot )
            // InternalMyDsl.g:2029:3: ruleNot
            {
             before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Not__TypeAssignment_0_0"
    // InternalMyDsl.g:2038:1: rule__Not__TypeAssignment_0_0 : ( ( 'not' ) ) ;
    public final void rule__Not__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2042:1: ( ( ( 'not' ) ) )
            // InternalMyDsl.g:2043:2: ( ( 'not' ) )
            {
            // InternalMyDsl.g:2043:2: ( ( 'not' ) )
            // InternalMyDsl.g:2044:3: ( 'not' )
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            // InternalMyDsl.g:2045:3: ( 'not' )
            // InternalMyDsl.g:2046:4: 'not'
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }

             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__TypeAssignment_0_0"


    // $ANTLR start "rule__Not__RightAssignment_0_1"
    // InternalMyDsl.g:2057:1: rule__Not__RightAssignment_0_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2061:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:2062:2: ( rulePrimary )
            {
            // InternalMyDsl.g:2062:2: ( rulePrimary )
            // InternalMyDsl.g:2063:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_0_1"


    // $ANTLR start "rule__Not__RightAssignment_1"
    // InternalMyDsl.g:2072:1: rule__Not__RightAssignment_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2076:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:2077:2: ( rulePrimary )
            {
            // InternalMyDsl.g:2077:2: ( rulePrimary )
            // InternalMyDsl.g:2078:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_1"
    // InternalMyDsl.g:2087:1: rule__Primary__ValueAssignment_1 : ( RULE_TRUE ) ;
    public final void rule__Primary__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2091:1: ( ( RULE_TRUE ) )
            // InternalMyDsl.g:2092:2: ( RULE_TRUE )
            {
            // InternalMyDsl.g:2092:2: ( RULE_TRUE )
            // InternalMyDsl.g:2093:3: RULE_TRUE
            {
             before(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 
            match(input,RULE_TRUE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_2"
    // InternalMyDsl.g:2102:1: rule__Primary__ValueAssignment_2 : ( RULE_FALSE ) ;
    public final void rule__Primary__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2106:1: ( ( RULE_FALSE ) )
            // InternalMyDsl.g:2107:2: ( RULE_FALSE )
            {
            // InternalMyDsl.g:2107:2: ( RULE_FALSE )
            // InternalMyDsl.g:2108:3: RULE_FALSE
            {
             before(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 
            match(input,RULE_FALSE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_2"


    // $ANTLR start "rule__Primary__ValueAssignment_3"
    // InternalMyDsl.g:2117:1: rule__Primary__ValueAssignment_3 : ( RULE_ID ) ;
    public final void rule__Primary__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2121:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:2122:2: ( RULE_ID )
            {
            // InternalMyDsl.g:2122:2: ( RULE_ID )
            // InternalMyDsl.g:2123:3: RULE_ID
            {
             before(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000400A0E2L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000001E4000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000000400A0E0L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000010000L});

}