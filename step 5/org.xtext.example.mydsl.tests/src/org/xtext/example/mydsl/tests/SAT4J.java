package org.xtext.example.mydsl.tests;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.sat4j.maxsat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

public class SAT4J {
	
	public SAT4J()
	{
		
	}
	
	@SuppressWarnings("deprecation")
	public static void File(String Filename) 
	{
		ISolver solver = SolverFactory.newDefault();
		solver.setTimeout(3600); // 1 hour timeout
		Reader reader = new DimacsReader(solver);
		// CNF filename is given on the command line
		try {
			//System.out.println(args[0]);
			IProblem problem = reader.parseInstance(Filename);
			if (problem.isSatisfiable()) 
			{
				System.out.println("SAT");
				System.out.println(reader.decode(problem.model()));
			} 
			else 
			{
				System.out.println("UNSAT");
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("FileNotFound");
			// TODO Auto-generated catch block
		} 
		catch (ParseFormatException e) 
		{
			System.out.println("Error Parsing");
			// TODO Auto-generated catch block
		} 
		catch (IOException e) 
		{
			System.out.println("Input Output Exception");
			// TODO Auto-generated catch block
		} 
		catch (ContradictionException e) 
		{
			System.out.println("UNSAT");
		} 
		catch (TimeoutException e) 
		{
			System.out.println("TIMEOUT");
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) 
	{
		ISolver solver = SolverFactory.newDefault();
		solver.setTimeout(3600); // 1 hour timeout
		Reader reader = new DimacsReader(solver);
		// CNF filename is given on the command line
		try {
			//System.out.println(args[0]);
			IProblem problem = reader.parseInstance(new ByteArrayInputStream(args[0].getBytes(StandardCharsets.UTF_8)));
			if (problem.isSatisfiable()) 
			{
				System.out.println("SAT");
				System.out.println(reader.decode(problem.model()));
			} 
			else 
			{
				System.out.println("UNSAT");
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("FileNotFound");
			// TODO Auto-generated catch block
		} 
		catch (ParseFormatException e) 
		{
			System.out.println("Error Parsing");
			// TODO Auto-generated catch block
		} 
		catch (IOException e) 
		{
			System.out.println("Input Output Exception");
			// TODO Auto-generated catch block
		} 
		catch (ContradictionException e) 
		{
			System.out.println("UNSAT");
		} 
		catch (TimeoutException e) 
		{
			System.out.println("TIMEOUT");
		}
	}
}