package org.xtext.example.mydsl.tests;

import java.util.Vector;

public class Or implements Operator {
	Vector<Operator> data;
	public Or(Vector<Operator> _data)
	{
		data = _data;
	}
	
	public Vector<Operator> getValue()
	{
		return data;
	}
	
	public void setValue(Vector<Operator> _data)
	{
		data = _data;
	}
	
	public void append(Operator value)
	{
		data.add(value);
	}
	
	public String type()
	{
		return "Or";
	}
}
