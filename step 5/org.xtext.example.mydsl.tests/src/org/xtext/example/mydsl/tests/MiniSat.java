package org.xtext.example.mydsl.tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MiniSat 
{
	public MiniSat()
	{
		
	}
		
	public static void File(String Filename) throws IOException, InterruptedException 
	{
		Process process = Runtime.getRuntime().exec("minisat " + Filename + " " + Filename + ".res");
		process.waitFor();
		InputStream flux=new FileInputStream(Filename + ".res");
		InputStreamReader lecture=new InputStreamReader(flux);
		BufferedReader buff=new BufferedReader(lecture);
		String ligne;
		while ((ligne=buff.readLine())!=null){
			System.out.println(ligne);
		}
		buff.close(); 
		Runtime.getRuntime().exec("rm " + Filename + ".res");
	}
	
	public static void main(String[] args) throws IOException, InterruptedException 
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter("File.cnf"));
        PrintWriter pWriter = new PrintWriter(bw);
        pWriter.print(args[0]);
        pWriter.close() ;
        File("File.cnf");
	}

}
