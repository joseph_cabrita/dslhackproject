/**
 * generated by Xtext 2.15.0
 */
package org.xtext.example.mydsl.tests;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.xtext.example.mydsl.myDsl.Expression;
import org.xtext.example.mydsl.myDsl.Solver;
import org.xtext.example.mydsl.myDsl.SolverFile;
import org.xtext.example.mydsl.tests.And;
import org.xtext.example.mydsl.tests.MiniSat;
import org.xtext.example.mydsl.tests.MyDslInjectorProvider;
import org.xtext.example.mydsl.tests.Operator;
import org.xtext.example.mydsl.tests.Or;
import org.xtext.example.mydsl.tests.SAT4J;
import org.xtext.example.mydsl.tests.Value;

@ExtendWith(InjectionExtension.class)
@InjectWith(MyDslInjectorProvider.class)
@SuppressWarnings("all")
public class MyDslParsingTest {
  private int index = 1;
  
  private int lines = 0;
  
  @Inject
  private ParseHelper<Expression> parseHelper;
  
  public String ExpressionToNA(final Expression expr) {
    String _xblockexpression = null;
    {
      String _type = expr.getType();
      boolean _equals = Objects.equal(_type, "<->");
      if (_equals) {
        final String A = this.ExpressionToNA(expr.getLeft());
        final String B = this.ExpressionToNA(expr.getRight());
        return (((((((("((not " + A) + " or ") + B) + ") and ( not ") + B) + " or ") + A) + "))");
      } else {
        String _type_1 = expr.getType();
        boolean _equals_1 = Objects.equal(_type_1, "->");
        if (_equals_1) {
          final String A_1 = this.ExpressionToNA(expr.getLeft());
          final String B_1 = this.ExpressionToNA(expr.getRight());
          return (((("(not " + A_1) + " or ") + B_1) + ")");
        } else {
          String _type_2 = expr.getType();
          boolean _equals_2 = Objects.equal(_type_2, "nand");
          if (_equals_2) {
            final String A_2 = this.ExpressionToNA(expr.getLeft());
            final String B_2 = this.ExpressionToNA(expr.getRight());
            return (((("(not " + A_2) + " or not ") + B_2) + ")");
          } else {
            String _type_3 = expr.getType();
            boolean _equals_3 = Objects.equal(_type_3, "and");
            if (_equals_3) {
              final String A_3 = this.ExpressionToNA(expr.getLeft());
              final String B_3 = this.ExpressionToNA(expr.getRight());
              return (((("(" + A_3) + " and ") + B_3) + ")");
            } else {
              String _type_4 = expr.getType();
              boolean _equals_4 = Objects.equal(_type_4, "or");
              if (_equals_4) {
                final String A_4 = this.ExpressionToNA(expr.getLeft());
                final String B_4 = this.ExpressionToNA(expr.getRight());
                return (((("(" + A_4) + " or ") + B_4) + ")");
              } else {
                String _type_5 = expr.getType();
                boolean _equals_5 = Objects.equal(_type_5, "not");
                if (_equals_5) {
                  final String A_5 = this.ExpressionToNA(expr.getRight());
                  return (("(not " + A_5) + ")");
                } else {
                  String _value = expr.getValue();
                  boolean _notEquals = (!Objects.equal(_value, null));
                  if (_notEquals) {
                    return expr.getValue();
                  } else {
                    Expression _right = expr.getRight();
                    boolean _notEquals_1 = (!Objects.equal(_right, null));
                    if (_notEquals_1) {
                      return this.ExpressionToNA(expr.getRight());
                    }
                  }
                }
              }
            }
          }
        }
      }
      _xblockexpression = Assertions.<String>fail("Error");
    }
    return _xblockexpression;
  }
  
  public String CompressNot(final Expression expr, final Boolean last) {
    if ((Objects.equal(expr.getType(), "not") && Objects.equal(expr.getRight().getType(), "not"))) {
      return this.CompressNot(expr.getRight().getRight(), last);
    } else {
      if ((Objects.equal(expr.getType(), "not") && (!Objects.equal(expr.getRight().getType(), null)))) {
        final String A = this.CompressNot(expr.getRight().getLeft(), last);
        final String B = this.CompressNot(expr.getRight().getRight(), last);
        String exp = "";
        String _type = expr.getRight().getType();
        boolean _equals = Objects.equal(_type, "or");
        if (_equals) {
          exp = " and ";
        } else {
          String _type_1 = expr.getRight().getType();
          boolean _equals_1 = Objects.equal(_type_1, "and");
          if (_equals_1) {
            exp = " or ";
          }
        }
        return (((((("not (" + A) + ")") + exp) + "not (") + B) + ")");
      } else {
        if ((Objects.equal(expr.getType(), "not") && ((last).booleanValue() == false))) {
          String _CompressNot = this.CompressNot(expr.getRight(), last);
          String _plus = ("(not " + _CompressNot);
          return (_plus + ")");
        } else {
          if ((Objects.equal(expr.getType(), "not") && ((last).booleanValue() == true))) {
            String _CompressNot_1 = this.CompressNot(expr.getRight(), last);
            return ("not " + _CompressNot_1);
          } else {
            String _type_2 = expr.getType();
            boolean _notEquals = (!Objects.equal(_type_2, null));
            if (_notEquals) {
              String _type_3 = expr.getType();
              boolean _equals_2 = Objects.equal(_type_3, "or");
              if (_equals_2) {
                String _CompressNot_2 = this.CompressNot(expr.getLeft(), last);
                String _plus_1 = ("(" + _CompressNot_2);
                String _plus_2 = (_plus_1 + " or ");
                String _CompressNot_3 = this.CompressNot(expr.getRight(), last);
                String _plus_3 = (_plus_2 + _CompressNot_3);
                return (_plus_3 + ")");
              } else {
                String _CompressNot_4 = this.CompressNot(expr.getLeft(), last);
                String _plus_4 = (_CompressNot_4 + " and ");
                String _CompressNot_5 = this.CompressNot(expr.getRight(), last);
                return (_plus_4 + _CompressNot_5);
              }
            } else {
              String _value = expr.getValue();
              boolean _notEquals_1 = (!Objects.equal(_value, null));
              if (_notEquals_1) {
                return expr.getValue();
              } else {
                return this.CompressNot(expr.getRight(), last);
              }
            }
          }
        }
      }
    }
  }
  
  public Vector<Operator> TransformToStruct(final Expression expr, final HashMap<String, Integer> map) {
    String _type = expr.getType();
    boolean _equals = Objects.equal(_type, "not");
    if (_equals) {
      Vector<Operator> temp = new Vector<Operator>();
      Integer _get = map.get(expr.getRight().getValue());
      boolean _equals_1 = Objects.equal(_get, null);
      if (_equals_1) {
        map.put(expr.getRight().getValue(), Integer.valueOf(this.index));
        int _index = this.index;
        this.index = (_index + 1);
      }
      Integer _get_1 = map.get(expr.getRight().getValue());
      int _minus = (-(_get_1).intValue());
      Value _value = new Value(_minus);
      temp.add(_value);
      return temp;
    } else {
      String _type_1 = expr.getType();
      boolean _equals_2 = Objects.equal(_type_1, "or");
      if (_equals_2) {
        Vector<Operator> temp_1 = new Vector<Operator>();
        temp_1.addAll(this.TransformToStruct(expr.getLeft(), map));
        temp_1.addAll(this.TransformToStruct(expr.getRight(), map));
        return temp_1;
      } else {
        String _type_2 = expr.getType();
        boolean _equals_3 = Objects.equal(_type_2, "and");
        if (_equals_3) {
          Vector<Operator> temp1 = new Vector<Operator>();
          temp1.addAll(this.TransformToStruct(expr.getLeft(), map));
          Vector<Operator> temp2 = new Vector<Operator>();
          temp2.addAll(this.TransformToStruct(expr.getRight(), map));
          Vector<Operator> temp_2 = new Vector<Operator>();
          And _and = new And(temp1, temp2);
          temp_2.add(_and);
          return temp_2;
        } else {
          String _value_1 = expr.getValue();
          boolean _notEquals = (!Objects.equal(_value_1, null));
          if (_notEquals) {
            Vector<Operator> temp_3 = new Vector<Operator>();
            Integer _get_2 = map.get(expr.getValue());
            boolean _equals_4 = Objects.equal(_get_2, null);
            if (_equals_4) {
              map.put(expr.getValue(), Integer.valueOf(this.index));
              int _index_1 = this.index;
              this.index = (_index_1 + 1);
            }
            Integer _get_3 = map.get(expr.getValue());
            Value _value_2 = new Value((_get_3).intValue());
            temp_3.add(_value_2);
            return temp_3;
          } else {
            Expression _right = expr.getRight();
            boolean _notEquals_1 = (!Objects.equal(_right, null));
            if (_notEquals_1) {
              return this.TransformToStruct(expr.getRight(), map);
            }
          }
        }
      }
    }
    return null;
  }
  
  public Vector<Operator> ToFNC(final Vector<Operator> expr) {
    Vector<Operator> values = new Vector<Operator>();
    Vector<Operator> _vector = new Vector<Operator>();
    Vector<Operator> _vector_1 = new Vector<Operator>();
    And and = new And(_vector, _vector_1);
    boolean oneAnd = false;
    Iterator<Operator> itr = expr.iterator();
    while (itr.hasNext()) {
      {
        Operator op = itr.next();
        if ((Objects.equal(op.type(), "Value") || oneAnd)) {
          values.add(op);
        } else {
          String _type = op.type();
          boolean _equals = Objects.equal(_type, "And");
          if (_equals) {
            and = ((And) op);
            oneAnd = true;
          }
        }
      }
    }
    if (oneAnd) {
      Vector<Operator> valL = new Vector<Operator>();
      valL.addAll(values);
      valL.addAll(this.ToFNC(and.getValueL()));
      Vector<Operator> valR = new Vector<Operator>();
      valR.addAll(values);
      valR.addAll(this.ToFNC(and.getValueR()));
      And preret = new And(valL, valR);
      Vector<Operator> ret = new Vector<Operator>();
      ret.add(preret);
      return ret;
    } else {
      return values;
    }
  }
  
  public void Print(final Vector<Operator> toPrint) {
    Iterator<Operator> itr = toPrint.iterator();
    while (itr.hasNext()) {
      {
        Operator op = itr.next();
        String _type = op.type();
        boolean _equals = Objects.equal(_type, "Value");
        if (_equals) {
          System.out.printf("%s ", Integer.valueOf(((Value) op).getValue()));
        } else {
          String _type_1 = op.type();
          boolean _equals_1 = Objects.equal(_type_1, "And");
          if (_equals_1) {
            System.out.printf("And([");
            this.Print(((And) op).getValueL());
            System.out.printf("][");
            this.Print(((And) op).getValueR());
            System.out.printf("])");
          } else {
            String _type_2 = op.type();
            boolean _equals_2 = Objects.equal(_type_2, "Or");
            if (_equals_2) {
              System.out.printf("Or{");
              this.Print(((Or) op).getValue());
              System.out.printf("}");
            } else {
              System.out.println(op);
              System.out.println(op.type());
            }
          }
        }
      }
    }
  }
  
  public Boolean EqualExpr(final Vector<Operator> toCompare1, final Vector<Operator> toCompare2) {
    int _size = toCompare1.size();
    int _size_1 = toCompare2.size();
    boolean _equals = (_size == _size_1);
    if (_equals) {
      boolean ret = true;
      for (int i = 0; (i < toCompare1.size()); i++) {
        {
          Operator op1 = toCompare1.get(i);
          Operator op2 = toCompare2.get(i);
          if (((Objects.equal(op1.type(), "Value") && Objects.equal(op2.type(), "Value")) && 
            (((Value) op1).getValue() == ((Value) op2).getValue()))) {
          } else {
            if ((((Objects.equal(op1.type(), "And") && Objects.equal(op2.type(), "And")) && 
              (this.EqualExpr(((And) op1).getValueL(), ((And) op2).getValueL())).booleanValue()) && 
              (this.EqualExpr(((And) op1).getValueR(), ((And) op2).getValueR())).booleanValue())) {
            } else {
              ret = false;
            }
          }
        }
      }
      return Boolean.valueOf(ret);
    }
    return Boolean.valueOf(false);
  }
  
  public String ToCNF(final Vector<Operator> expr, final Boolean start, final Collection<Integer> viewed) {
    String str = "";
    if ((start).booleanValue()) {
      this.lines = 0;
    }
    String _type = expr.get(0).type();
    boolean _equals = Objects.equal(_type, "And");
    if (_equals) {
      Operator _get = expr.get(0);
      String _ToCNF = this.ToCNF(((And) _get).getValueL(), Boolean.valueOf(false), viewed);
      String _plus = (_ToCNF + "\n");
      Operator _get_1 = expr.get(0);
      String _ToCNF_1 = this.ToCNF(((And) _get_1).getValueR(), Boolean.valueOf(false), viewed);
      String _plus_1 = (_plus + _ToCNF_1);
      str = _plus_1;
    } else {
      int size = expr.size();
      boolean init = false;
      int i = 0;
      while ((size > 0)) {
        if (((!init) && (size == 1))) {
          String _str = str;
          Operator _get_2 = expr.get(0);
          String _string = Integer.valueOf(((Value) _get_2).getValue()).toString();
          String _plus_2 = (_string + " 0");
          str = (_str + _plus_2);
          size = 0;
          int _lines = this.lines;
          this.lines = (_lines + 1);
          Operator _get_3 = expr.get(0);
          viewed.add(Integer.valueOf(Math.abs(((Value) _get_3).getValue())));
        } else {
          if (((!init) && (size == 2))) {
            String _str_1 = str;
            Operator _get_4 = expr.get(0);
            String _string_1 = Integer.valueOf(((Value) _get_4).getValue()).toString();
            String _plus_3 = (_string_1 + " ");
            Operator _get_5 = expr.get(1);
            String _string_2 = Integer.valueOf(((Value) _get_5).getValue()).toString();
            String _plus_4 = (_plus_3 + _string_2);
            String _plus_5 = (_plus_4 + " 0");
            str = (_str_1 + _plus_5);
            size = 0;
            int _lines_1 = this.lines;
            this.lines = (_lines_1 + 1);
            Operator _get_6 = expr.get(0);
            viewed.add(Integer.valueOf(Math.abs(((Value) _get_6).getValue())));
            Operator _get_7 = expr.get(1);
            viewed.add(Integer.valueOf(Math.abs(((Value) _get_7).getValue())));
          } else {
            if (((!init) && (size == 3))) {
              String _str_2 = str;
              Operator _get_8 = expr.get(0);
              String _string_3 = Integer.valueOf(((Value) _get_8).getValue()).toString();
              String _plus_6 = (_string_3 + " ");
              Operator _get_9 = expr.get(1);
              String _string_4 = Integer.valueOf(((Value) _get_9).getValue()).toString();
              String _plus_7 = (_plus_6 + _string_4);
              String _plus_8 = (_plus_7 + " ");
              Operator _get_10 = expr.get(2);
              String _string_5 = Integer.valueOf(((Value) _get_10).getValue()).toString();
              String _plus_9 = (_plus_8 + _string_5);
              String _plus_10 = (_plus_9 + " 0");
              str = (_str_2 + _plus_10);
              size = 0;
              int _lines_2 = this.lines;
              this.lines = (_lines_2 + 1);
              Operator _get_11 = expr.get(0);
              viewed.add(Integer.valueOf(Math.abs(((Value) _get_11).getValue())));
              Operator _get_12 = expr.get(1);
              viewed.add(Integer.valueOf(Math.abs(((Value) _get_12).getValue())));
              Operator _get_13 = expr.get(2);
              viewed.add(Integer.valueOf(Math.abs(((Value) _get_13).getValue())));
            } else {
              if ((!init)) {
                String _str_3 = str;
                Operator _get_14 = expr.get(i);
                String _string_6 = Integer.valueOf(((Value) _get_14).getValue()).toString();
                String _plus_11 = (_string_6 + " ");
                Operator _get_15 = expr.get((i + 1));
                String _string_7 = Integer.valueOf(((Value) _get_15).getValue()).toString();
                String _plus_12 = (_plus_11 + _string_7);
                String _plus_13 = (_plus_12 + " ");
                String _string_8 = Integer.valueOf(this.index).toString();
                String _plus_14 = (_plus_13 + _string_8);
                String _plus_15 = (_plus_14 + " 0 \n");
                str = (_str_3 + _plus_15);
                Operator _get_16 = expr.get(i);
                viewed.add(Integer.valueOf(Math.abs(((Value) _get_16).getValue())));
                Operator _get_17 = expr.get((i + 1));
                viewed.add(Integer.valueOf(Math.abs(((Value) _get_17).getValue())));
                viewed.add(Integer.valueOf(this.index));
                int _i = i;
                i = (_i + 2);
                this.index++;
                int _size = size;
                size = (_size - 2);
                init = true;
                int _lines_3 = this.lines;
                this.lines = (_lines_3 + 1);
              } else {
                if ((size == 2)) {
                  String _str_4 = str;
                  String _string_9 = Integer.valueOf((-(this.index - 1))).toString();
                  String _plus_16 = (_string_9 + " ");
                  Operator _get_18 = expr.get(i);
                  String _string_10 = Integer.valueOf(((Value) _get_18).getValue()).toString();
                  String _plus_17 = (_plus_16 + _string_10);
                  String _plus_18 = (_plus_17 + " ");
                  Operator _get_19 = expr.get((i + 1));
                  String _string_11 = Integer.valueOf(((Value) _get_19).getValue()).toString();
                  String _plus_19 = (_plus_18 + _string_11);
                  String _plus_20 = (_plus_19 + " 0");
                  str = (_str_4 + _plus_20);
                  Operator _get_20 = expr.get(i);
                  viewed.add(Integer.valueOf(Math.abs(((Value) _get_20).getValue())));
                  Operator _get_21 = expr.get((i + 1));
                  viewed.add(Integer.valueOf(Math.abs(((Value) _get_21).getValue())));
                  size = 0;
                  int _i_1 = i;
                  i = (_i_1 + 2);
                  int _lines_4 = this.lines;
                  this.lines = (_lines_4 + 1);
                } else {
                  String _str_5 = str;
                  String _string_12 = Integer.valueOf((-(this.index - 1))).toString();
                  String _plus_21 = (_string_12 + " ");
                  Operator _get_22 = expr.get(i);
                  String _string_13 = Integer.valueOf(((Value) _get_22).getValue()).toString();
                  String _plus_22 = (_plus_21 + _string_13);
                  String _plus_23 = (_plus_22 + " ");
                  String _string_14 = Integer.valueOf(this.index).toString();
                  String _plus_24 = (_plus_23 + _string_14);
                  String _plus_25 = (_plus_24 + " 0 \n");
                  str = (_str_5 + _plus_25);
                  Operator _get_23 = expr.get(i);
                  viewed.add(Integer.valueOf(Math.abs(((Value) _get_23).getValue())));
                  viewed.add(Integer.valueOf(this.index));
                  int _i_2 = i;
                  i = (_i_2 + 1);
                  this.index++;
                  int _size_1 = size;
                  size = (_size_1 - 1);
                  int _lines_5 = this.lines;
                  this.lines = (_lines_5 + 1);
                }
              }
            }
          }
        }
      }
    }
    if ((start).booleanValue()) {
      int _size = viewed.size();
      String _plus_2 = ("p cnf " + Integer.valueOf(_size));
      String _plus_3 = (_plus_2 + " ");
      String _plus_4 = (_plus_3 + Integer.valueOf(this.lines));
      String _plus_5 = (_plus_4 + "\n");
      return (_plus_5 + str);
    } else {
      return str;
    }
  }
  
  @Test
  public void loadModel() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("not (a and b) SAT4J;");
    _builder.newLine();
    _builder.append("P and (Q -> (A <-> B)) or a nand not (c -> D <-> F) MiniSat;");
    _builder.newLine();
    _builder.append("c or Q and P SAT4J;");
    _builder.newLine();
    _builder.append("a and d SAT4J;");
    _builder.newLine();
    _builder.append("c or f;");
    _builder.newLine();
    _builder.append("Q -> A <-> B nand Variable;");
    _builder.newLine();
    _builder.append("not Une_variable_complexe;");
    _builder.newLine();
    _builder.append("((((a or D))));");
    _builder.newLine();
    _builder.append("(a and (b and c));");
    _builder.newLine();
    _builder.append("not(a or not a)  SAT4J;");
    _builder.newLine();
    _builder.append("FILE \"simple.cnf\" SAT4J;");
    _builder.newLine();
    _builder.append("FILE \"complex.cnf\" MiniSat;");
    _builder.newLine();
    _builder.append("FILE \"simple.cnf\" MiniSat;");
    _builder.newLine();
    String[] test = new String[] { _builder.toString() };
    this.main(test);
  }
  
  public int main(final String[] args) {
    try {
      List<String> strList = this.ComputeParsing(args);
      final Expression result = this.parseHelper.parse(args[0]);
      Assertions.assertNotNull(result);
      System.out.println("Formulas : ");
      final EList<Solver> SATList = result.getSAT();
      for (int i = 0; (i < strList.size()); i++) {
        {
          String _value = SATList.get(i).getValue();
          boolean _equals = Objects.equal(_value, "SAT4J");
          if (_equals) {
            List<String> value = new ArrayList<String>();
            value.add(strList.get(i));
            final List<String> _converted_value = (List<String>)value;
            SAT4J.main(((String[])Conversions.unwrapArray(_converted_value, String.class)));
          }
          String _value_1 = SATList.get(i).getValue();
          boolean _equals_1 = Objects.equal(_value_1, "MiniSat");
          if (_equals_1) {
            List<String> value_1 = new ArrayList<String>();
            value_1.add(strList.get(i));
            final List<String> _converted_value_1 = (List<String>)value_1;
            MiniSat.main(((String[])Conversions.unwrapArray(_converted_value_1, String.class)));
          }
        }
      }
      System.out.println("\n\nFiles : ");
      final EList<String> SATFiles = result.getFile();
      final EList<SolverFile> SATFList = result.getSATF();
      for (int i = 0; (i < SATFiles.size()); i++) {
        String _value = SATFList.get(i).getValue();
        boolean _equals = Objects.equal(_value, "SAT4J");
        if (_equals) {
          SAT4J.File(SATFiles.get(i));
        } else {
          String _value_1 = SATFList.get(i).getValue();
          boolean _equals_1 = Objects.equal(_value_1, "MiniSat");
          if (_equals_1) {
            MiniSat.File(SATFiles.get(i));
          }
        }
      }
      return 0;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public List<String> ComputeParsing(final String[] args) {
    try {
      List<String> strList = new ArrayList<String>();
      final Expression result = this.parseHelper.parse(args[0]);
      Assertions.assertNotNull(result);
      final EList<Expression> FormulaList = result.getFormula();
      for (int i = 0; (i < FormulaList.size()); i++) {
        {
          int j = 0;
          String _ExpressionToNA = this.ExpressionToNA(FormulaList.get(i));
          String forNot = (_ExpressionToNA + ";");
          String _CompressNot = this.CompressNot(this.parseHelper.parse(forNot).getFormula().get(0), Boolean.valueOf(false));
          String next = (_CompressNot + ";");
          while (((!Objects.equal(next, forNot)) && (j < 20))) {
            {
              forNot = next;
              String _CompressNot_1 = this.CompressNot(this.parseHelper.parse(forNot).getFormula().get(0), Boolean.valueOf(false));
              String _plus = (_CompressNot_1 + ";");
              next = _plus;
              int _j = j;
              j = (_j + 1);
            }
          }
          HashMap<String, Integer> map = new HashMap<String, Integer>();
          String _CompressNot_1 = this.CompressNot(this.parseHelper.parse(next).getFormula().get(0), Boolean.valueOf(true));
          String _plus = (_CompressNot_1 + ";");
          next = _plus;
          Vector<Operator> ret = this.TransformToStruct(this.parseHelper.parse(next).getFormula().get(0), map);
          Vector<Operator> retb = this.ToFNC(ret);
          j = 0;
          while (((!(this.EqualExpr(ret, retb)).booleanValue()) && (j < 20))) {
            {
              ret = retb;
              retb = this.ToFNC(retb);
              j++;
            }
          }
          TreeSet<Integer> _treeSet = new TreeSet<Integer>();
          strList.add(this.ToCNF(ret, Boolean.valueOf(true), _treeSet));
        }
      }
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder.append(_join);
      Assertions.assertTrue(_isEmpty, _builder.toString());
      return strList;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
