package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_TRUE", "RULE_FALSE", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FILE'", "';'", "'('", "')'", "'SAT4J'", "'MiniSat'", "'<->'", "'->'", "'nand'", "'or'", "'and'", "'not'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_TRUE=5;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=7;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_INT=8;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_FALSE=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStart"
    // InternalMyDsl.g:53:1: entryRuleStart : ruleStart EOF ;
    public final void entryRuleStart() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleStart EOF )
            // InternalMyDsl.g:55:1: ruleStart EOF
            {
             before(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            ruleStart();

            state._fsp--;

             after(grammarAccess.getStartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalMyDsl.g:62:1: ruleStart : ( ( rule__Start__Alternatives )* ) ;
    public final void ruleStart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Start__Alternatives )* ) )
            // InternalMyDsl.g:67:2: ( ( rule__Start__Alternatives )* )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Start__Alternatives )* )
            // InternalMyDsl.g:68:3: ( rule__Start__Alternatives )*
            {
             before(grammarAccess.getStartAccess().getAlternatives()); 
            // InternalMyDsl.g:69:3: ( rule__Start__Alternatives )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_TRUE && LA1_0<=RULE_ID)||LA1_0==13||LA1_0==15||LA1_0==24) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:69:4: rule__Start__Alternatives
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Start__Alternatives();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStartAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleSolver"
    // InternalMyDsl.g:78:1: entryRuleSolver : ruleSolver EOF ;
    public final void entryRuleSolver() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleSolver EOF )
            // InternalMyDsl.g:80:1: ruleSolver EOF
            {
             before(grammarAccess.getSolverRule()); 
            pushFollow(FOLLOW_1);
            ruleSolver();

            state._fsp--;

             after(grammarAccess.getSolverRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolver"


    // $ANTLR start "ruleSolver"
    // InternalMyDsl.g:87:1: ruleSolver : ( ( rule__Solver__Alternatives ) ) ;
    public final void ruleSolver() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Solver__Alternatives ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Solver__Alternatives ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Solver__Alternatives ) )
            // InternalMyDsl.g:93:3: ( rule__Solver__Alternatives )
            {
             before(grammarAccess.getSolverAccess().getAlternatives()); 
            // InternalMyDsl.g:94:3: ( rule__Solver__Alternatives )
            // InternalMyDsl.g:94:4: rule__Solver__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolver"


    // $ANTLR start "entryRuleSolverFile"
    // InternalMyDsl.g:103:1: entryRuleSolverFile : ruleSolverFile EOF ;
    public final void entryRuleSolverFile() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleSolverFile EOF )
            // InternalMyDsl.g:105:1: ruleSolverFile EOF
            {
             before(grammarAccess.getSolverFileRule()); 
            pushFollow(FOLLOW_1);
            ruleSolverFile();

            state._fsp--;

             after(grammarAccess.getSolverFileRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSolverFile"


    // $ANTLR start "ruleSolverFile"
    // InternalMyDsl.g:112:1: ruleSolverFile : ( ( rule__SolverFile__Alternatives ) ) ;
    public final void ruleSolverFile() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__SolverFile__Alternatives ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__SolverFile__Alternatives ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__SolverFile__Alternatives ) )
            // InternalMyDsl.g:118:3: ( rule__SolverFile__Alternatives )
            {
             before(grammarAccess.getSolverFileAccess().getAlternatives()); 
            // InternalMyDsl.g:119:3: ( rule__SolverFile__Alternatives )
            // InternalMyDsl.g:119:4: rule__SolverFile__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SolverFile__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSolverFileAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSolverFile"


    // $ANTLR start "entryRuleBiimplies"
    // InternalMyDsl.g:128:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleBiimplies EOF )
            // InternalMyDsl.g:130:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalMyDsl.g:137:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__Biimplies__Group__0 )
            // InternalMyDsl.g:144:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalMyDsl.g:153:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleImplies EOF )
            // InternalMyDsl.g:155:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalMyDsl.g:162:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Implies__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Implies__Group__0 )
            // InternalMyDsl.g:169:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleExcludes"
    // InternalMyDsl.g:178:1: entryRuleExcludes : ruleExcludes EOF ;
    public final void entryRuleExcludes() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleExcludes EOF )
            // InternalMyDsl.g:180:1: ruleExcludes EOF
            {
             before(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getExcludesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalMyDsl.g:187:1: ruleExcludes : ( ( rule__Excludes__Group__0 ) ) ;
    public final void ruleExcludes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__Excludes__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__Excludes__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__Excludes__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__Excludes__Group__0 )
            {
             before(grammarAccess.getExcludesAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__Excludes__Group__0 )
            // InternalMyDsl.g:194:4: rule__Excludes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleOr"
    // InternalMyDsl.g:203:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleOr EOF )
            // InternalMyDsl.g:205:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMyDsl.g:212:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__Or__Group__0 ) )
            // InternalMyDsl.g:218:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalMyDsl.g:219:3: ( rule__Or__Group__0 )
            // InternalMyDsl.g:219:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMyDsl.g:228:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( ruleAnd EOF )
            // InternalMyDsl.g:230:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMyDsl.g:237:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__And__Group__0 ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__And__Group__0 ) )
            // InternalMyDsl.g:243:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalMyDsl.g:244:3: ( rule__And__Group__0 )
            // InternalMyDsl.g:244:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalMyDsl.g:253:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( ruleNot EOF )
            // InternalMyDsl.g:255:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMyDsl.g:262:1: ruleNot : ( ( rule__Not__Alternatives ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__Not__Alternatives ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__Not__Alternatives ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__Not__Alternatives ) )
            // InternalMyDsl.g:268:3: ( rule__Not__Alternatives )
            {
             before(grammarAccess.getNotAccess().getAlternatives()); 
            // InternalMyDsl.g:269:3: ( rule__Not__Alternatives )
            // InternalMyDsl.g:269:4: rule__Not__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Not__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalMyDsl.g:278:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalMyDsl.g:279:1: ( rulePrimary EOF )
            // InternalMyDsl.g:280:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMyDsl.g:287:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:291:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalMyDsl.g:292:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalMyDsl.g:292:2: ( ( rule__Primary__Alternatives ) )
            // InternalMyDsl.g:293:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalMyDsl.g:294:3: ( rule__Primary__Alternatives )
            // InternalMyDsl.g:294:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "rule__Start__Alternatives"
    // InternalMyDsl.g:302:1: rule__Start__Alternatives : ( ( ( rule__Start__Group_0__0 ) ) | ( ( rule__Start__Group_1__0 ) ) );
    public final void rule__Start__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:306:1: ( ( ( rule__Start__Group_0__0 ) ) | ( ( rule__Start__Group_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=RULE_TRUE && LA2_0<=RULE_ID)||LA2_0==15||LA2_0==24) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:307:2: ( ( rule__Start__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:307:2: ( ( rule__Start__Group_0__0 ) )
                    // InternalMyDsl.g:308:3: ( rule__Start__Group_0__0 )
                    {
                     before(grammarAccess.getStartAccess().getGroup_0()); 
                    // InternalMyDsl.g:309:3: ( rule__Start__Group_0__0 )
                    // InternalMyDsl.g:309:4: rule__Start__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Start__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStartAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:313:2: ( ( rule__Start__Group_1__0 ) )
                    {
                    // InternalMyDsl.g:313:2: ( ( rule__Start__Group_1__0 ) )
                    // InternalMyDsl.g:314:3: ( rule__Start__Group_1__0 )
                    {
                     before(grammarAccess.getStartAccess().getGroup_1()); 
                    // InternalMyDsl.g:315:3: ( rule__Start__Group_1__0 )
                    // InternalMyDsl.g:315:4: rule__Start__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Start__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStartAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Alternatives"


    // $ANTLR start "rule__Solver__Alternatives"
    // InternalMyDsl.g:323:1: rule__Solver__Alternatives : ( ( ( rule__Solver__Group_0__0 ) ) | ( ( rule__Solver__Group_1__0 ) ) | ( ( rule__Solver__ValueAssignment_2 ) ) );
    public final void rule__Solver__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:327:1: ( ( ( rule__Solver__Group_0__0 ) ) | ( ( rule__Solver__Group_1__0 ) ) | ( ( rule__Solver__ValueAssignment_2 ) ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt3=1;
                }
                break;
            case 18:
                {
                alt3=2;
                }
                break;
            case 14:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:328:2: ( ( rule__Solver__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:328:2: ( ( rule__Solver__Group_0__0 ) )
                    // InternalMyDsl.g:329:3: ( rule__Solver__Group_0__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_0()); 
                    // InternalMyDsl.g:330:3: ( rule__Solver__Group_0__0 )
                    // InternalMyDsl.g:330:4: rule__Solver__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:334:2: ( ( rule__Solver__Group_1__0 ) )
                    {
                    // InternalMyDsl.g:334:2: ( ( rule__Solver__Group_1__0 ) )
                    // InternalMyDsl.g:335:3: ( rule__Solver__Group_1__0 )
                    {
                     before(grammarAccess.getSolverAccess().getGroup_1()); 
                    // InternalMyDsl.g:336:3: ( rule__Solver__Group_1__0 )
                    // InternalMyDsl.g:336:4: rule__Solver__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:340:2: ( ( rule__Solver__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:340:2: ( ( rule__Solver__ValueAssignment_2 ) )
                    // InternalMyDsl.g:341:3: ( rule__Solver__ValueAssignment_2 )
                    {
                     before(grammarAccess.getSolverAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:342:3: ( rule__Solver__ValueAssignment_2 )
                    // InternalMyDsl.g:342:4: rule__Solver__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Solver__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverAccess().getValueAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Alternatives"


    // $ANTLR start "rule__SolverFile__Alternatives"
    // InternalMyDsl.g:350:1: rule__SolverFile__Alternatives : ( ( ( rule__SolverFile__ValueAssignment_0 ) ) | ( ( rule__SolverFile__ValueAssignment_1 ) ) );
    public final void rule__SolverFile__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:354:1: ( ( ( rule__SolverFile__ValueAssignment_0 ) ) | ( ( rule__SolverFile__ValueAssignment_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            else if ( (LA4_0==18) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:355:2: ( ( rule__SolverFile__ValueAssignment_0 ) )
                    {
                    // InternalMyDsl.g:355:2: ( ( rule__SolverFile__ValueAssignment_0 ) )
                    // InternalMyDsl.g:356:3: ( rule__SolverFile__ValueAssignment_0 )
                    {
                     before(grammarAccess.getSolverFileAccess().getValueAssignment_0()); 
                    // InternalMyDsl.g:357:3: ( rule__SolverFile__ValueAssignment_0 )
                    // InternalMyDsl.g:357:4: rule__SolverFile__ValueAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SolverFile__ValueAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverFileAccess().getValueAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:361:2: ( ( rule__SolverFile__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:361:2: ( ( rule__SolverFile__ValueAssignment_1 ) )
                    // InternalMyDsl.g:362:3: ( rule__SolverFile__ValueAssignment_1 )
                    {
                     before(grammarAccess.getSolverFileAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:363:3: ( rule__SolverFile__ValueAssignment_1 )
                    // InternalMyDsl.g:363:4: rule__SolverFile__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SolverFile__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSolverFileAccess().getValueAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__Alternatives"


    // $ANTLR start "rule__Not__Alternatives"
    // InternalMyDsl.g:371:1: rule__Not__Alternatives : ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) );
    public final void rule__Not__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:375:1: ( ( ( rule__Not__Group_0__0 ) ) | ( ( rule__Not__RightAssignment_1 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==24) ) {
                alt5=1;
            }
            else if ( ((LA5_0>=RULE_TRUE && LA5_0<=RULE_ID)||LA5_0==15) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:376:2: ( ( rule__Not__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:376:2: ( ( rule__Not__Group_0__0 ) )
                    // InternalMyDsl.g:377:3: ( rule__Not__Group_0__0 )
                    {
                     before(grammarAccess.getNotAccess().getGroup_0()); 
                    // InternalMyDsl.g:378:3: ( rule__Not__Group_0__0 )
                    // InternalMyDsl.g:378:4: rule__Not__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:382:2: ( ( rule__Not__RightAssignment_1 ) )
                    {
                    // InternalMyDsl.g:382:2: ( ( rule__Not__RightAssignment_1 ) )
                    // InternalMyDsl.g:383:3: ( rule__Not__RightAssignment_1 )
                    {
                     before(grammarAccess.getNotAccess().getRightAssignment_1()); 
                    // InternalMyDsl.g:384:3: ( rule__Not__RightAssignment_1 )
                    // InternalMyDsl.g:384:4: rule__Not__RightAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Not__RightAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getNotAccess().getRightAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalMyDsl.g:392:1: rule__Primary__Alternatives : ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:396:1: ( ( ( rule__Primary__Group_0__0 ) ) | ( ( rule__Primary__ValueAssignment_1 ) ) | ( ( rule__Primary__ValueAssignment_2 ) ) | ( ( rule__Primary__ValueAssignment_3 ) ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt6=1;
                }
                break;
            case RULE_TRUE:
                {
                alt6=2;
                }
                break;
            case RULE_FALSE:
                {
                alt6=3;
                }
                break;
            case RULE_ID:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:397:2: ( ( rule__Primary__Group_0__0 ) )
                    {
                    // InternalMyDsl.g:397:2: ( ( rule__Primary__Group_0__0 ) )
                    // InternalMyDsl.g:398:3: ( rule__Primary__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_0()); 
                    // InternalMyDsl.g:399:3: ( rule__Primary__Group_0__0 )
                    // InternalMyDsl.g:399:4: rule__Primary__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:403:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    {
                    // InternalMyDsl.g:403:2: ( ( rule__Primary__ValueAssignment_1 ) )
                    // InternalMyDsl.g:404:3: ( rule__Primary__ValueAssignment_1 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 
                    // InternalMyDsl.g:405:3: ( rule__Primary__ValueAssignment_1 )
                    // InternalMyDsl.g:405:4: rule__Primary__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:409:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    {
                    // InternalMyDsl.g:409:2: ( ( rule__Primary__ValueAssignment_2 ) )
                    // InternalMyDsl.g:410:3: ( rule__Primary__ValueAssignment_2 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 
                    // InternalMyDsl.g:411:3: ( rule__Primary__ValueAssignment_2 )
                    // InternalMyDsl.g:411:4: rule__Primary__ValueAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:415:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    {
                    // InternalMyDsl.g:415:2: ( ( rule__Primary__ValueAssignment_3 ) )
                    // InternalMyDsl.g:416:3: ( rule__Primary__ValueAssignment_3 )
                    {
                     before(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 
                    // InternalMyDsl.g:417:3: ( rule__Primary__ValueAssignment_3 )
                    // InternalMyDsl.g:417:4: rule__Primary__ValueAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__ValueAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getValueAssignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Start__Group_0__0"
    // InternalMyDsl.g:425:1: rule__Start__Group_0__0 : rule__Start__Group_0__0__Impl rule__Start__Group_0__1 ;
    public final void rule__Start__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:429:1: ( rule__Start__Group_0__0__Impl rule__Start__Group_0__1 )
            // InternalMyDsl.g:430:2: rule__Start__Group_0__0__Impl rule__Start__Group_0__1
            {
            pushFollow(FOLLOW_4);
            rule__Start__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__0"


    // $ANTLR start "rule__Start__Group_0__0__Impl"
    // InternalMyDsl.g:437:1: rule__Start__Group_0__0__Impl : ( 'FILE' ) ;
    public final void rule__Start__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:441:1: ( ( 'FILE' ) )
            // InternalMyDsl.g:442:1: ( 'FILE' )
            {
            // InternalMyDsl.g:442:1: ( 'FILE' )
            // InternalMyDsl.g:443:2: 'FILE'
            {
             before(grammarAccess.getStartAccess().getFILEKeyword_0_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getFILEKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__0__Impl"


    // $ANTLR start "rule__Start__Group_0__1"
    // InternalMyDsl.g:452:1: rule__Start__Group_0__1 : rule__Start__Group_0__1__Impl rule__Start__Group_0__2 ;
    public final void rule__Start__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:456:1: ( rule__Start__Group_0__1__Impl rule__Start__Group_0__2 )
            // InternalMyDsl.g:457:2: rule__Start__Group_0__1__Impl rule__Start__Group_0__2
            {
            pushFollow(FOLLOW_5);
            rule__Start__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__1"


    // $ANTLR start "rule__Start__Group_0__1__Impl"
    // InternalMyDsl.g:464:1: rule__Start__Group_0__1__Impl : ( ( rule__Start__FileAssignment_0_1 ) ) ;
    public final void rule__Start__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:468:1: ( ( ( rule__Start__FileAssignment_0_1 ) ) )
            // InternalMyDsl.g:469:1: ( ( rule__Start__FileAssignment_0_1 ) )
            {
            // InternalMyDsl.g:469:1: ( ( rule__Start__FileAssignment_0_1 ) )
            // InternalMyDsl.g:470:2: ( rule__Start__FileAssignment_0_1 )
            {
             before(grammarAccess.getStartAccess().getFileAssignment_0_1()); 
            // InternalMyDsl.g:471:2: ( rule__Start__FileAssignment_0_1 )
            // InternalMyDsl.g:471:3: rule__Start__FileAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Start__FileAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFileAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__1__Impl"


    // $ANTLR start "rule__Start__Group_0__2"
    // InternalMyDsl.g:479:1: rule__Start__Group_0__2 : rule__Start__Group_0__2__Impl rule__Start__Group_0__3 ;
    public final void rule__Start__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:483:1: ( rule__Start__Group_0__2__Impl rule__Start__Group_0__3 )
            // InternalMyDsl.g:484:2: rule__Start__Group_0__2__Impl rule__Start__Group_0__3
            {
            pushFollow(FOLLOW_6);
            rule__Start__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__2"


    // $ANTLR start "rule__Start__Group_0__2__Impl"
    // InternalMyDsl.g:491:1: rule__Start__Group_0__2__Impl : ( ( rule__Start__SATFAssignment_0_2 ) ) ;
    public final void rule__Start__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:495:1: ( ( ( rule__Start__SATFAssignment_0_2 ) ) )
            // InternalMyDsl.g:496:1: ( ( rule__Start__SATFAssignment_0_2 ) )
            {
            // InternalMyDsl.g:496:1: ( ( rule__Start__SATFAssignment_0_2 ) )
            // InternalMyDsl.g:497:2: ( rule__Start__SATFAssignment_0_2 )
            {
             before(grammarAccess.getStartAccess().getSATFAssignment_0_2()); 
            // InternalMyDsl.g:498:2: ( rule__Start__SATFAssignment_0_2 )
            // InternalMyDsl.g:498:3: rule__Start__SATFAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Start__SATFAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getSATFAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__2__Impl"


    // $ANTLR start "rule__Start__Group_0__3"
    // InternalMyDsl.g:506:1: rule__Start__Group_0__3 : rule__Start__Group_0__3__Impl ;
    public final void rule__Start__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:510:1: ( rule__Start__Group_0__3__Impl )
            // InternalMyDsl.g:511:2: rule__Start__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__3"


    // $ANTLR start "rule__Start__Group_0__3__Impl"
    // InternalMyDsl.g:517:1: rule__Start__Group_0__3__Impl : ( ';' ) ;
    public final void rule__Start__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:521:1: ( ( ';' ) )
            // InternalMyDsl.g:522:1: ( ';' )
            {
            // InternalMyDsl.g:522:1: ( ';' )
            // InternalMyDsl.g:523:2: ';'
            {
             before(grammarAccess.getStartAccess().getSemicolonKeyword_0_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getSemicolonKeyword_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_0__3__Impl"


    // $ANTLR start "rule__Start__Group_1__0"
    // InternalMyDsl.g:533:1: rule__Start__Group_1__0 : rule__Start__Group_1__0__Impl rule__Start__Group_1__1 ;
    public final void rule__Start__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:537:1: ( rule__Start__Group_1__0__Impl rule__Start__Group_1__1 )
            // InternalMyDsl.g:538:2: rule__Start__Group_1__0__Impl rule__Start__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Start__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__0"


    // $ANTLR start "rule__Start__Group_1__0__Impl"
    // InternalMyDsl.g:545:1: rule__Start__Group_1__0__Impl : ( ( rule__Start__FormulaAssignment_1_0 ) ) ;
    public final void rule__Start__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:549:1: ( ( ( rule__Start__FormulaAssignment_1_0 ) ) )
            // InternalMyDsl.g:550:1: ( ( rule__Start__FormulaAssignment_1_0 ) )
            {
            // InternalMyDsl.g:550:1: ( ( rule__Start__FormulaAssignment_1_0 ) )
            // InternalMyDsl.g:551:2: ( rule__Start__FormulaAssignment_1_0 )
            {
             before(grammarAccess.getStartAccess().getFormulaAssignment_1_0()); 
            // InternalMyDsl.g:552:2: ( rule__Start__FormulaAssignment_1_0 )
            // InternalMyDsl.g:552:3: rule__Start__FormulaAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Start__FormulaAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getFormulaAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__0__Impl"


    // $ANTLR start "rule__Start__Group_1__1"
    // InternalMyDsl.g:560:1: rule__Start__Group_1__1 : rule__Start__Group_1__1__Impl ;
    public final void rule__Start__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:564:1: ( rule__Start__Group_1__1__Impl )
            // InternalMyDsl.g:565:2: rule__Start__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__1"


    // $ANTLR start "rule__Start__Group_1__1__Impl"
    // InternalMyDsl.g:571:1: rule__Start__Group_1__1__Impl : ( ( rule__Start__SATAssignment_1_1 ) ) ;
    public final void rule__Start__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:575:1: ( ( ( rule__Start__SATAssignment_1_1 ) ) )
            // InternalMyDsl.g:576:1: ( ( rule__Start__SATAssignment_1_1 ) )
            {
            // InternalMyDsl.g:576:1: ( ( rule__Start__SATAssignment_1_1 ) )
            // InternalMyDsl.g:577:2: ( rule__Start__SATAssignment_1_1 )
            {
             before(grammarAccess.getStartAccess().getSATAssignment_1_1()); 
            // InternalMyDsl.g:578:2: ( rule__Start__SATAssignment_1_1 )
            // InternalMyDsl.g:578:3: rule__Start__SATAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Start__SATAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getSATAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group_1__1__Impl"


    // $ANTLR start "rule__Solver__Group_0__0"
    // InternalMyDsl.g:587:1: rule__Solver__Group_0__0 : rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1 ;
    public final void rule__Solver__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:591:1: ( rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1 )
            // InternalMyDsl.g:592:2: rule__Solver__Group_0__0__Impl rule__Solver__Group_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Solver__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__0"


    // $ANTLR start "rule__Solver__Group_0__0__Impl"
    // InternalMyDsl.g:599:1: rule__Solver__Group_0__0__Impl : ( ( rule__Solver__ValueAssignment_0_0 ) ) ;
    public final void rule__Solver__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:603:1: ( ( ( rule__Solver__ValueAssignment_0_0 ) ) )
            // InternalMyDsl.g:604:1: ( ( rule__Solver__ValueAssignment_0_0 ) )
            {
            // InternalMyDsl.g:604:1: ( ( rule__Solver__ValueAssignment_0_0 ) )
            // InternalMyDsl.g:605:2: ( rule__Solver__ValueAssignment_0_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_0_0()); 
            // InternalMyDsl.g:606:2: ( rule__Solver__ValueAssignment_0_0 )
            // InternalMyDsl.g:606:3: rule__Solver__ValueAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__0__Impl"


    // $ANTLR start "rule__Solver__Group_0__1"
    // InternalMyDsl.g:614:1: rule__Solver__Group_0__1 : rule__Solver__Group_0__1__Impl ;
    public final void rule__Solver__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:618:1: ( rule__Solver__Group_0__1__Impl )
            // InternalMyDsl.g:619:2: rule__Solver__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__1"


    // $ANTLR start "rule__Solver__Group_0__1__Impl"
    // InternalMyDsl.g:625:1: rule__Solver__Group_0__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:629:1: ( ( ';' ) )
            // InternalMyDsl.g:630:1: ( ';' )
            {
            // InternalMyDsl.g:630:1: ( ';' )
            // InternalMyDsl.g:631:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_0_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_0__1__Impl"


    // $ANTLR start "rule__Solver__Group_1__0"
    // InternalMyDsl.g:641:1: rule__Solver__Group_1__0 : rule__Solver__Group_1__0__Impl rule__Solver__Group_1__1 ;
    public final void rule__Solver__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:645:1: ( rule__Solver__Group_1__0__Impl rule__Solver__Group_1__1 )
            // InternalMyDsl.g:646:2: rule__Solver__Group_1__0__Impl rule__Solver__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Solver__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Solver__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__0"


    // $ANTLR start "rule__Solver__Group_1__0__Impl"
    // InternalMyDsl.g:653:1: rule__Solver__Group_1__0__Impl : ( ( rule__Solver__ValueAssignment_1_0 ) ) ;
    public final void rule__Solver__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:657:1: ( ( ( rule__Solver__ValueAssignment_1_0 ) ) )
            // InternalMyDsl.g:658:1: ( ( rule__Solver__ValueAssignment_1_0 ) )
            {
            // InternalMyDsl.g:658:1: ( ( rule__Solver__ValueAssignment_1_0 ) )
            // InternalMyDsl.g:659:2: ( rule__Solver__ValueAssignment_1_0 )
            {
             before(grammarAccess.getSolverAccess().getValueAssignment_1_0()); 
            // InternalMyDsl.g:660:2: ( rule__Solver__ValueAssignment_1_0 )
            // InternalMyDsl.g:660:3: rule__Solver__ValueAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Solver__ValueAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getSolverAccess().getValueAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__0__Impl"


    // $ANTLR start "rule__Solver__Group_1__1"
    // InternalMyDsl.g:668:1: rule__Solver__Group_1__1 : rule__Solver__Group_1__1__Impl ;
    public final void rule__Solver__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:672:1: ( rule__Solver__Group_1__1__Impl )
            // InternalMyDsl.g:673:2: rule__Solver__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Solver__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__1"


    // $ANTLR start "rule__Solver__Group_1__1__Impl"
    // InternalMyDsl.g:679:1: rule__Solver__Group_1__1__Impl : ( ';' ) ;
    public final void rule__Solver__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:683:1: ( ( ';' ) )
            // InternalMyDsl.g:684:1: ( ';' )
            {
            // InternalMyDsl.g:684:1: ( ';' )
            // InternalMyDsl.g:685:2: ';'
            {
             before(grammarAccess.getSolverAccess().getSemicolonKeyword_1_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getSemicolonKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalMyDsl.g:695:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:699:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalMyDsl.g:700:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalMyDsl.g:707:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:711:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:712:1: ( ruleImplies )
            {
            // InternalMyDsl.g:712:1: ( ruleImplies )
            // InternalMyDsl.g:713:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalMyDsl.g:722:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:726:1: ( rule__Biimplies__Group__1__Impl )
            // InternalMyDsl.g:727:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalMyDsl.g:733:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:737:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalMyDsl.g:738:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalMyDsl.g:738:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalMyDsl.g:739:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:740:2: ( rule__Biimplies__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==19) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:740:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalMyDsl.g:749:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:753:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalMyDsl.g:754:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalMyDsl.g:761:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:765:1: ( ( () ) )
            // InternalMyDsl.g:766:1: ( () )
            {
            // InternalMyDsl.g:766:1: ( () )
            // InternalMyDsl.g:767:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalMyDsl.g:768:2: ()
            // InternalMyDsl.g:768:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalMyDsl.g:776:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:780:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalMyDsl.g:781:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalMyDsl.g:788:1: rule__Biimplies__Group_1__1__Impl : ( ( rule__Biimplies__TypeAssignment_1_1 ) ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:792:1: ( ( ( rule__Biimplies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:793:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:793:1: ( ( rule__Biimplies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:794:2: ( rule__Biimplies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:795:2: ( rule__Biimplies__TypeAssignment_1_1 )
            // InternalMyDsl.g:795:3: rule__Biimplies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalMyDsl.g:803:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:807:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalMyDsl.g:808:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalMyDsl.g:814:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:818:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:819:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:819:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:820:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:821:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalMyDsl.g:821:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalMyDsl.g:830:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:834:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalMyDsl.g:835:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalMyDsl.g:842:1: rule__Implies__Group__0__Impl : ( ruleExcludes ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:846:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:847:1: ( ruleExcludes )
            {
            // InternalMyDsl.g:847:1: ( ruleExcludes )
            // InternalMyDsl.g:848:2: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getExcludesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalMyDsl.g:857:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:861:1: ( rule__Implies__Group__1__Impl )
            // InternalMyDsl.g:862:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalMyDsl.g:868:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:872:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalMyDsl.g:873:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalMyDsl.g:873:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalMyDsl.g:874:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalMyDsl.g:875:2: ( rule__Implies__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==20) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:875:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalMyDsl.g:884:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:888:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalMyDsl.g:889:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalMyDsl.g:896:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:900:1: ( ( () ) )
            // InternalMyDsl.g:901:1: ( () )
            {
            // InternalMyDsl.g:901:1: ( () )
            // InternalMyDsl.g:902:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalMyDsl.g:903:2: ()
            // InternalMyDsl.g:903:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalMyDsl.g:911:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:915:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalMyDsl.g:916:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalMyDsl.g:923:1: rule__Implies__Group_1__1__Impl : ( ( rule__Implies__TypeAssignment_1_1 ) ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:927:1: ( ( ( rule__Implies__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:928:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:928:1: ( ( rule__Implies__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:929:2: ( rule__Implies__TypeAssignment_1_1 )
            {
             before(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:930:2: ( rule__Implies__TypeAssignment_1_1 )
            // InternalMyDsl.g:930:3: rule__Implies__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Implies__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalMyDsl.g:938:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:942:1: ( rule__Implies__Group_1__2__Impl )
            // InternalMyDsl.g:943:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalMyDsl.g:949:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:953:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:954:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:954:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalMyDsl.g:955:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:956:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalMyDsl.g:956:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__Excludes__Group__0"
    // InternalMyDsl.g:965:1: rule__Excludes__Group__0 : rule__Excludes__Group__0__Impl rule__Excludes__Group__1 ;
    public final void rule__Excludes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:969:1: ( rule__Excludes__Group__0__Impl rule__Excludes__Group__1 )
            // InternalMyDsl.g:970:2: rule__Excludes__Group__0__Impl rule__Excludes__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Excludes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0"


    // $ANTLR start "rule__Excludes__Group__0__Impl"
    // InternalMyDsl.g:977:1: rule__Excludes__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Excludes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:981:1: ( ( ruleOr ) )
            // InternalMyDsl.g:982:1: ( ruleOr )
            {
            // InternalMyDsl.g:982:1: ( ruleOr )
            // InternalMyDsl.g:983:2: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0__Impl"


    // $ANTLR start "rule__Excludes__Group__1"
    // InternalMyDsl.g:992:1: rule__Excludes__Group__1 : rule__Excludes__Group__1__Impl ;
    public final void rule__Excludes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:996:1: ( rule__Excludes__Group__1__Impl )
            // InternalMyDsl.g:997:2: rule__Excludes__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1"


    // $ANTLR start "rule__Excludes__Group__1__Impl"
    // InternalMyDsl.g:1003:1: rule__Excludes__Group__1__Impl : ( ( rule__Excludes__Group_1__0 )* ) ;
    public final void rule__Excludes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1007:1: ( ( ( rule__Excludes__Group_1__0 )* ) )
            // InternalMyDsl.g:1008:1: ( ( rule__Excludes__Group_1__0 )* )
            {
            // InternalMyDsl.g:1008:1: ( ( rule__Excludes__Group_1__0 )* )
            // InternalMyDsl.g:1009:2: ( rule__Excludes__Group_1__0 )*
            {
             before(grammarAccess.getExcludesAccess().getGroup_1()); 
            // InternalMyDsl.g:1010:2: ( rule__Excludes__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==21) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:1010:3: rule__Excludes__Group_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Excludes__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getExcludesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__0"
    // InternalMyDsl.g:1019:1: rule__Excludes__Group_1__0 : rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 ;
    public final void rule__Excludes__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1023:1: ( rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 )
            // InternalMyDsl.g:1024:2: rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1
            {
            pushFollow(FOLLOW_13);
            rule__Excludes__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0"


    // $ANTLR start "rule__Excludes__Group_1__0__Impl"
    // InternalMyDsl.g:1031:1: rule__Excludes__Group_1__0__Impl : ( () ) ;
    public final void rule__Excludes__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1035:1: ( ( () ) )
            // InternalMyDsl.g:1036:1: ( () )
            {
            // InternalMyDsl.g:1036:1: ( () )
            // InternalMyDsl.g:1037:2: ()
            {
             before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 
            // InternalMyDsl.g:1038:2: ()
            // InternalMyDsl.g:1038:3: 
            {
            }

             after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0__Impl"


    // $ANTLR start "rule__Excludes__Group_1__1"
    // InternalMyDsl.g:1046:1: rule__Excludes__Group_1__1 : rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 ;
    public final void rule__Excludes__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1050:1: ( rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 )
            // InternalMyDsl.g:1051:2: rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Excludes__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1"


    // $ANTLR start "rule__Excludes__Group_1__1__Impl"
    // InternalMyDsl.g:1058:1: rule__Excludes__Group_1__1__Impl : ( ( rule__Excludes__TypeAssignment_1_1 ) ) ;
    public final void rule__Excludes__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1062:1: ( ( ( rule__Excludes__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1063:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1063:1: ( ( rule__Excludes__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1064:2: ( rule__Excludes__TypeAssignment_1_1 )
            {
             before(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1065:2: ( rule__Excludes__TypeAssignment_1_1 )
            // InternalMyDsl.g:1065:3: rule__Excludes__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__2"
    // InternalMyDsl.g:1073:1: rule__Excludes__Group_1__2 : rule__Excludes__Group_1__2__Impl ;
    public final void rule__Excludes__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1077:1: ( rule__Excludes__Group_1__2__Impl )
            // InternalMyDsl.g:1078:2: rule__Excludes__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2"


    // $ANTLR start "rule__Excludes__Group_1__2__Impl"
    // InternalMyDsl.g:1084:1: rule__Excludes__Group_1__2__Impl : ( ( rule__Excludes__RightAssignment_1_2 ) ) ;
    public final void rule__Excludes__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1088:1: ( ( ( rule__Excludes__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1089:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1089:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1090:2: ( rule__Excludes__RightAssignment_1_2 )
            {
             before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1091:2: ( rule__Excludes__RightAssignment_1_2 )
            // InternalMyDsl.g:1091:3: rule__Excludes__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalMyDsl.g:1100:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1104:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalMyDsl.g:1105:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalMyDsl.g:1112:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1116:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1117:1: ( ruleAnd )
            {
            // InternalMyDsl.g:1117:1: ( ruleAnd )
            // InternalMyDsl.g:1118:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalMyDsl.g:1127:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1131:1: ( rule__Or__Group__1__Impl )
            // InternalMyDsl.g:1132:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalMyDsl.g:1138:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1142:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalMyDsl.g:1143:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalMyDsl.g:1143:1: ( ( rule__Or__Group_1__0 )* )
            // InternalMyDsl.g:1144:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalMyDsl.g:1145:2: ( rule__Or__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==22) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:1145:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalMyDsl.g:1154:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1158:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalMyDsl.g:1159:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalMyDsl.g:1166:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1170:1: ( ( () ) )
            // InternalMyDsl.g:1171:1: ( () )
            {
            // InternalMyDsl.g:1171:1: ( () )
            // InternalMyDsl.g:1172:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalMyDsl.g:1173:2: ()
            // InternalMyDsl.g:1173:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalMyDsl.g:1181:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1185:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalMyDsl.g:1186:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalMyDsl.g:1193:1: rule__Or__Group_1__1__Impl : ( ( rule__Or__TypeAssignment_1_1 ) ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1197:1: ( ( ( rule__Or__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1198:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1198:1: ( ( rule__Or__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1199:2: ( rule__Or__TypeAssignment_1_1 )
            {
             before(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1200:2: ( rule__Or__TypeAssignment_1_1 )
            // InternalMyDsl.g:1200:3: rule__Or__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Or__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalMyDsl.g:1208:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1212:1: ( rule__Or__Group_1__2__Impl )
            // InternalMyDsl.g:1213:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalMyDsl.g:1219:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1223:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1224:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1224:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1225:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1226:2: ( rule__Or__RightAssignment_1_2 )
            // InternalMyDsl.g:1226:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalMyDsl.g:1235:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1239:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalMyDsl.g:1240:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalMyDsl.g:1247:1: rule__And__Group__0__Impl : ( ruleNot ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1251:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1252:1: ( ruleNot )
            {
            // InternalMyDsl.g:1252:1: ( ruleNot )
            // InternalMyDsl.g:1253:2: ruleNot
            {
             before(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getNotParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalMyDsl.g:1262:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1266:1: ( rule__And__Group__1__Impl )
            // InternalMyDsl.g:1267:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalMyDsl.g:1273:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1277:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalMyDsl.g:1278:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalMyDsl.g:1278:1: ( ( rule__And__Group_1__0 )* )
            // InternalMyDsl.g:1279:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalMyDsl.g:1280:2: ( rule__And__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==23) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMyDsl.g:1280:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalMyDsl.g:1289:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1293:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalMyDsl.g:1294:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalMyDsl.g:1301:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1305:1: ( ( () ) )
            // InternalMyDsl.g:1306:1: ( () )
            {
            // InternalMyDsl.g:1306:1: ( () )
            // InternalMyDsl.g:1307:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalMyDsl.g:1308:2: ()
            // InternalMyDsl.g:1308:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalMyDsl.g:1316:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1320:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalMyDsl.g:1321:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalMyDsl.g:1328:1: rule__And__Group_1__1__Impl : ( ( rule__And__TypeAssignment_1_1 ) ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1332:1: ( ( ( rule__And__TypeAssignment_1_1 ) ) )
            // InternalMyDsl.g:1333:1: ( ( rule__And__TypeAssignment_1_1 ) )
            {
            // InternalMyDsl.g:1333:1: ( ( rule__And__TypeAssignment_1_1 ) )
            // InternalMyDsl.g:1334:2: ( rule__And__TypeAssignment_1_1 )
            {
             before(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 
            // InternalMyDsl.g:1335:2: ( rule__And__TypeAssignment_1_1 )
            // InternalMyDsl.g:1335:3: rule__And__TypeAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__And__TypeAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getTypeAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalMyDsl.g:1343:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1347:1: ( rule__And__Group_1__2__Impl )
            // InternalMyDsl.g:1348:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalMyDsl.g:1354:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1358:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalMyDsl.g:1359:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalMyDsl.g:1359:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalMyDsl.g:1360:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalMyDsl.g:1361:2: ( rule__And__RightAssignment_1_2 )
            // InternalMyDsl.g:1361:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group_0__0"
    // InternalMyDsl.g:1370:1: rule__Not__Group_0__0 : rule__Not__Group_0__0__Impl rule__Not__Group_0__1 ;
    public final void rule__Not__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1374:1: ( rule__Not__Group_0__0__Impl rule__Not__Group_0__1 )
            // InternalMyDsl.g:1375:2: rule__Not__Group_0__0__Impl rule__Not__Group_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Not__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0"


    // $ANTLR start "rule__Not__Group_0__0__Impl"
    // InternalMyDsl.g:1382:1: rule__Not__Group_0__0__Impl : ( ( rule__Not__TypeAssignment_0_0 ) ) ;
    public final void rule__Not__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1386:1: ( ( ( rule__Not__TypeAssignment_0_0 ) ) )
            // InternalMyDsl.g:1387:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            {
            // InternalMyDsl.g:1387:1: ( ( rule__Not__TypeAssignment_0_0 ) )
            // InternalMyDsl.g:1388:2: ( rule__Not__TypeAssignment_0_0 )
            {
             before(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 
            // InternalMyDsl.g:1389:2: ( rule__Not__TypeAssignment_0_0 )
            // InternalMyDsl.g:1389:3: rule__Not__TypeAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Not__TypeAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getTypeAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__0__Impl"


    // $ANTLR start "rule__Not__Group_0__1"
    // InternalMyDsl.g:1397:1: rule__Not__Group_0__1 : rule__Not__Group_0__1__Impl ;
    public final void rule__Not__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1401:1: ( rule__Not__Group_0__1__Impl )
            // InternalMyDsl.g:1402:2: rule__Not__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1"


    // $ANTLR start "rule__Not__Group_0__1__Impl"
    // InternalMyDsl.g:1408:1: rule__Not__Group_0__1__Impl : ( ( rule__Not__RightAssignment_0_1 ) ) ;
    public final void rule__Not__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1412:1: ( ( ( rule__Not__RightAssignment_0_1 ) ) )
            // InternalMyDsl.g:1413:1: ( ( rule__Not__RightAssignment_0_1 ) )
            {
            // InternalMyDsl.g:1413:1: ( ( rule__Not__RightAssignment_0_1 ) )
            // InternalMyDsl.g:1414:2: ( rule__Not__RightAssignment_0_1 )
            {
             before(grammarAccess.getNotAccess().getRightAssignment_0_1()); 
            // InternalMyDsl.g:1415:2: ( rule__Not__RightAssignment_0_1 )
            // InternalMyDsl.g:1415:3: rule__Not__RightAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Not__RightAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getRightAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__0"
    // InternalMyDsl.g:1424:1: rule__Primary__Group_0__0 : rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 ;
    public final void rule__Primary__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1428:1: ( rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1 )
            // InternalMyDsl.g:1429:2: rule__Primary__Group_0__0__Impl rule__Primary__Group_0__1
            {
            pushFollow(FOLLOW_10);
            rule__Primary__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0"


    // $ANTLR start "rule__Primary__Group_0__0__Impl"
    // InternalMyDsl.g:1436:1: rule__Primary__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1440:1: ( ( '(' ) )
            // InternalMyDsl.g:1441:1: ( '(' )
            {
            // InternalMyDsl.g:1441:1: ( '(' )
            // InternalMyDsl.g:1442:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__0__Impl"


    // $ANTLR start "rule__Primary__Group_0__1"
    // InternalMyDsl.g:1451:1: rule__Primary__Group_0__1 : rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 ;
    public final void rule__Primary__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1455:1: ( rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2 )
            // InternalMyDsl.g:1456:2: rule__Primary__Group_0__1__Impl rule__Primary__Group_0__2
            {
            pushFollow(FOLLOW_19);
            rule__Primary__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1"


    // $ANTLR start "rule__Primary__Group_0__1__Impl"
    // InternalMyDsl.g:1463:1: rule__Primary__Group_0__1__Impl : ( ruleBiimplies ) ;
    public final void rule__Primary__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1467:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1468:1: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1468:1: ( ruleBiimplies )
            // InternalMyDsl.g:1469:2: ruleBiimplies
            {
             before(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getBiimpliesParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__1__Impl"


    // $ANTLR start "rule__Primary__Group_0__2"
    // InternalMyDsl.g:1478:1: rule__Primary__Group_0__2 : rule__Primary__Group_0__2__Impl ;
    public final void rule__Primary__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1482:1: ( rule__Primary__Group_0__2__Impl )
            // InternalMyDsl.g:1483:2: rule__Primary__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2"


    // $ANTLR start "rule__Primary__Group_0__2__Impl"
    // InternalMyDsl.g:1489:1: rule__Primary__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1493:1: ( ( ')' ) )
            // InternalMyDsl.g:1494:1: ( ')' )
            {
            // InternalMyDsl.g:1494:1: ( ')' )
            // InternalMyDsl.g:1495:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_0__2__Impl"


    // $ANTLR start "rule__Start__FileAssignment_0_1"
    // InternalMyDsl.g:1505:1: rule__Start__FileAssignment_0_1 : ( RULE_STRING ) ;
    public final void rule__Start__FileAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1509:1: ( ( RULE_STRING ) )
            // InternalMyDsl.g:1510:2: ( RULE_STRING )
            {
            // InternalMyDsl.g:1510:2: ( RULE_STRING )
            // InternalMyDsl.g:1511:3: RULE_STRING
            {
             before(grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStartAccess().getFileSTRINGTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FileAssignment_0_1"


    // $ANTLR start "rule__Start__SATFAssignment_0_2"
    // InternalMyDsl.g:1520:1: rule__Start__SATFAssignment_0_2 : ( ruleSolverFile ) ;
    public final void rule__Start__SATFAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1524:1: ( ( ruleSolverFile ) )
            // InternalMyDsl.g:1525:2: ( ruleSolverFile )
            {
            // InternalMyDsl.g:1525:2: ( ruleSolverFile )
            // InternalMyDsl.g:1526:3: ruleSolverFile
            {
             before(grammarAccess.getStartAccess().getSATFSolverFileParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleSolverFile();

            state._fsp--;

             after(grammarAccess.getStartAccess().getSATFSolverFileParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__SATFAssignment_0_2"


    // $ANTLR start "rule__Start__FormulaAssignment_1_0"
    // InternalMyDsl.g:1535:1: rule__Start__FormulaAssignment_1_0 : ( ruleBiimplies ) ;
    public final void rule__Start__FormulaAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1539:1: ( ( ruleBiimplies ) )
            // InternalMyDsl.g:1540:2: ( ruleBiimplies )
            {
            // InternalMyDsl.g:1540:2: ( ruleBiimplies )
            // InternalMyDsl.g:1541:3: ruleBiimplies
            {
             before(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getStartAccess().getFormulaBiimpliesParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__FormulaAssignment_1_0"


    // $ANTLR start "rule__Start__SATAssignment_1_1"
    // InternalMyDsl.g:1550:1: rule__Start__SATAssignment_1_1 : ( ruleSolver ) ;
    public final void rule__Start__SATAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1554:1: ( ( ruleSolver ) )
            // InternalMyDsl.g:1555:2: ( ruleSolver )
            {
            // InternalMyDsl.g:1555:2: ( ruleSolver )
            // InternalMyDsl.g:1556:3: ruleSolver
            {
             before(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSolver();

            state._fsp--;

             after(grammarAccess.getStartAccess().getSATSolverParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__SATAssignment_1_1"


    // $ANTLR start "rule__Solver__ValueAssignment_0_0"
    // InternalMyDsl.g:1565:1: rule__Solver__ValueAssignment_0_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__Solver__ValueAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1569:1: ( ( ( 'SAT4J' ) ) )
            // InternalMyDsl.g:1570:2: ( ( 'SAT4J' ) )
            {
            // InternalMyDsl.g:1570:2: ( ( 'SAT4J' ) )
            // InternalMyDsl.g:1571:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 
            // InternalMyDsl.g:1572:3: ( 'SAT4J' )
            // InternalMyDsl.g:1573:4: 'SAT4J'
            {
             before(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueSAT4JKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_0_0"


    // $ANTLR start "rule__Solver__ValueAssignment_1_0"
    // InternalMyDsl.g:1584:1: rule__Solver__ValueAssignment_1_0 : ( ( 'MiniSat' ) ) ;
    public final void rule__Solver__ValueAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1588:1: ( ( ( 'MiniSat' ) ) )
            // InternalMyDsl.g:1589:2: ( ( 'MiniSat' ) )
            {
            // InternalMyDsl.g:1589:2: ( ( 'MiniSat' ) )
            // InternalMyDsl.g:1590:3: ( 'MiniSat' )
            {
             before(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 
            // InternalMyDsl.g:1591:3: ( 'MiniSat' )
            // InternalMyDsl.g:1592:4: 'MiniSat'
            {
             before(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueMiniSatKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_1_0"


    // $ANTLR start "rule__Solver__ValueAssignment_2"
    // InternalMyDsl.g:1603:1: rule__Solver__ValueAssignment_2 : ( ( ';' ) ) ;
    public final void rule__Solver__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1607:1: ( ( ( ';' ) ) )
            // InternalMyDsl.g:1608:2: ( ( ';' ) )
            {
            // InternalMyDsl.g:1608:2: ( ( ';' ) )
            // InternalMyDsl.g:1609:3: ( ';' )
            {
             before(grammarAccess.getSolverAccess().getValueSemicolonKeyword_2_0()); 
            // InternalMyDsl.g:1610:3: ( ';' )
            // InternalMyDsl.g:1611:4: ';'
            {
             before(grammarAccess.getSolverAccess().getValueSemicolonKeyword_2_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSolverAccess().getValueSemicolonKeyword_2_0()); 

            }

             after(grammarAccess.getSolverAccess().getValueSemicolonKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Solver__ValueAssignment_2"


    // $ANTLR start "rule__SolverFile__ValueAssignment_0"
    // InternalMyDsl.g:1622:1: rule__SolverFile__ValueAssignment_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__SolverFile__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1626:1: ( ( ( 'SAT4J' ) ) )
            // InternalMyDsl.g:1627:2: ( ( 'SAT4J' ) )
            {
            // InternalMyDsl.g:1627:2: ( ( 'SAT4J' ) )
            // InternalMyDsl.g:1628:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 
            // InternalMyDsl.g:1629:3: ( 'SAT4J' )
            // InternalMyDsl.g:1630:4: 'SAT4J'
            {
             before(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 

            }

             after(grammarAccess.getSolverFileAccess().getValueSAT4JKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__ValueAssignment_0"


    // $ANTLR start "rule__SolverFile__ValueAssignment_1"
    // InternalMyDsl.g:1641:1: rule__SolverFile__ValueAssignment_1 : ( ( 'MiniSat' ) ) ;
    public final void rule__SolverFile__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1645:1: ( ( ( 'MiniSat' ) ) )
            // InternalMyDsl.g:1646:2: ( ( 'MiniSat' ) )
            {
            // InternalMyDsl.g:1646:2: ( ( 'MiniSat' ) )
            // InternalMyDsl.g:1647:3: ( 'MiniSat' )
            {
             before(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 
            // InternalMyDsl.g:1648:3: ( 'MiniSat' )
            // InternalMyDsl.g:1649:4: 'MiniSat'
            {
             before(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 

            }

             after(grammarAccess.getSolverFileAccess().getValueMiniSatKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SolverFile__ValueAssignment_1"


    // $ANTLR start "rule__Biimplies__TypeAssignment_1_1"
    // InternalMyDsl.g:1660:1: rule__Biimplies__TypeAssignment_1_1 : ( ( '<->' ) ) ;
    public final void rule__Biimplies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1664:1: ( ( ( '<->' ) ) )
            // InternalMyDsl.g:1665:2: ( ( '<->' ) )
            {
            // InternalMyDsl.g:1665:2: ( ( '<->' ) )
            // InternalMyDsl.g:1666:3: ( '<->' )
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1667:3: ( '<->' )
            // InternalMyDsl.g:1668:4: '<->'
            {
             before(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getBiimpliesAccess().getTypeLessThanSignHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__TypeAssignment_1_1"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalMyDsl.g:1679:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1683:1: ( ( ruleImplies ) )
            // InternalMyDsl.g:1684:2: ( ruleImplies )
            {
            // InternalMyDsl.g:1684:2: ( ruleImplies )
            // InternalMyDsl.g:1685:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__TypeAssignment_1_1"
    // InternalMyDsl.g:1694:1: rule__Implies__TypeAssignment_1_1 : ( ( '->' ) ) ;
    public final void rule__Implies__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1698:1: ( ( ( '->' ) ) )
            // InternalMyDsl.g:1699:2: ( ( '->' ) )
            {
            // InternalMyDsl.g:1699:2: ( ( '->' ) )
            // InternalMyDsl.g:1700:3: ( '->' )
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            // InternalMyDsl.g:1701:3: ( '->' )
            // InternalMyDsl.g:1702:4: '->'
            {
             before(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }

             after(grammarAccess.getImpliesAccess().getTypeHyphenMinusGreaterThanSignKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__TypeAssignment_1_1"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalMyDsl.g:1713:1: rule__Implies__RightAssignment_1_2 : ( ruleExcludes ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1717:1: ( ( ruleExcludes ) )
            // InternalMyDsl.g:1718:2: ( ruleExcludes )
            {
            // InternalMyDsl.g:1718:2: ( ruleExcludes )
            // InternalMyDsl.g:1719:3: ruleExcludes
            {
             before(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightExcludesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__Excludes__TypeAssignment_1_1"
    // InternalMyDsl.g:1728:1: rule__Excludes__TypeAssignment_1_1 : ( ( 'nand' ) ) ;
    public final void rule__Excludes__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1732:1: ( ( ( 'nand' ) ) )
            // InternalMyDsl.g:1733:2: ( ( 'nand' ) )
            {
            // InternalMyDsl.g:1733:2: ( ( 'nand' ) )
            // InternalMyDsl.g:1734:3: ( 'nand' )
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            // InternalMyDsl.g:1735:3: ( 'nand' )
            // InternalMyDsl.g:1736:4: 'nand'
            {
             before(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }

             after(grammarAccess.getExcludesAccess().getTypeNandKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__TypeAssignment_1_1"


    // $ANTLR start "rule__Excludes__RightAssignment_1_2"
    // InternalMyDsl.g:1747:1: rule__Excludes__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Excludes__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1751:1: ( ( ruleOr ) )
            // InternalMyDsl.g:1752:2: ( ruleOr )
            {
            // InternalMyDsl.g:1752:2: ( ruleOr )
            // InternalMyDsl.g:1753:3: ruleOr
            {
             before(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__RightAssignment_1_2"


    // $ANTLR start "rule__Or__TypeAssignment_1_1"
    // InternalMyDsl.g:1762:1: rule__Or__TypeAssignment_1_1 : ( ( 'or' ) ) ;
    public final void rule__Or__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1766:1: ( ( ( 'or' ) ) )
            // InternalMyDsl.g:1767:2: ( ( 'or' ) )
            {
            // InternalMyDsl.g:1767:2: ( ( 'or' ) )
            // InternalMyDsl.g:1768:3: ( 'or' )
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            // InternalMyDsl.g:1769:3: ( 'or' )
            // InternalMyDsl.g:1770:4: 'or'
            {
             before(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }

             after(grammarAccess.getOrAccess().getTypeOrKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__TypeAssignment_1_1"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalMyDsl.g:1781:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1785:1: ( ( ruleAnd ) )
            // InternalMyDsl.g:1786:2: ( ruleAnd )
            {
            // InternalMyDsl.g:1786:2: ( ruleAnd )
            // InternalMyDsl.g:1787:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__TypeAssignment_1_1"
    // InternalMyDsl.g:1796:1: rule__And__TypeAssignment_1_1 : ( ( 'and' ) ) ;
    public final void rule__And__TypeAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1800:1: ( ( ( 'and' ) ) )
            // InternalMyDsl.g:1801:2: ( ( 'and' ) )
            {
            // InternalMyDsl.g:1801:2: ( ( 'and' ) )
            // InternalMyDsl.g:1802:3: ( 'and' )
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            // InternalMyDsl.g:1803:3: ( 'and' )
            // InternalMyDsl.g:1804:4: 'and'
            {
             before(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }

             after(grammarAccess.getAndAccess().getTypeAndKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__TypeAssignment_1_1"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalMyDsl.g:1815:1: rule__And__RightAssignment_1_2 : ( ruleNot ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1819:1: ( ( ruleNot ) )
            // InternalMyDsl.g:1820:2: ( ruleNot )
            {
            // InternalMyDsl.g:1820:2: ( ruleNot )
            // InternalMyDsl.g:1821:3: ruleNot
            {
             before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Not__TypeAssignment_0_0"
    // InternalMyDsl.g:1830:1: rule__Not__TypeAssignment_0_0 : ( ( 'not' ) ) ;
    public final void rule__Not__TypeAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1834:1: ( ( ( 'not' ) ) )
            // InternalMyDsl.g:1835:2: ( ( 'not' ) )
            {
            // InternalMyDsl.g:1835:2: ( ( 'not' ) )
            // InternalMyDsl.g:1836:3: ( 'not' )
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            // InternalMyDsl.g:1837:3: ( 'not' )
            // InternalMyDsl.g:1838:4: 'not'
            {
             before(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }

             after(grammarAccess.getNotAccess().getTypeNotKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__TypeAssignment_0_0"


    // $ANTLR start "rule__Not__RightAssignment_0_1"
    // InternalMyDsl.g:1849:1: rule__Not__RightAssignment_0_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1853:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1854:2: ( rulePrimary )
            {
            // InternalMyDsl.g:1854:2: ( rulePrimary )
            // InternalMyDsl.g:1855:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_0_1"


    // $ANTLR start "rule__Not__RightAssignment_1"
    // InternalMyDsl.g:1864:1: rule__Not__RightAssignment_1 : ( rulePrimary ) ;
    public final void rule__Not__RightAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1868:1: ( ( rulePrimary ) )
            // InternalMyDsl.g:1869:2: ( rulePrimary )
            {
            // InternalMyDsl.g:1869:2: ( rulePrimary )
            // InternalMyDsl.g:1870:3: rulePrimary
            {
             before(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getRightPrimaryParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__RightAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_1"
    // InternalMyDsl.g:1879:1: rule__Primary__ValueAssignment_1 : ( RULE_TRUE ) ;
    public final void rule__Primary__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1883:1: ( ( RULE_TRUE ) )
            // InternalMyDsl.g:1884:2: ( RULE_TRUE )
            {
            // InternalMyDsl.g:1884:2: ( RULE_TRUE )
            // InternalMyDsl.g:1885:3: RULE_TRUE
            {
             before(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 
            match(input,RULE_TRUE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueTRUETerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_1"


    // $ANTLR start "rule__Primary__ValueAssignment_2"
    // InternalMyDsl.g:1894:1: rule__Primary__ValueAssignment_2 : ( RULE_FALSE ) ;
    public final void rule__Primary__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1898:1: ( ( RULE_FALSE ) )
            // InternalMyDsl.g:1899:2: ( RULE_FALSE )
            {
            // InternalMyDsl.g:1899:2: ( RULE_FALSE )
            // InternalMyDsl.g:1900:3: RULE_FALSE
            {
             before(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 
            match(input,RULE_FALSE,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueFALSETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_2"


    // $ANTLR start "rule__Primary__ValueAssignment_3"
    // InternalMyDsl.g:1909:1: rule__Primary__ValueAssignment_3 : ( RULE_ID ) ;
    public final void rule__Primary__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1913:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1914:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1914:2: ( RULE_ID )
            // InternalMyDsl.g:1915:3: RULE_ID
            {
             before(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getValueIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__ValueAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000100A0E2L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000064000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000000100A0E0L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000010000L});

}